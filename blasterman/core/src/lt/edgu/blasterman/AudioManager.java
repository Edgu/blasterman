package lt.edgu.blasterman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public abstract class AudioManager {

	// Sounds
	public static Sound powerUp;
	public static Sound explosion;
	public static Sound select;
	public static Sound plantBomb;

	// Music
	public static Music pegi_3;
	public static Music menuMusic;
	public static Music gameMusic;

	private AudioManager() {}

	public static void init() {
		// Sounds
		powerUp = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/powerup.wav"));
		explosion = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/explosion.wav"));
		select = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/select.wav"));
		plantBomb = Gdx.audio.newSound(Gdx.files.internal("audio/sounds/plantBomb.wav"));

		// Music
		pegi_3 = Gdx.audio.newMusic(Gdx.files.internal("audio/music/PEGI_3.mp3"));
		menuMusic = Gdx.audio.newMusic(Gdx.files.internal("audio/music/menu.mp3"));
		gameMusic = Gdx.audio.newMusic(Gdx.files.internal("audio/music/test.mp3"));
	}


	public static void select() {
		select.play();
	}

	public static void playGameMusic(boolean loop) {
		gameMusic.setLooping(loop);
		gameMusic.setVolume(0.5F);
		gameMusic.play();
	}

	public static void stopGameMusic() {
		gameMusic.stop();
	}

	public static void playMenuMusic(boolean loop) {
		menuMusic.setLooping(loop);
		menuMusic.setVolume(0.5F);
		menuMusic.play();
	}

	public static void stopMenuMusic() {
		menuMusic.stop();
	}

	public static void dispose() {
		powerUp.dispose();
		explosion.dispose();
		select.dispose();
		plantBomb.dispose();
		gameMusic.dispose();
		menuMusic.dispose();
		pegi_3.dispose();
	}
}
