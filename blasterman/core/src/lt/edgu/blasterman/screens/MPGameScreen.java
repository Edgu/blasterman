package lt.edgu.blasterman.screens;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.TiledManager;
import lt.edgu.blasterman.entity.MPPlayer;
import lt.edgu.blasterman.entity.OtherPlayer;
import lt.edgu.blasterman.network.MasterServerClient;
import lt.edgu.blasterman.network.RoomServerClient;
import lt.edgu.blasterman.network.packets.PacketNewMessage;
import lt.edgu.blasterman.network.packets.room.PacketNewBomb;
import lt.edgu.blasterman.network.packets.room.PacketStartGame;
import lt.edgu.blasterman.util.UI;

public class MPGameScreen implements Screen, GestureDetector.GestureListener, InputProcessor {
	public TiledManager tiledManager;
	public TiledMap tiledMap;
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera camera;
	private Vector3 screenSize;
	private SpriteBatch textBatch;
	private BitmapFont font;
	private GlyphLayout layout;
	private volatile MPPlayer player;
	private MasterServerClient masterServerClient;
	private volatile RoomServerClient roomServerClient;
	public Map<Integer, OtherPlayer> players;
	private volatile TiledMapTileLayer mainLayer;
	private long javaHeap;
	private long nativeHeap;
	private String username;
	public Vector2 deathCamera;
	public Stage stage;
	private TextureAtlas textureAtlas;
	public Skin skin;
	private TextureAtlas buttonsAtlas;
	private Skin buttonsSkin;
	public Touchpad touchpad;
	private volatile InputMultiplexer inputMultiplexer;
	private GestureDetector gestureDetector;

	// Temp Var
	private float nickOffsetX;
	private RoomsScreen roomsScreen;
	private boolean usingPhone;
	private SpriteBatch background;
	private String mapName;

	// Tables
	public Table hud;
	public Table playerYellow;
	public Table playerBlue;
	public Table playerRed;
	public Table playerGray;
	public Table chat;
	public Table leftBottomButtons;
	public Table roomName;

	// Labels
	public Label yellowPlayerName;
	public Label bluePlayerName;
	public Label redPlayerName;
	public Label grayPlayerName;
	public Label chatWindow;

	public TextField messageField;
	public ScrollPane scrollPane;

	// TextButton
	public TextButton backButton;
	public TextButton sendMessageButton;
	public TextButton hostButton;
	public TextButton bombButton;

	private float zoom = 1.0f;
	private float initialScale = 1.0f;
	public boolean debug = true;

	public MPGameScreen(String username) {
		players = new HashMap<Integer, OtherPlayer>();
		this.masterServerClient = new MasterServerClient();
		this.masterServerClient.connect(username);
		this.username = username;
		create();
	}

	public MPGameScreen() {
		players = new HashMap<Integer, OtherPlayer>();
		this.masterServerClient = new MasterServerClient();
		this.masterServerClient.connect("UNNAMED");
		this.username = "UNNAMED";
		create();
	}

	public MPGameScreen(MasterServerClient msClient, RoomsScreen roomsScreen) {
		this.roomsScreen = roomsScreen;
		players = new HashMap<Integer, OtherPlayer>();
		this.masterServerClient = msClient;
		this.username = roomsScreen.username;
		create();
	}

	public MPGameScreen(MasterServerClient msClient, RoomsScreen roomsScreen, String mapName) {
		this.roomsScreen = roomsScreen;
		players = new HashMap<Integer, OtherPlayer>();
		this.masterServerClient = msClient;
		this.username = roomsScreen.username;
		this.mapName = mapName;
		create();
	}

	void create() {
		buttonsAtlas = new TextureAtlas(Gdx.files.internal("ui/button_trans.atlas"));
		buttonsSkin = new Skin(Gdx.files.internal("ui/buttons.json"), buttonsAtlas);
		textureAtlas = new TextureAtlas("ui/uiskin.atlas");
		skin = new Skin(Gdx.files.internal("ui/loginSkin.json"), textureAtlas);
		players = new HashMap<Integer, OtherPlayer>();
		font = new BitmapFont();
		font.setColor(Color.WHITE);
		TmxMapLoader.Parameters par = new TmxMapLoader.Parameters();
		par.textureMinFilter = TextureFilter.Nearest;
		par.textureMagFilter = TextureFilter.Nearest;
		tiledMap = new TmxMapLoader().load("maps/multiPlayer/" + mapName + "/" + mapName + ".tmx", par);
		tiledManager = new TiledManager();
		tiledManager.init(tiledMap);
		renderer = new OrthogonalTiledMapRenderer(tiledMap);
		mainLayer = (TiledMapTileLayer) tiledMap.getLayers().get(0);
		camera = new OrthographicCamera();
		screenSize = new Vector3((tiledMap.getProperties().get("width", Integer.class) * 32) / 2, (tiledMap.getProperties().get("height", Integer.class) * 32) / 2, 0);
		player = new MPPlayer((TiledMapTileLayer) tiledMap.getLayers().get(0), this);
		layout = new GlyphLayout();
		layout.setText(font, username);
		nickOffsetX = (layout.width / 2) - (player.getWidth() / 2);
		javaHeap = Gdx.app.getJavaHeap() / 1000;
		nativeHeap = Gdx.app.getNativeHeap() / 1000;
		textBatch = new SpriteBatch();
		roomName = new Table(skin);

		playerYellow = new Table(skin);
		yellowPlayerName = new Label("", skin);
		playerBlue = new Table(skin);
		bluePlayerName = new Label("", skin);
		playerRed = new Table(skin);
		redPlayerName = new Label("", skin);
		playerGray = new Table(skin);
		grayPlayerName = new Label("", skin);
		background = new SpriteBatch();
		chatWindow = new Label("Connected to room.", skin);
		chatWindow.setWrap(true);
		chatWindow.setAlignment(Align.topLeft);
		scrollPane = new ScrollPane(chatWindow, skin);
		messageField = new TextField("", skin);
		sendMessageButton = new TextButton("SEND", skin);
		sendMessageButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				newMessage(messageField.getText(), username, false);
			}
		});
		stage = new Stage();

		float offsetX = 0.0145F * stage.getWidth();
		float tableWidth = 0.06F * stage.getWidth();
		float tableHeight = 0.09F * stage.getHeight();

		//		roomName.clear();
		//		roomName.setBounds(0, 0.9525F * stage.getHeight(), 0.234F * stage.getWidth(), (300 / 1000) * stage.getHeight());
		//		roomName.add(roomsScreen.currentRoom.name);

		/** FIX GRAY IMAGE **/
		playerYellow.setBounds(offsetX, 0.802F * stage.getHeight(), tableWidth, tableHeight);
		Table yellow = new Table(skin);
		yellow.setBounds(0.15F * stage.getWidth(), 0.801F * stage.getHeight(), 0.057F, 0.097F * stage.getHeight());
		yellow.add(yellowPlayerName);
		playerYellow.setDebug(false);

		playerBlue.setBounds(offsetX, 0.680F * stage.getHeight(), tableWidth, tableHeight);
		Table blue = new Table(skin);
		blue.setBounds(0.15F * stage.getWidth(), 0.680F * stage.getHeight(), 0.057F, 0.097F * stage.getHeight());
		blue.add(bluePlayerName);

		playerRed.setBounds(offsetX, 0.558F * stage.getHeight(), tableWidth, tableHeight);
		Table red = new Table(skin);
		red.setBounds(0.15F * stage.getWidth(), 0.559F * stage.getHeight(), 0.057F, 0.097F * stage.getHeight());
		red.add(redPlayerName);

		playerGray.setBounds(offsetX, 0.438F * stage.getHeight(), tableWidth, tableHeight);
		Table gray = new Table(skin);
		gray.setBounds(0.15F * stage.getWidth(), 0.437F * stage.getHeight(), 0.057F, 0.097F * stage.getHeight());
		gray.add(grayPlayerName);

		backButton = new TextButton("Rooms", skin);
		backButton.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				((Game) Gdx.app.getApplicationListener()).setScreen(roomsScreen);
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		switch (Gdx.app.getType()) {
			case Android:
				usingPhone = true;
				Skin touchpadSkin = new Skin();
				touchpadSkin.add("touchBackground", new Texture("ui/touchBackground.png"));
				touchpadSkin.add("touchKnob", new Texture("ui/touchKnob.png"));
				TouchpadStyle touchpadStyle = new TouchpadStyle();
				Drawable touchBackground = touchpadSkin.getDrawable("touchBackground");
				Drawable touchKnob = touchpadSkin.getDrawable("touchKnob");
				touchpadStyle.background = touchBackground;
				touchpadStyle.knob = touchKnob;
				touchpad = new Touchpad(0, touchpadStyle);
				touchpad.setBounds(50, 50, 200, 200);
				touchpad.setVisible(false);
				touchpad.setTouchable(Touchable.disabled);
				break;
			case Applet:
				usingPhone = false;
				break;
			case Desktop:
				usingPhone = false;
				break;
			case HeadlessDesktop:
				usingPhone = false;
				break;
			case WebGL:
				usingPhone = false;
				break;
			case iOS:
				usingPhone = true;
				break;
			default:
				usingPhone = false;
				break;
		}

		hud = new Table(skin);
		hud.setBounds(0, 0, 0.2415f * stage.getWidth(), stage.getHeight());
		hud.background(TextureManager.ingameHUD.getDrawable());

		chat = new Table(skin);
		chat.setWidth(0.6F * stage.getWidth());
		chat.setHeight(0.5F * stage.getHeight());
		chat.setPosition(((stage.getWidth() + (0.234F * stage.getWidth())) - chat.getWidth()) / 2, (stage.getHeight() - chat.getHeight()) / 2);

		chat.center();
		chat.add(messageField).left().height(50).fillX().center();
		chat.add(sendMessageButton).right().fill();
		chat.row();
		chat.add().spaceBottom(5);
		chat.row();
		chat.add(scrollPane).size(chat.getWidth(), chat.getHeight()).colspan(2);

		bombButton = new TextButton("BOMB", buttonsSkin);
		bombButton.setVisible(false);
		bombButton.setTouchable(Touchable.disabled);
		bombButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AudioManager.plantBomb.play();
				switch (player.getCurrentMove()) {
				case 'w':
					createNewBomb(new Vector2((int) ((player.getPosition().x + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) (player.getPosition().y / mainLayer.getTileHeight()) * 32));
					break;
				case 'a':
					createNewBomb(new Vector2((int) (((player.getPosition().x + 15) + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) ((player.getPosition().y + player.getHeight() / 2) / mainLayer.getTileHeight()) * 32));
					break;
				case 's':
					createNewBomb(new Vector2((int) ((player.getPosition().x + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) ((player.getPosition().y + 31) / mainLayer.getTileHeight()) * 32));
					break;
				case 'd':
					createNewBomb(new Vector2((int) (((player.getPosition().x - 16) + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) ((player.getPosition().y + player.getHeight() / 2) / mainLayer.getTileHeight()) * 32));
					break;
				case 'x':
					createNewBomb(new Vector2((int) ((player.getPosition().x + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) (player.getPosition().y / mainLayer.getTileHeight()) * 32));
					break;
				}
				super.clicked(event, x, y);
			}
		});

		Table bombTable = new Table(skin);
		bombTable.setPosition(0.87f * stage.getWidth(), 0.19f * stage.getHeight());
		bombTable.add(bombButton).size(0.15f * stage.getWidth(), 0.2f * stage.getHeight());

		stage.addActor(hud);
		stage.addActor(playerYellow);
		stage.addActor(yellow);
		stage.addActor(playerBlue);
		stage.addActor(blue);
		stage.addActor(playerRed);
		stage.addActor(red);
		stage.addActor(playerGray);
		stage.addActor(gray);
		stage.addActor(roomName);
		stage.addActor(bombTable);

		leftBottomButtons = new Table(skin);
		leftBottomButtons.setBounds(0.010F * stage.getWidth(), 0.30F * stage.getHeight(), 0.21375F * stage.getWidth(), 0.150F * stage.getHeight());
		leftBottomButtons.left();

		hostButton = new TextButton("START", skin);
		hostButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				PacketStartGame packet = new PacketStartGame();
				roomServerClient.getClient().sendTCP(packet);
			}
		});

		leftBottomButtons.add(backButton).width(leftBottomButtons.getWidth() / 2).height(leftBottomButtons.getHeight() / 2);
		leftBottomButtons.add(hostButton).width(leftBottomButtons.getWidth() / 2).height(leftBottomButtons.getHeight() / 2);
		hostButton.setVisible(false);
		stage.addActor(leftBottomButtons);
	}

	@Override
	public void show() {
		AudioManager.playGameMusic(true);

		gestureDetector = new GestureDetector(this);
		inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(gestureDetector);
		inputMultiplexer.addProcessor(stage);
		inputMultiplexer.addProcessor(this);
		Gdx.input.setInputProcessor(inputMultiplexer);

		roomName.clear();
		roomName.setBounds(0, 0.9525F * stage.getHeight(), 0.234F * stage.getWidth(), (300 / 1000) * stage.getHeight());
		roomName.add(roomsScreen.currentRoom.name);

		if (roomsScreen.currentRoom.host.equals(roomsScreen.username)) {
			hostButton.setVisible(true);
		}

		if (usingPhone) {
			stage.addActor(touchpad);
		}

		if (!roomServerClient.roomStarted) {
			stage.addActor(chat);
		} else {
			inputMultiplexer.addProcessor(getPlayer());
			if (player.isDead()) {
				hostButton.setVisible(false);
				backButton.setVisible(true);
			}
		}
		camera.position.set(screenSize.x - ((0.2415f * Gdx.graphics.getWidth()) / 2), screenSize.y, 0);
	}

	public void gameOver(String winner) {
		UI.gameFinish(winner, skin, stage);
	}

	public void updateCam() {
		if (camera.position.x <= (-stage.getWidth() / 2)) {
			camera.position.set(screenSize.x - ((0.2415f * Gdx.graphics.getWidth()) / 2), screenSize.y, 0);
		}
	}

	public void newMessage(String message, String sender, boolean received) {
		if (received) {
			chatWindow.setText("[" + sender + "] " + message + "\n" + chatWindow.getText());
		} else if (!message.isEmpty() || message.equals(" ")) {
			messageField.setText("");
			PacketNewMessage packet = new PacketNewMessage();
			packet.sender = sender;
			packet.message = message;
			roomServerClient.getClient().sendTCP(packet);
			chatWindow.setText("[" + sender + "] " + message + "\n" + chatWindow.getText());
		}
	}

	@Override
	public void render(float delta) {
		camera.zoom = zoom;

		background.begin();
		background.draw(TextureManager.grass, 0, 0, TextureManager.grass.getWidth(), TextureManager.grass.getHeight(), (int) stage.getWidth(), (int) stage.getHeight());
		background.end();

		updateCam();
		camera.update();

		// Renders Tiled Map
		renderer.setView(camera);
		if (roomServerClient != null) {
			renderer.render();
		}

		renderer.getBatch().setProjectionMatrix(camera.combined);

		renderer.getBatch().begin();

		try {
			for (OtherPlayer mpPlayer : players.values()) {
				if (mpPlayer.isAlive()) {
					mpPlayer.render(renderer.getBatch(), delta);
					if (mpPlayer.getUsername() != null) {
						layout.setText(font, mpPlayer.getUsername());
						float nickOffsetX = (layout.width / 2) - (mpPlayer.getWidth() / 2);
						font.draw(renderer.getBatch(), mpPlayer.getUsername(), mpPlayer.getPosition().x - nickOffsetX, mpPlayer.getPosition().y + 48);
					}
				}	
			}

			if (!player.isDead() && roomServerClient != null) {
				player.render(renderer.getBatch(), delta);
				try {
					layout.setText(font, username);
					nickOffsetX = (layout.width / 2) - (player.getWidth() / 2);
					font.draw(renderer.getBatch(), username, (int) player.getPosition().x - nickOffsetX, (int) player.getPosition().y + 48);
				} catch (NullPointerException exception) {}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}

		renderer.getBatch().end();

		stage.act(delta);
		stage.draw();

		if (debug && player != null) {
			textBatch.begin();
			if (roomServerClient != null && player.getPosition() != null) {
				//				font.draw(textBatch, "Bombs: " + player.getBombs(), 10, Gdx.graphics.getHeight() - 10);
				//				font.draw(textBatch, "Speed: " + player.getSpeed(), 10, Gdx.graphics.getHeight() - 30);
				try {
					font.draw(textBatch, "Ping: " + (int) roomServerClient.getClient().getReturnTripTime() + " ms. Latency: " + roomServerClient.getClient().getReturnTripTime() / 2 + " ms. x: " + (int) player.getPosition().x + " y: " + (int) player.getPosition().y + ", Java Heap: " + javaHeap + "mb" + ", Native Heap: " + nativeHeap + "mb", 7, 20);
					font.draw(textBatch, "Tile: x: " + (int) ((player.getPosition().x + (player.getHeight() / 2)) / mainLayer.getTileHeight()) + " , y: " + (int) ((player.getPosition().y + (player.getWidth() / 2)) / mainLayer.getTileWidth()), 7, 40);
					//				font.draw(textBatch, "Room Port: " + roomServerClient.getPort(), 7, 60);
				} catch (NullPointerException e) {}
			}
			textBatch.end();
		}
		if (usingPhone && touchpad.isVisible()) {
			onScreenTouchPad();
		}
		update(delta);
	}

	public Vector3 convertToWorldCoordinates(Vector3 coordinates) {
		return camera.unproject(coordinates);
	}

	public void onScreenTouchPad() {
		if (player != null && player.getCurrentMove() == 'x') {
			if (touchpad.getKnobPercentY() > 0.55) {
				player.forwardMove = true;
			} else {
				if (player.forwardMove) {
					player.forwardMove = false;
				}
			}

			if (touchpad.getKnobPercentX() < -0.55) {
				player.leftMove = true;
			} else {
				if (player.leftMove) {
					player.leftMove = false;
				}
			}

			if (touchpad.getKnobPercentY() < -0.55) {
				player.backwardMove = true;
			} else {
				if (player.backwardMove) {
					player.backwardMove = false;
				}
			}

			if (touchpad.getKnobPercentX() > 0.55) {
				player.rightMove = true;
			} else {
				if (player.rightMove) {
					player.rightMove = false;
				}
			}
		}
	}

	public void update(float delta) {
		javaHeap = Gdx.app.getJavaHeap() / 1048576;
		nativeHeap = Gdx.app.getNativeHeap() / 1048576;
	}

	public void createNewBomb(Vector2 position) {
		if (roomServerClient != null && roomServerClient.getClient() != null && roomServerClient.getClient().isConnected()) {
			if (player != null) {
				int x = (int) (position.x / mainLayer.getTileWidth());
				int y = (int) (position.y / mainLayer.getTileHeight());
				TiledMapTile currentTile = mainLayer.getCell(x, y).getTile();
				if (currentTile.getId() != 18 && currentTile.getId() == 14) {
					AudioManager.plantBomb.play();
					PacketNewBomb packet = new PacketNewBomb();
					packet.id = roomServerClient.getClient().getID();
					packet.x = x;
					packet.y = y;
					roomServerClient.getClient().sendTCP(packet);
				} else {
					System.out.println("There is already a bomb placed at this location!");
				}
			}
		}
	}

	public MPPlayer getPlayer() {
		return player;
	}

	@Override
	public void resize(int width, int height) {
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		stage.getViewport().update(width, height, true);
		camera.update();
//		create();
		show();
	}

	@Override
	public void dispose() {
		tiledMap.dispose();
		renderer.dispose();
		textBatch.dispose();
		font.dispose();
		if (player != null) {
			player.dispose();
		}
		masterServerClient.getClient().close();
		players.clear();
		textBatch.dispose();
		background.dispose();
	}

	@Override
	public void pause() {
		AudioManager.stopGameMusic();
	}

	@Override
	public void resume() {
		AudioManager.playGameMusic(true);
	}

	@Override
	public void hide() {
		AudioManager.stopGameMusic();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setRoomServerClient(RoomServerClient roomServerClient) {
		this.roomServerClient = roomServerClient;
	}

	public RoomServerClient getRoomServerClient() {
		return roomServerClient;
	}

	public TiledMapTileLayer getTiledLayer() {
		return mainLayer;
	}

	public void playerDie() {
		player.setDead(true);
		bombButton.setVisible(false);
		bombButton.setTouchable(Touchable.disabled);
		if (usingPhone) {
			touchpad.setVisible(false);
			touchpad.setTouchable(Touchable.disabled);
		}
		inputMultiplexer.removeProcessor(player);
		//		player = null;
	}

	public InputMultiplexer getInputMultiplexer() {
		return inputMultiplexer;
	}

	public MasterServerClient getMasterServerClient() {
		return masterServerClient;
	}

	public boolean isUsingPhone() {
		return usingPhone;
	}

	// GestureListener

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		initialScale = zoom;
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		if (x > (0.2415f * stage.getWidth()) && !bombButton.isPressed()) {
			if (usingPhone) {
				if (!touchpad.isTouched()) {
					camera.translate(-deltaX, +deltaY);
					camera.update();
					return true;
				} else {
					return false;
				}
			} else {
				camera.translate(-deltaX, +deltaY);
				camera.update();
			}
		} else {
			return false;
		}
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		if (Gdx.input.getX(0) > (0.2415f * stage.getWidth()) && Gdx.input.getX(1) > (0.2415f * stage.getWidth()) && !bombButton.isPressed()) {
			if (usingPhone) {
				if (!touchpad.isTouched()) {
					zoom = MathUtils.clamp(initialScale * (initialDistance / distance), 0.1f, 1.0f);
					return true;
				} else {
					return false;
				}
			} else {
				zoom = MathUtils.clamp(initialScale * (initialDistance / distance), 0.1f, 1.0f);
				return true;
			}
		}
		return false;
	}

	public void setTiledMap(String mapName) {
		this.mapName = mapName;
		TmxMapLoader.Parameters par = new TmxMapLoader.Parameters();
		par.textureMinFilter = TextureFilter.Linear;
		par.textureMagFilter = TextureFilter.Nearest;
		tiledMap = new TmxMapLoader().load(("maps/multiPlayer/" + mapName + "/" + mapName + ".tmx"), par);
		tiledManager.init(tiledMap);
		renderer.setMap(tiledMap);
		mainLayer = (TiledMapTileLayer) tiledMap.getLayers().get(0);
		screenSize.set((tiledMap.getProperties().get("width", Integer.class) * 32) / 2, (tiledMap.getProperties().get("height", Integer.class) * 32) / 2, 0);
		player.setTiledMainLayer(mainLayer);
		for (OtherPlayer mpPlayer : players.values()) {
			mpPlayer.setTiledManager(tiledManager);
			mpPlayer.setMainLayer(mainLayer);
		}
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// Zoom out
		if (amount > 0 && zoom < 1) {
			zoom += 0.1f;
		}

		// Zoom in
		if (amount < 0 && zoom > 0.1) {
			zoom -= 0.1f;
		}
		return true;
	}

	@Override
	public void pinchStop() {
		// TODO Auto-generated method stub

	}
}