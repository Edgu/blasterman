package lt.edgu.blasterman.screens;

import lt.edgu.blasterman.network.MasterServerClient;
import lt.edgu.blasterman.network.packets.PacketNewMessage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public class LobbyScreen implements Screen {

	private Stage stage;
	private TextureAtlas textureAtlas;
	private Skin skin;
	private Table table;

	private Label lobbyChat;
	private TextField messageField;
	private TextButton sendMessageButton;
	private ScrollPane scrollPane;
	
	private String username;

	private MasterServerClient masterServerClient;

	public LobbyScreen(MasterServerClient bmClient, String username) {
		this.masterServerClient = bmClient;
		this.masterServerClient.setLobbyScreen(this);
		this.username = username;
	}

	public LobbyScreen() {
		this.masterServerClient = new MasterServerClient();
		this.masterServerClient.setLobbyScreen(this);
//		this.bmClient.connect();
		this.username = null;
	}

	@Override
	public void show() {
		stage = new Stage();

		Gdx.input.setInputProcessor(stage);

		textureAtlas = new TextureAtlas("ui/uiskin.atlas");
		skin = new Skin(Gdx.files.internal("ui/loginSkin.json"), textureAtlas);

		table = new Table(skin);
		table.setFillParent(true);

		// TextArea
		lobbyChat = new Label("Connected to lobby.", skin);
		
		scrollPane = new ScrollPane(lobbyChat);

		// TextField
		messageField = new TextField("", skin);

		// Buttons
		sendMessageButton = new TextButton("SEND", skin);
		sendMessageButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				newMessage(messageField.getText(), username, false);
			}
		});

		table.left().top().padTop(5).padLeft(5);
		table.add(lobbyChat).colspan(2).spaceBottom(10).left();
		table.add(scrollPane).fillY().width(20);
		table.row();
		table.add(messageField).left();
		table.add(sendMessageButton).right();
		table.debug();

		stage.addActor(table);
	}

	@Override
	public void render(float delta) {
		stage.act(delta);
		stage.draw();
	}

	public void newMessage(String message, String sender, boolean received) {
		if (received) {
			lobbyChat.setText(lobbyChat.getText() + "\n" + "[" + sender + "] " + message);
		} else if (!message.isEmpty()) {
			messageField.setText("");
			PacketNewMessage packet = new PacketNewMessage();
			packet.sender = sender;
			packet.message = message;
			masterServerClient.getClient().sendTCP(packet);
			lobbyChat.setText(lobbyChat.getText() + "\n" + "[" + sender + "] " + message);
		}
	}
	
	@Override
	public void resize(int width, int height) {
		stage.setViewport(new ExtendViewport(width, height));
		stage.getViewport().update(width, height, true);
		table.getCell(lobbyChat).size((float) (width / 1.30), (float) (height / 1.30));
		table.getCell(messageField).width((float) (width / 1.30) - 60);
	}

	@Override
	public void dispose() {
		stage.dispose();
		textureAtlas.dispose();
		skin.dispose();
		table = null;
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {
		System.out.println("LoginScreen: hide();");
		dispose();
	}
}