package lt.edgu.blasterman.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.tween.SpriteAccessor;

public class SplashScreen implements Screen {

	private SpriteBatch batch;
	private TweenManager tweenManager;

	@Override
	public void show() {
		batch = new SpriteBatch();

		TextureManager.splashScreen.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		TextureManager.pegi.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		tweenManager = new TweenManager();
		Tween.registerAccessor(Sprite.class, new SpriteAccessor());
		Tween.set(TextureManager.splashScreen, SpriteAccessor.ALPHA).target(0).start(tweenManager);
		Tween.set(TextureManager.pegi, SpriteAccessor.ALPHA).target(0).start(tweenManager);

		// Fade in/out
		Tween.to(TextureManager.splashScreen, SpriteAccessor.ALPHA, 2).target(1).repeatYoyo(1, 0.7f).setCallback(new TweenCallback() {
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				AudioManager.pegi_3.play();
				Tween.to(TextureManager.pegi, SpriteAccessor.ALPHA, 2).target(1).repeatYoyo(1, 0.7f).setCallback(new TweenCallback() {
					@Override
					public void onEvent(int arg0, BaseTween<?> arg1) {
						((Game) Gdx.app.getApplicationListener()).setScreen(new ModeScreen("SplashScreen"));
					}

				}).start(tweenManager);
			}
		}).start(tweenManager);

		// Batch renders for a moment pictures if this is not done, if tween.update moved up, then it might crash JVM.
		tweenManager.update(1);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		TextureManager.splashScreen.draw(batch);
		TextureManager.pegi.draw(batch);
		batch.end();

		tweenManager.update(delta * 0.9F);

		if (Gdx.input.justTouched()) {
			AudioManager.pegi_3.stop();
			((Game) Gdx.app.getApplicationListener()).setScreen(new ModeScreen("SplashScreen"));
		}
	}

	@Override
	public void resize(int width, int height) {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {
		dispose();
	}
	
	@Override
	public void dispose() {
		TextureManager.splashScreen.getTexture().dispose();
		TextureManager.pegi.getTexture().dispose();
		batch.dispose();
		tweenManager = null;
	}
}
