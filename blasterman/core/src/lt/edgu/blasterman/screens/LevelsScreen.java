package lt.edgu.blasterman.screens;

import lt.edgu.blasterman.Blasterman;
import lt.edgu.blasterman.TextureManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

public class LevelsScreen implements Screen {

	private Skin skin;
	private Stage stage;
	private Table container;

	public LevelsScreen() {}

	@Override
	public void show() {
		stage = new Stage();
		skin = Blasterman.skin;

		Gdx.input.setInputProcessor(stage);

		container = new Table(skin);
		container.setFillParent(true);
		container.center();
		container.padTop(0.1f * stage.getHeight());

		int buttonWidth = (int) (0.1f * stage.getWidth());
		int buttonHeight = (int) (0.17f * stage.getHeight());

		for (int i = 1; i <= 10; i++) {
			Button levelButton = new Button(skin);
			ButtonStyle style = levelButton.getStyle();
			style.up = null;
			//style.down = null;
			Label label = new Label(Integer.toString(i), skin);
			label.setFontScale(2f);
			label.setAlignment(Align.center);
			// Stack image on button
			levelButton.stack(new Image(TextureManager.grayPlayer.getDrawable()), label).expand().fill();
			if (i % 5 != 0) {
				container.add(levelButton).size(buttonWidth, buttonHeight).spaceRight(0.05f * stage.getWidth());
			} else {
				container.add(levelButton).size(buttonWidth, buttonHeight);
				container.row().space(0.05f * stage.getHeight(), 0, 0.05f * stage.getHeight(), 0);
			}
			levelButton.setName(Integer.toString(i));
			levelButton.addListener(clickListener);
		}

		TextButton backButton = new TextButton("Back", skin);
		backButton.setName("backButton");
		backButton.addListener(clickListener);

		Table backTable = new Table(skin);
		backTable.setBounds(0, 0, stage.getWidth(), 0.2f * stage.getHeight());
		backTable.center();
		backTable.add(backButton).size(buttonWidth * 5, buttonHeight * 0.8f).center();

		stage.addActor(container);
		stage.addActor(backTable);
		stage.addAction(Actions.sequence(Actions.moveTo(-stage.getWidth(), 0), Actions.moveTo(0, 0, .5f)));
	}

	public ClickListener clickListener = new ClickListener() {
		@Override
		public void clicked (InputEvent event, float x, float y) {
			String actorName = event.getListenerActor().getName();
			if (actorName.equals("backButton")) {
				stage.addAction(Actions.sequence(Actions.moveTo(-stage.getWidth(), 0, .5f), Actions.run(new Runnable() {
					@Override
					public void run() {
						((Game) Gdx.app.getApplicationListener()).setScreen(new ModeScreen("LevelsScreen"));
					}
				})));
			} else {
				System.out.println("Level Clicked: " + event.getListenerActor().getName());
				((Game) Gdx.app.getApplicationListener()).setScreen(new SPGameScreen("Level" + event.getListenerActor().getName()));
			}
		}
	};

	@Override
	public void render(float delta) {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	@Override
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
		show();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	public void dispose () {
		stage.dispose();
	}

}