package lt.edgu.blasterman.screens;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.Blasterman;
import lt.edgu.blasterman.network.LoginServerClient;
import lt.edgu.blasterman.network.packets.auth.PacketLogin;
import lt.edgu.blasterman.network.packets.auth.PacketRegister;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class AuthScreen implements Screen {

	private Stage stage;
	private Skin skin;
	private Table table;
	public TextButton loginButton, modeButton, register, registerButton;

	private TextField usernameField;
	private TextField passwordField;
	public TextField statusField;

	private LoginServerClient loginServerClient;

	private boolean firstShow;

	public AuthScreen() {
		loginServerClient = new LoginServerClient(this);
		loginServerClient.connect();
		stage = new Stage();
		skin = Blasterman.skin;
		firstShow = true;
	}

	@Override
	public void show() {
		stage = new Stage();

		table = new Table(skin);
		table.setBounds(0.5f * stage.getWidth(), 0.38f * stage.getHeight(), 0, 0);


		// TextFields
		usernameField = new TextField("Username", skin, "default");
		usernameField.addListener(new ClickListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				usernameField.setText("");
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		passwordField = new TextField("Password", skin, "default");
		passwordField.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				passwordField.setText("");
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		passwordField.setPasswordMode(true);
		passwordField.setPasswordCharacter('*');

		TextureAtlas checkbox = new TextureAtlas(Gdx.files.internal("ui/checkbox.pack"));
		Skin checkboxSkin = new Skin(Gdx.files.internal("ui/checkbox.json"), checkbox);

		final CheckBox remember = new CheckBox(" Remember me?", checkboxSkin);
		Blasterman.getPrefereces().putBoolean("JustChanged", false);
		if (Blasterman.getPrefereces().getBoolean("Remember")) {
			remember.setChecked(true);
			usernameField.setText(Blasterman.getPrefereces().getString("Username"));
			passwordField.setText(Blasterman.getPrefereces().getString("Password"));
		}

		remember.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (!remember.isChecked()) {
					Blasterman.getPrefereces().putBoolean("Remember", false);
					Blasterman.getPrefereces().remove("Username");
					Blasterman.getPrefereces().remove("Password");
				}
				Blasterman.getPrefereces().flush();
			}
		});

		// Buttons
		loginButton = new TextButton("Login", skin, "default");
		loginButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AudioManager.select.play();
				if (loginServerClient.client.isConnected() && ((Game) Gdx.app.getApplicationListener()).getScreen() == AuthScreen.this) {
					if (!usernameField.getText().isEmpty() && !passwordField.getText().isEmpty()) {
						final PacketLogin packet = new PacketLogin();
						if (remember.isChecked()) {
							Blasterman.getPrefereces().putBoolean("Remember", true);
							Blasterman.getPrefereces().putString("Username", usernameField.getText());
							Blasterman.getPrefereces().putString("Password", passwordField.getText());
						}
						Blasterman.getPrefereces().flush();
						packet.password = new String(Hex.encodeHex(DigestUtils.sha512(passwordField.getText())));
						loginButton.setTouchable(Touchable.disabled);
						loginButton.setDisabled(true);
						statusField.setText("STATUS: Connecting to server...");
						packet.username = usernameField.getText();
						loginServerClient.client.sendTCP(packet);
					} else {
						statusField.setText("STATUS: Username or/and Password are empty!");
					}
				}
			}
		});

		modeButton = new TextButton("BACK", skin, "default");
		modeButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AudioManager.select.play();
				stage.addAction(Actions.sequence(Actions.moveTo(stage.getWidth(), 0, 0.5f), Actions.run(new Runnable() {
					@Override
					public void run() {
						((Game) Gdx.app.getApplicationListener()).setScreen(new ModeScreen("AuthScreen"));
					}
				})));
			}
		});

		statusField = new TextField("STATUS: Please enter your login details.", skin, "default");
		statusField.setDisabled(true);

		register = new TextButton("Don't have an account?", skin, "default");
		register.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AudioManager.select.play();
				table.setVisible(false);
				table.setTouchable(Touchable.disabled);
				register();
			}
		});

		float spaceTop = 0.02f * stage.getHeight();
		float textFieldWidth = 0.343f * stage.getWidth();
		float textFieldHeight = 0.07f * stage.getHeight();
		float buttonWidth = 0.13f * stage.getWidth();
		float buttonHeight = 0.13f * stage.getHeight();
		table.add(usernameField).width(textFieldWidth).height(textFieldHeight).fill().colspan(2);
		table.row().spaceTop(spaceTop);
		table.add(passwordField).width(textFieldWidth).height(textFieldHeight).fill().colspan(2);
		table.row().spaceTop(spaceTop);
		table.add(loginButton).width(buttonWidth).height(buttonHeight).fill().right().padRight(20);
		table.add(modeButton).width(buttonWidth).height(buttonHeight).fill().left().padLeft(20);
		table.row().spaceTop(spaceTop).colspan(2);
		table.add(register).width(textFieldWidth).height(0.06f * stage.getHeight());
		table.row().spaceTop(spaceTop);
		table.add(remember).colspan(2);
		table.row().spaceTop(spaceTop * 3).colspan(2);
		table.add(statusField).width(stage.getWidth() * 0.9F);

		stage.addActor(table);

		Gdx.input.setInputProcessor(stage);

		if (firstShow) {
			stage.addAction(Actions.sequence(Actions.moveTo(stage.getWidth(), 0), Actions.moveTo(0, 0, .5f)));
		}

		firstShow = false;
	}

	public void register() {

		final Table table = new Table(skin);
		table.setFillParent(true);
		// TextFields
		final TextField usernameField = new TextField("Enter your username", skin, "default");
		usernameField.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				usernameField.setText("");
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		final TextField passwordField = new TextField("Enter your password", skin, "default");
		passwordField.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				passwordField.setText("");
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		final TextField confirmPassword = new TextField("Confirm password", skin, "default");
		confirmPassword.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				confirmPassword.setText("");
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		final TextField email = new TextField("Enter your e-mail", skin, "default");
		email.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				email.setText("");
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		passwordField.setPasswordMode(true);
		confirmPassword.setPasswordMode(true);

		// Buttons
		registerButton = new TextButton("Register", skin, "default");
		registerButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!usernameField.getText().isEmpty()) {
					if (!passwordField.getText().isEmpty() && !confirmPassword.getText().isEmpty()) {
						if (passwordField.getText().equals(confirmPassword.getText())) {
							Pattern pattern = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
							Matcher matcher = pattern.matcher(email.getText().toUpperCase());
							if (!email.getText().isEmpty() && matcher.matches()) {
								String sha512hex = new String(Hex.encodeHex(DigestUtils.sha512(passwordField.getText())));
								PacketRegister packet = new PacketRegister();
								packet.username = usernameField.getText();
								packet.password = sha512hex;
								packet.confirmPassword = sha512hex;
								packet.email = email.getText();
								loginServerClient.client.sendTCP(packet);
								registerButton.setTouchable(Touchable.disabled);
								registerButton.setDisabled(true);
							} else {
								statusField.setText("STATUS: Invalid Email!");
							}
						} else {
							statusField.setText("STATUS: Passwords do not match!");
						}
					} else {
						statusField.setText("STATUS: Password fields cannot be left empty!");
					}
				} else {
					statusField.setText("STATUS: Username field cannot be left empty!");
				}
			}
		});

		TextButton exitButton = new TextButton("EXIT", skin, "default");
		exitButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});

		statusField = new TextField("STATUS: Please enter your details.", skin, "default");
		statusField.setDisabled(true);

		TextButton loginView = new TextButton("Already have an account?", skin, "default");
		loginView.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				table.remove();
				AuthScreen.this.table.setVisible(true);
				AuthScreen.this.table.setTouchable(Touchable.enabled);
			}
		});

		float spaceTop = 0.018f * stage.getHeight();
		float textFieldWidth = 0.343f * stage.getWidth();
		float textFieldHeight = 0.07f * stage.getHeight();
		float buttonWidth = 0.13f * stage.getWidth();
		float buttonHeight = 0.13f * stage.getHeight();

		table.padTop(spaceTop * 7.5f);
		table.add(usernameField).width(textFieldWidth).height(textFieldHeight).fill().padRight(10);
		table.add(passwordField).width(textFieldWidth).spaceTop(spaceTop).height(textFieldHeight).fill().padLeft(10);
		table.row();
		table.add(email).width(textFieldWidth).spaceTop(spaceTop).height(textFieldHeight).fill().padRight(10);
		table.add(confirmPassword).width(textFieldWidth).spaceTop(spaceTop).height(textFieldHeight).fill().padLeft(10);
		table.row().spaceTop(spaceTop);
		table.add(registerButton).width(buttonWidth).height(buttonHeight).right().padRight(20);
		table.add(exitButton).width(buttonWidth).height(buttonHeight).left().padLeft(20);
		table.row().colspan(2);
		table.add(loginView).spaceTop(spaceTop).width(textFieldWidth).height(buttonHeight / 2.85f);
		table.row().spaceTop(spaceTop * 3).colspan(2);
		table.add(statusField).width(stage.getWidth() * 0.9F);
		stage.addActor(table);
	}

	@Override
	public void render(float delta) {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// If there is no check in place, resize is called when screen is created and,
		// screen transition then is not seen by user because it would call show second, etc time.
		if (stage.getViewport().getScreenWidth() != width || stage.getViewport().getScreenHeight() != height) {
			stage.getViewport().update(width, height, true);
			show();
		}
	}

	@Override
	public void dispose() {
		System.out.println("auth dispose()");
		stage.dispose();
		table = null;
		loginButton = null;
		modeButton = null;
		usernameField = null;
		passwordField = null;
		statusField = null;
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {
		dispose();
	}
}