package lt.edgu.blasterman.screens;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.Blasterman;
import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.network.MPRoom;
import lt.edgu.blasterman.network.MasterServerClient;
import lt.edgu.blasterman.network.RoomServerClient;
import lt.edgu.blasterman.network.packets.PacketCreateRoom;
import lt.edgu.blasterman.network.packets.PacketRequestRoomList;

public class RoomsScreen implements Screen {
	private Stage stage;
	private Table roomsTable;
	private Table buttonsTable;
	private Table intf;
	private Skin skin;

	public List<MPRoom> rooms;
	public ArrayList<MPRoom> tempRoomList;
	private ScrollPane scrollPane;

	private MasterServerClient msClient;
	private MPGameScreen gameScreen;

	public String username;

	private Skin buttonsSkin;
	private TextureAtlas buttonsAtlas;

	// Buttons
	private TextButton createRoom, join, back, refresh;

	// Room Information
	private Label name;
	private Label host;
	private Label port;

	// Player Names
	private Label yellow;
	private Label blue;
	private Label red;
	private Label gray;

	// TEMP VAR
	public int lastSelected = -1;
	public volatile boolean receivedPacket;
	public volatile boolean joined;
	public volatile byte joinCreatedRoom;
	public MPRoom currentRoom;
	public MPRoom lastSelectedRoom;

	// Font
	//	private FreeTypeFontGenerator generator;
	//	private FreeTypeFontParameter parameter;
	//	private BitmapFont font;

	public RoomsScreen(String username) {
		this.username = username;
		msClient = new MasterServerClient();
		msClient.setRoomsScreen(this);
		msClient.connect(username);
		gameScreen = new MPGameScreen(msClient, this, "Bomberman");
		create();
	}

	public RoomsScreen() {
		Random random = new Random();
		this.username = "USERNAME" + (random.nextInt((9000 - 1) + 1) + 1);
		msClient = new MasterServerClient();
		msClient.setRoomsScreen(this);
		msClient.connect(username);
		gameScreen = new MPGameScreen(msClient, this, "Bomberman");
		create();
	}

	public void create() {
		//		generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Pixeleris.ttf"));
		//		parameter = new FreeTypeFontParameter();
		//		parameter.size = 12;
		//		font = generator.generateFont(parameter);
		skin = Blasterman.skin;
		// For buttons
		buttonsAtlas = new TextureAtlas(Gdx.files.internal("ui/buttons.atlas"));
		buttonsSkin = new Skin(Gdx.files.internal("ui/buttons.json"), buttonsAtlas);

		// Buttons
		join = new TextButton("Join Room", buttonsSkin);
		join.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if (rooms.getSelected() != null) {
					joinRoom(rooms.getSelected().port);
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		createRoom = new TextButton("CreateRoom", buttonsSkin);
		createRoom.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				AudioManager.select();
				enableTables(false);
				createRoom();
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		refresh = new TextButton("Refresh", buttonsSkin);
		refresh.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				AudioManager.select();
				msClient.getClient().sendTCP(new PacketRequestRoomList());
				return super.touchDown(event, x, y, pointer, button);
			}

		});

		back = new TextButton("Back", buttonsSkin);
		back.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				AudioManager.select();
				((Game) Gdx.app.getApplicationListener()).setScreen(gameScreen);
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}

	@Override
	public void show() {
		joinCreatedRoom = -1;
		AudioManager.playMenuMusic(true);
		if (stage != null) {
			stage.dispose();
		}
		stage = new Stage(new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));

		intf = new Table(skin);
		intf.setFillParent(true);
		intf.setBackground(TextureManager.roomScreenBG.getDrawable());

		roomsTable = new Table(skin);
		roomsTable.setBounds(328, 263, 376, 252);
		roomsTable.setBounds(0.3F * stage.getWidth(), 0.4F * stage.getHeight(), 0.2F * stage.getWidth(), 0.3F * stage.getHeight());

		buttonsTable = new Table(skin);
		buttonsTable.setBounds(0.7F * stage.getWidth(), 0.5F * stage.getHeight(), 0.2F * stage.getWidth(), 0.2F * stage.getHeight());

		name = new Label("", skin);
		name.setBounds(0.32F * stage.getWidth(), 0.285F * stage.getHeight(), name.getWidth(), name.getHeight());
		name.setFontScale(0.80F);
		host = new Label("", skin);
		host.setBounds(0.32F * stage.getWidth(), 0.265F * stage.getHeight(), host.getWidth(), host.getHeight());
		host.setFontScale(0.80F);
		port = new Label("", skin);
		port.setBounds(0.32F * stage.getWidth(), 0.245F * stage.getHeight(), port.getWidth(), port.getHeight());
		port.setFontScale(0.70F);

		yellow = new Label("", skin);
		yellow.setBounds(0.3F * stage.getWidth(), 0.215F * stage.getHeight(), yellow.getWidth(), yellow.getHeight());
		blue = new Label("", skin);
		blue.setBounds(0.3F * stage.getWidth(), 0.185F * stage.getHeight(), blue.getWidth(), blue.getHeight());
		red = new Label("", skin);
		red.setBounds(0.43F * stage.getWidth(), 0.215F * stage.getHeight(), red.getWidth(), red.getHeight());
		gray = new Label("", skin);
		gray.setBounds(0.43F * stage.getWidth(), 0.185F * stage.getHeight(), gray.getWidth(), gray.getHeight());

		tempRoomList = new ArrayList<MPRoom>();
		rooms = new List<MPRoom>(skin);
		rooms.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				cleanLabels();
				if (rooms.getSelectedIndex() != lastSelected) {
					AudioManager.select();
				}
				if (rooms.getSelected() != null) {
					name.setText(rooms.getSelected().name.trim());
					host.setText(rooms.getSelected().host);
					port.setText(String.valueOf(rooms.getSelected().port));
					for (int i = 0; i < rooms.getSelected().players.size(); i++) {
						String nameNcolor = rooms.getSelected().players.get(i);
						switch (nameNcolor.charAt(nameNcolor.length() - 1)) {
						case '0':
							yellow.setText(nameNcolor.substring(0, nameNcolor.length() - 1));
							break;
						case '1':
							blue.setText(nameNcolor.substring(0, nameNcolor.length() - 1));
							break;
						case '2':
							red.setText(nameNcolor.substring(0, nameNcolor.length() - 1));
							break;
						case '3':
							gray.setText(nameNcolor.substring(0, nameNcolor.length() - 1));
							break;
						default:
							System.out.println("PLAYER COLOR ERROR");
							System.out.println(nameNcolor.charAt(nameNcolor.length() - 1));
							break;
						}
					}
				}
				lastSelected = rooms.getSelectedIndex();
			}
		});

		msClient.getClient().sendTCP(new PacketRequestRoomList());

		scrollPane = new ScrollPane(rooms, skin);
		scrollPane.setFadeScrollBars(false);

		float spaceBottom = 0.03f * stage.getHeight();
		float buttonWidth = 0.20f * stage.getWidth();
		float buttonHeight = 0.08f * stage.getHeight();
		roomsTable.add(scrollPane).width(0.29F * stage.getWidth()).height(0.3F * stage.getHeight());
		buttonsTable.top();
		buttonsTable.add(join).size(buttonWidth, buttonHeight).spaceBottom(spaceBottom);
		buttonsTable.row();
		buttonsTable.add(createRoom).size(buttonWidth, buttonHeight).spaceBottom(spaceBottom);
		buttonsTable.row();
		buttonsTable.add(refresh).size(buttonWidth, buttonHeight).spaceBottom(spaceBottom);
		buttonsTable.row();

		try {
			if (gameScreen.getRoomServerClient().getClient().isConnected() && currentRoom != null) {
				buttonsTable.add(back).size(buttonWidth, buttonHeight).spaceTop(0.15f * stage.getHeight());
			}
		} catch (NullPointerException e) {}

		stage.addActor(intf);
		stage.addActor(roomsTable);
		stage.addActor(buttonsTable);
		stage.addActor(name);
		stage.addActor(host);
		stage.addActor(port);

		stage.addActor(yellow);
		stage.addActor(blue);
		stage.addActor(red);
		stage.addActor(gray);

		Gdx.input.setInputProcessor(stage);
	}

	public void enableTables(boolean enable) {
		Touchable touchable = (enable) ? Touchable.enabled : Touchable.disabled;

		// Hide/Show
		intf.setVisible(enable);
		roomsTable.setVisible(enable);
		buttonsTable.setVisible(enable);
		name.setVisible(enable);
		host.setVisible(enable);
		port.setVisible(enable);
		yellow.setVisible(enable);
		blue.setVisible(enable);
		red.setVisible(enable);
		gray.setVisible(enable);

		// Touchable
		intf.setTouchable(touchable);
		roomsTable.setTouchable(touchable);
		buttonsTable.setTouchable(touchable);
		name.setTouchable(touchable);
		host.setTouchable(touchable);
		port.setTouchable(touchable);
		yellow.setTouchable(touchable);
		blue.setTouchable(touchable);
		red.setTouchable(touchable);
		gray.setTouchable(touchable);
	}

	public void createRoom() {
		final Table crTable = new Table();
		final Table thumbnail = new Table();
		thumbnail.setBounds(0.527f * Gdx.graphics.getWidth(), 0.324f * Gdx.graphics.getHeight(), (0.29F * Gdx.graphics.getWidth()), (0.3F * Gdx.graphics.getHeight()));
		thumbnail.add(TextureManager.bombermanThumbnail).width(0.29F * stage.getWidth()).height(0.3F * stage.getHeight());
		crTable.setFillParent(true);

		float spaceTop = 0.018f * stage.getHeight();
		float textFieldWidth = 0.343f * stage.getWidth();
		float textFieldHeight = 0.07f * stage.getHeight();
		float buttonWidth = 0.13f * stage.getWidth();
		float buttonHeight = 0.13f * stage.getHeight();

		final TextField roomName = new TextField(username + "'s Room", skin);
		roomName.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				roomName.setText("");
				super.clicked(event, x, y);
			}
		});
		final List<String> mapList = new List<String>(skin);
		mapList.setItems(new String[] {"Bomberman", "Space_Invaders"});

		mapList.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				thumbnail.clear();
				String mapName = mapList.getSelected().toString();
				Image thumbnailImage = null;
				switch (mapName) {
				case "Bomberman":
					thumbnailImage = TextureManager.bombermanThumbnail;
					break;
				case "Space_Invaders":
					thumbnailImage = TextureManager.spaceInvadersThumbnail;
					break;
				default:
					break;
				}
				thumbnail.add(thumbnailImage).width(0.29F * stage.getWidth()).height(0.3F * stage.getHeight());
			}
		});

		final TextField statusField = new TextField("Please enter/select room configuration.", skin);
		statusField.setDisabled(true);

		final TextButton create = new TextButton("Create", buttonsSkin);
		create.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				create.setDisabled(true);
				AudioManager.select.play();
				if (!roomName.getText().isEmpty()) {
					if (msClient.getClient() != null && msClient.getClient().isConnected()) {
						PacketCreateRoom packet = new PacketCreateRoom();
						packet.host = username;
						packet.name = roomName.getText();
						packet.mapName = mapList.getSelected().toString();
						msClient.getClient().sendTCP(packet);
					}
				} else {
					statusField.setText("Please enter a name for your room!");
				}
			}
		});

		TextButton cancel = new TextButton("Cancel", buttonsSkin);
		cancel.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AudioManager.select.play();
				thumbnail.setVisible(false);
				thumbnail.setTouchable(Touchable.disabled);
				crTable.setVisible(false);
				crTable.setTouchable(Touchable.disabled);
				enableTables(true);

			}
		});

		crTable.padTop(spaceTop * 11f);
		crTable.add(roomName).width(textFieldWidth).height(textFieldHeight).fill().colspan(2);
		crTable.row().spaceTop(spaceTop);
		crTable.add(mapList).width(0.29F * stage.getWidth()).height(0.3F * stage.getHeight());
		crTable.add().width(0.29F * stage.getWidth()).height(0.3F * stage.getHeight());
		crTable.row().spaceTop(spaceTop);
		crTable.add(create).width(buttonWidth).height(buttonHeight).right().padRight(20);
		crTable.add(cancel).width(buttonWidth).height(buttonHeight).left().padLeft(20);
		crTable.row().spaceTop(spaceTop * 2.5f).colspan(2);
		crTable.add(statusField).width(stage.getWidth() * 0.9F);
		stage.addActor(crTable);
		stage.addActor(thumbnail);
	}

	public void joinRoom(int port) {
		AudioManager.select();
		// Doing this because when last player joins the room, rooms list get refreshed so he doesn't see the room anymore
		// thus he is not able to connect to the rooms.getSelected() so we save last selection and then join to that room
		// if joined == true.
		lastSelectedRoom = rooms.getSelected();
		
		joined = (receivedPacket) ? true : false;
		
		if (gameScreen.getRoomServerClient() == null) {
			gameScreen.setRoomServerClient(new RoomServerClient(RoomsScreen.this, gameScreen, port));
			gameScreen.getRoomServerClient().connect();
		} else if (rooms.getSelected().port != gameScreen.getRoomServerClient().getPort()) {
			gameScreen.getRoomServerClient().getClient().close();
			gameScreen.setRoomServerClient(new RoomServerClient(RoomsScreen.this, gameScreen, port));
			gameScreen.create();
			gameScreen.getRoomServerClient().connect();
		} else if (port == gameScreen.getRoomServerClient().getPort() && gameScreen.getRoomServerClient().getClient().isConnected()) {
			// DO NOTHUING
		}
	}

	public void cleanLabels() {
		name.setText("");
		host.setText("");
		port.setText("");
		yellow.setText("");
		blue.setText("");
		red.setText("");
		gray.setText("");
	}

	// Game logic
	public void update(float delta) {
		if (joined) {
			currentRoom = lastSelectedRoom;
			gameScreen.setTiledMap(currentRoom.mapName);
			((Game) Gdx.app.getApplicationListener()).setScreen(gameScreen);
			System.out.println("Change screen to gameScreen");
			joined = false;
		}

		if (joinCreatedRoom == 1) {
			if (rooms.getSelected() != null) {
				System.out.println("join created room");
				joinRoom(rooms.getSelected().port);
				joinCreatedRoom = -1;
			}
		}
	}

	@Override
	public void render(float delta) {
		update(delta);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
		show();
	}

	@Override
	public void hide() {
		AudioManager.stopMenuMusic();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		stage.dispose();
		AudioManager.dispose();
	}
}
