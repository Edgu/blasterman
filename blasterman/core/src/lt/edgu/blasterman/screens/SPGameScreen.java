package lt.edgu.blasterman.screens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.TiledManager;
import lt.edgu.blasterman.entity.Bomb;
import lt.edgu.blasterman.entity.Entity;
import lt.edgu.blasterman.entity.SPPlayer;
import lt.edgu.blasterman.entity.npc.NPC;
import lt.edgu.blasterman.entity.npc.Test;
import lt.edgu.blasterman.util.Node;
import lt.edgu.blasterman.util.UI;

public class SPGameScreen implements Screen, GestureDetector.GestureListener, InputProcessor {
	private TiledManager tiledManager;
	public TiledMap tiledMap;
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera camera;
	private Vector3 screenSize;
	private SpriteBatch textBatch;
	private volatile TiledMapTileLayer mainLayer;
	private String username;
	public Vector2 deathCamera;
	public Stage stage;
	private TextureAtlas textureAtlas;
	public Skin skin;
	private TextureAtlas buttonsAtlas;
	private Skin buttonsSkin;
	public Touchpad touchpad;
	private volatile InputMultiplexer inputMultiplexer;
	private GestureDetector gestureDetector;

	// Temp Var
	private boolean usingPhone;
	private SpriteBatch background;
	private String mapName;

	// TextButton
	public TextButton backButton;
	public TextButton bombButton;

	private float zoom = 1.0f;
	private float initialScale = 1.0f;

	private SPPlayer player;

	public static ArrayList<Vector2> willExplode = new ArrayList<Vector2>();

	// Temp Variable
	private ArrayList<NPC> npc;
	private Comparator<Node> nodeSorter = new Comparator<Node>() {
		public int compare(Node one, Node two) {
			if (two.fCost < one.fCost) {
				return +1;
			} else if (two.fCost > one.fCost)  {
				return -1;
			} else {
				return 0;
			}
		}
	};

	public SPGameScreen() {
		this.username = "UNNAMED";
		this.mapName = "Bomberman";
		create();
	}

	public SPGameScreen(String levelName) {
		this.username = "UNNAMED";
		this.mapName = levelName;
		create();
	}

	void create() {
		buttonsAtlas = new TextureAtlas(Gdx.files.internal("ui/button_trans.atlas"));
		buttonsSkin = new Skin(Gdx.files.internal("ui/buttons.json"), buttonsAtlas);
		textureAtlas = new TextureAtlas("ui/uiskin.atlas");
		skin = new Skin(Gdx.files.internal("ui/loginSkin.json"), textureAtlas);
		TmxMapLoader.Parameters par = new TmxMapLoader.Parameters();
		par.textureMinFilter = TextureFilter.Linear;
		par.textureMagFilter = TextureFilter.Nearest;
		tiledMap = new TmxMapLoader().load("maps/singlePlayer/" + mapName + "/" + mapName + ".tmx", par);
		tiledManager = new TiledManager();
		tiledManager.init(tiledMap);
		renderer = new OrthogonalTiledMapRenderer(tiledMap);
		mainLayer = (TiledMapTileLayer) tiledMap.getLayers().get(0);
		TiledMapTileLayer npcLayer = (TiledMapTileLayer) tiledMap.getLayers().get(1);
		camera = new OrthographicCamera();
		screenSize = new Vector3((tiledMap.getProperties().get("width", Integer.class) * 32) / 2, (tiledMap.getProperties().get("height", Integer.class) * 32) / 2, 0);

		player = new SPPlayer(this);

		npc = new ArrayList<NPC>();
		//		int count = 0;
		for (int rows = 0; rows < npcLayer.getWidth(); rows++) {
			for (int columns = 0; columns < npcLayer.getHeight(); columns++) {
				try {
					if (npcLayer.getCell(rows, columns).getTile().getProperties().get("name").equals("spawnTile")) {
						try {
							//							if (count > 0) continue;
							//							count++;
							npc.add(new Test(this, (int) (rows * npcLayer.getTileWidth()), (int) (columns * npcLayer.getTileHeight())));
						} catch (Exception exception) {
							exception.printStackTrace();
						}
					}
				} catch (NullPointerException e) {}
			}
		}

		textBatch = new SpriteBatch();
		background = new SpriteBatch();
		stage = new Stage();

		backButton = new TextButton("Exit", skin);
		backButton.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				((Game) Gdx.app.getApplicationListener()).setScreen(new LevelsScreen());
				return super.touchDown(event, x, y, pointer, button);
			}
		});

		backButton.setSize(0.1f * stage.getWidth(), 0.1f * stage.getHeight());

		switch (Gdx.app.getType()) {
		case Android:
			usingPhone = true;
			Skin touchpadSkin = new Skin();
			touchpadSkin.add("touchBackground", new Texture("ui/touchBackground.png"));
			touchpadSkin.add("touchKnob", new Texture("ui/touchKnob.png"));
			TouchpadStyle touchpadStyle = new TouchpadStyle();
			Drawable touchBackground = touchpadSkin.getDrawable("touchBackground");
			Drawable touchKnob = touchpadSkin.getDrawable("touchKnob");
			touchpadStyle.background = touchBackground;
			touchpadStyle.knob = touchKnob;
			touchpad = new Touchpad(0, touchpadStyle);
			touchpad.setBounds(50, 50, 200, 200);
			touchpad.setVisible(true);
			touchpad.setTouchable(Touchable.enabled);
			break;
		case Applet:
			usingPhone = false;
			break;
		case Desktop:
			usingPhone = false;
			break;
		case HeadlessDesktop:
			usingPhone = false;
			break;
		case WebGL:
			usingPhone = false;
			break;
		case iOS:
			usingPhone = true;
			break;
		default:
			usingPhone = false;
			break;
		}

		bombButton = new TextButton("BOMB", buttonsSkin);
		bombButton.setVisible(true);
		bombButton.setTouchable(Touchable.enabled);
		bombButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AudioManager.plantBomb.play();
				switch (player.getCurrentMove()) {
				case 'w':
					createNewBomb(new Vector2((int) ((player.getPosition().x + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) (player.getPosition().y / mainLayer.getTileHeight()) * 32));
					break;
				case 'a':
					createNewBomb(new Vector2((int) (((player.getPosition().x + 15) + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) ((player.getPosition().y + player.getHeight() / 2) / mainLayer.getTileHeight()) * 32));
					break;
				case 's':
					createNewBomb(new Vector2((int) ((player.getPosition().x + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) ((player.getPosition().y + 31) / mainLayer.getTileHeight()) * 32));
					break;
				case 'd':
					createNewBomb(new Vector2((int) (((player.getPosition().x - 16) + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) ((player.getPosition().y + player.getHeight() / 2) / mainLayer.getTileHeight()) * 32));
					break;
				case 'x':
					createNewBomb(new Vector2((int) ((player.getPosition().x + player.getWidth() / 2) / mainLayer.getTileWidth()) * 32,
							(int) (player.getPosition().y / mainLayer.getTileHeight()) * 32));
					break;
				}
				super.clicked(event, x, y);
			}
		});

		Table bombTable = new Table(skin);
		bombTable.setPosition(0.87f * stage.getWidth(), 0.19f * stage.getHeight());
		bombTable.add(bombButton).size(0.15f * stage.getWidth(), 0.2f * stage.getHeight());

		stage.addActor(backButton);
		stage.addActor(bombTable);
	}

	@Override
	public void show() {
		AudioManager.playGameMusic(true);

		gestureDetector = new GestureDetector(this);
		inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(gestureDetector);
		inputMultiplexer.addProcessor(stage);
		inputMultiplexer.addProcessor(this);
		inputMultiplexer.addProcessor(player);
		Gdx.input.setInputProcessor(inputMultiplexer);

		if (usingPhone) {
			stage.addActor(touchpad);
		}

		camera.position.set(screenSize.x - ((0.2415f * Gdx.graphics.getWidth()) / 2), screenSize.y, 0);
	}

	public void gameOver(String winner) {
		UI.gameFinish(winner, skin, stage);
	}

	public void updateCam() {
		if (camera.position.x <= (-stage.getWidth() / 2)) {
			camera.position.set(screenSize.x - ((0.2415f * Gdx.graphics.getWidth()) / 2), screenSize.y, 0);
		}
	}

	// A* Path Finding Algorithm
	public List<Node> findPath(Vector2 start, Vector2 finish) {
		// List of nodes that need to be checked.
		List<Node> uncheckedNodes = new ArrayList<Node>();
		// List of nodes that have been checked.
		List<Node> checkedNodes = new ArrayList<Node>();
		// Current Node
		Node current = new Node(start, null, 0, getDistance(start, finish));
		// Add Current Node
		uncheckedNodes.add(current);

		// Search for path, if openList is empty means there is no path.
		while (uncheckedNodes.size() > 0) {
			Collections.sort(uncheckedNodes, nodeSorter);
			current = uncheckedNodes.get(0);
			if (current.position.equals(finish)) {
				List<Node> path = new ArrayList<Node>();
				while (current.parent != null) {
					path.add(current);
					current = current.parent;
				}
				// Release memory
				uncheckedNodes.clear();
				checkedNodes.clear();
				// Path found, return;
				return path;
			}
			uncheckedNodes.remove(current);
			checkedNodes.add(current);

			/* 
			 * Start from one because we cannot move diagonally.
			 * Skip iterations that are divisible by 2 because its
			 * either a middle tile or diagonal.
			 */
			for (int i = 1; i < 9; i++) {
				if (i % 2 == 0) {
					continue;
				}
				int x = (int) current.position.x;
				int y = (int) current.position.y;
				// Adjacent tiles or nodes //
				int xi = (i % 3) - 1;
				int yi = (i / 3) - 1;
				// ----------------------- //
				TiledMapTile atTile = mainLayer.getCell(x + xi, y + yi).getTile();
				Vector2 at = new Vector2(x + xi, y + yi);
				if (atTile == null) {
					continue;
				}
				if (isTileSolid(at)) {
					continue;
				}
				double gCost = current.gCost + getDistance(current.position, at);
				double hCost = getDistance(at, finish);
				Node node = new Node(at, current, gCost, hCost);
				if (vectorInList(checkedNodes, at) && gCost >= node.gCost) {
					continue;
				}
				if (!vectorInList(uncheckedNodes, at) || gCost < node.gCost) {
					uncheckedNodes.add(node);
				}
			}
		}
		checkedNodes.clear();
		return null;
	}

	/*
	 * Find shortest path to the tile.
	 * 		Steps:
	 * 				- Get all tiles of given type
	 * 				- Get all possible paths to the surrounding tiles. NESW
	 * 				- Sort paths
	 * 
	 * NOTE: Tile list has to be supplied, can be save tile list or any tile list.
	 * 			Safe tile list may be used when looking for a path and any tile list
	 * 			may be used when trying to move to safety.
	 * 
	 */
	public List<Node> testFindPathTile(TiledMapTile tile, Entity entity, ArrayList<Vector2> tileList) {
		ArrayList<Vector2> surroundingTiles = surroundingTiles(tileList);
		List<List<Node>> paths = new ArrayList<List<Node>>();
		for (int i = 0; i < surroundingTiles.size(); i++) {
			if (getDistance(entity.getRoundedPosition(), surroundingTiles.get(i)) == 0) {
				continue;
			}
			List<Node> path = findPath(entity.getRoundedPosition(), new Vector2(Math.round(surroundingTiles.get(i).x) / 32, Math.round(surroundingTiles.get(i).y) / 32));
			if (path != null && path.size() > 0) {
				paths.add(path);
			}
		}
		return sortPaths(paths);
	}

	public ArrayList<Vector2> getSurroundingTiles(Vector2 tile) {
		ArrayList<Vector2> surroundingTiles = new ArrayList<Vector2>();
		surroundingTiles.add(new Vector2(tile.x, tile.y + 32));
		surroundingTiles.add(new Vector2(tile.x + 32, tile.y));
		surroundingTiles.add(new Vector2(tile.x, tile.y - 32));
		surroundingTiles.add(new Vector2(tile.x - 32, tile.y));
		return surroundingTiles;
	}

	public ArrayList<Vector2> surroundingTiles(ArrayList<Vector2> tiles) {
		ArrayList<Vector2> tileList = new ArrayList<Vector2>();
		for (Vector2 tile : tiles) {
			ArrayList<Vector2> surroundingTiles = getSurroundingTiles(tile);
			for (Vector2 surroundingTile : surroundingTiles) {
				if (!isTileSolid(surroundingTile)) {
					tileList.add(surroundingTile);
				}
			}
		}
		return tileList;
	}

	/*
	 * Finds path to the nearest safe tile.
	 */
	public List<Node> findSavePath(Entity entity) {
		ArrayList<Vector2> tiles = safeTileList(tiledManager.grassTile);
		List<List<Node>> paths = new ArrayList<List<Node>>();
		for (int i = 0; i < tiles.size(); i++) {
			if (getDistance(entity.getRoundedPosition(), tiles.get(i)) == 0) {
				continue;
			}
			List<Node> path = findPath(entity.getRoundedPosition(), new Vector2(tiles.get(i).x / 32, tiles.get(i).y / 32));
			if (path != null && path.size() > 0) {
				paths.add(path);
			}
		}
		return sortPaths(paths);
	}

	public ArrayList<Vector2> tileList(TiledMapTile tile) {
		ArrayList<Vector2> tiles = new ArrayList<Vector2>();
		try {
			for (int rows = 0; rows < mainLayer.getWidth(); rows++) {
				for (int columns = 0; columns < mainLayer.getHeight(); columns++) {
					if (mainLayer.getCell(rows, columns).getTile() == tile) {
						tiles.add(new Vector2(rows * 32, columns * 32));
					}
				}
			}
		} catch (NullPointerException e) {}
		return tiles;
	}

	private ArrayList<Vector2> safeTileList(TiledMapTile tile) {
		ArrayList<Vector2> tiles = tileList(tile);
		ArrayList<Vector2> safeTiles = new ArrayList<Vector2>();
		try {
			for (int i = 0; i < tiles.size(); i++) {
				if (!isRisky(tiles.get(i))) {
					safeTiles.add(tiles.get(i));
				}
			}
		} catch (NullPointerException e) {}
		return safeTiles;
	}

	// Bubble sort paths.
	public static List<Node> sortPaths(List<List<Node>> paths) {
		int size = paths.size();
		List<Node> temp = null;

		for (int i = 0; i < size; i++) {
			for (int j = 1; j < (size - i); j++) {
				if (paths.get(j - 1) != null && paths.get(j) != null) {
					if (paths.get(j - 1).size() > paths.get(j).size()) {
						temp = paths.get(j - 1);
						paths.set(j - 1, paths.get(j));
						paths.set(j, temp);
					}
				}
			}
		}

		for (int i = 0; i < paths.size(); i++) {
			if (paths.get(0) != null && paths.size() > 0) {
				return paths.get(0);
			}
		}

		return null;
	}

	private boolean vectorInList(List<Node> list, Vector2 vector) {
		for (Node node : list) {
			if (node.position.equals(vector)) {
				return true;
			}
		}
		return false;
	}

	private double getDistance(Vector2 start, Vector2 finish) {
		double dx = start.x - finish.x;
		double dy = start.y - finish.y;
		return Math.sqrt(dx * dx + dy * dy);
	}

	public boolean isTileSolid(Vector2 position) {
		Cell cell = mainLayer.getCell((int) position.x, (int) position.y);
		return cell != null && cell.getTile() != null && (cell.getTile().getProperties().containsKey("solid") || cell.getTile().getProperties().get("name").equals("explosion"));
	}

	//	private int getTileID(Vector2 position) {
	//		return mainLayer.getCell((int) (position.x / mainLayer.getTileWidth()), (int) (position.y / mainLayer.getTileHeight())).getTile().getId();
	//	}

	public boolean isRisky(Vector2 position) {
		if (isTileSolid(new Vector2(position.x / 32, position.y / 32))) {
			return true;
		}
		
		for (int i = 0; i < willExplode.size(); i++) {
			if (willExplode.get(i).x == position.x && willExplode.get(i).y == position.y) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void render(float delta) {
		player.setBombs(5);
		player.setSpeed(player.getSpeed() + 32);
		
		camera.zoom = zoom;

		background.begin();
		background.draw(TextureManager.grass, 0, 0, TextureManager.grass.getWidth(), TextureManager.grass.getHeight(), (int) stage.getWidth(), (int) stage.getHeight());
		background.end();

		updateCam();
		camera.update();

		renderer.setView(camera);
		renderer.render();

		renderer.getBatch().setProjectionMatrix(camera.combined);

		renderer.getBatch().begin();

		for (int i = 0; i < npc.size(); i++) {
			if (!npc.get(i).isDead()) {
				npc.get(i).render(renderer.getBatch(), delta);
			} else {
				npc.remove(i);
			}
		}

		for (int i = 0; i < willExplode.size(); i++) {
			renderer.getBatch().draw(TextureManager.x, willExplode.get(i).x, willExplode.get(i).y, 32, 32);
		}

		if (!player.isDead()) {
			player.render(renderer.getBatch(), delta);
		}

		renderer.getBatch().end();

		stage.act(delta);
		stage.draw();

		if (usingPhone && touchpad.isVisible()) {
			onScreenTouchPad();
		}

		update(delta);
	}

	private void update(float delta) {

	}

	public void onScreenTouchPad() {
		if (player != null && player.getCurrentMove() == 'x') {
			if (touchpad.getKnobPercentY() > 0.55) {
				player.forwardMove = true;
			} else {
				if (player.forwardMove) {
					player.forwardMove = false;
				}
			}

			if (touchpad.getKnobPercentX() < -0.55) {
				player.leftMove = true;
			} else {
				if (player.leftMove) {
					player.leftMove = false;
				}
			}

			if (touchpad.getKnobPercentY() < -0.55) {
				player.backwardMove = true;
			} else {
				if (player.backwardMove) {
					player.backwardMove = false;
				}
			}

			if (touchpad.getKnobPercentX() > 0.55) {
				player.rightMove = true;
			} else {
				if (player.rightMove) {
					player.rightMove = false;
				}
			}
		}
	}

	public void createNewBomb(Vector2 position) {
		int x = (int) (position.x / mainLayer.getTileWidth());
		int y = (int) (position.y / mainLayer.getTileHeight());
		AudioManager.plantBomb.play();
		if (placeBomb(mainLayer.getCell(x, y).getTile().getId(), 14, 10, 11, 12, 13, 17)) {
			mainLayer.getCell(x, y).setTile(tiledManager.bombTile);
			new Bomb((new Random().nextInt((69569596 - 24) + 786) + 25), x, y, player.getBombStrenght(), this);
		}
	}

	private boolean placeBomb(int targetTileID, int... tileID) {
		for (int tile : tileID) {
			if (targetTileID == tile) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void resize(int width, int height) {
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		stage.getViewport().update(width, height, true);
		camera.update();
		show();
	}

	@Override
	public void dispose() {
		tiledMap.dispose();
		renderer.dispose();
		textBatch.dispose();
		if (player != null) {
			player.dispose();
		}
		textBatch.dispose();
		background.dispose();
	}

	@Override
	public void pause() {
		AudioManager.stopGameMusic();
	}

	@Override
	public void resume() {
		AudioManager.playGameMusic(true);
	}

	@Override
	public void hide() {
		AudioManager.stopGameMusic();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public TiledMapTileLayer getTiledLayer() {
		return mainLayer;
	}

	public void playerDie() {
		player.setDead(true);
		bombButton.setVisible(false);
		bombButton.setTouchable(Touchable.disabled);
		if (usingPhone) {
			touchpad.setVisible(false);
			touchpad.setTouchable(Touchable.disabled);
		}
		inputMultiplexer.removeProcessor(player);
	}

	public InputMultiplexer getInputMultiplexer() {
		return inputMultiplexer;
	}

	public boolean isUsingPhone() {
		return usingPhone;
	}

	// GestureListener

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		initialScale = zoom;
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		if (Gdx.input.isButtonPressed(Buttons.RIGHT)) return false;
		if (x > (0.2415f * stage.getWidth()) && !bombButton.isPressed()) {
			if (usingPhone) {
				if (!touchpad.isTouched()) {
					camera.translate(-deltaX, +deltaY);
					camera.update();
					return true;
				} else {
					return false;
				}
			} else {
				camera.translate(-deltaX, +deltaY);
				camera.update();
			}
		} else {
			return false;
		}
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		if (Gdx.input.getX(0) > (0.2415f * stage.getWidth()) && Gdx.input.getX(1) > (0.2415f * stage.getWidth()) && !bombButton.isPressed()) {
			if (usingPhone) {
				if (!touchpad.isTouched()) {
					zoom = MathUtils.clamp(initialScale * (initialDistance / distance), 0.1f, 1.0f);
					return true;
				} else {
					return false;
				}
			} else {
				zoom = MathUtils.clamp(initialScale * (initialDistance / distance), 0.1f, 1.0f);
				return true;
			}
		}
		return false;
	}

	public void setTiledMap(String mapName) {
		this.mapName = mapName;
		TmxMapLoader.Parameters par = new TmxMapLoader.Parameters();
		par.textureMinFilter = TextureFilter.Linear;
		par.textureMagFilter = TextureFilter.Nearest;
		tiledMap = new TmxMapLoader().load(("maps/" + mapName + "/" + mapName + ".tmx"), par);
		tiledManager.init(tiledMap);
		renderer.setMap(tiledMap);
		mainLayer = (TiledMapTileLayer) tiledMap.getLayers().get(0);
		screenSize.set((tiledMap.getProperties().get("width", Integer.class) * 32) / 2, (tiledMap.getProperties().get("height", Integer.class) * 32) / 2, 0);
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		if (character == 'f' || character == 'F') {
			List<Node> path = findSavePath(player);
			try {
				for (Node node : path) {
					mainLayer.getCell((int) node.position.x, (int) node.position.y).setTile(tiledManager.additionalBombPowerUp);
				}
			} catch (NullPointerException e) {}
		}
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		Vector3 pos = new Vector3(camera.unproject(new Vector3(screenX, screenY, 0)));
		pos = new Vector3(Math.round(pos.x) / 32, Math.round(pos.y) / 32, 0);
		try {
			switch (button) {
			case Buttons.LEFT:
				mainLayer.getCell((int) pos.x, (int) pos.y).setTile(tiledManager.boxTile);
				break;
			case Buttons.RIGHT:
				mainLayer.getCell((int) pos.x, (int) pos.y).setTile(tiledManager.grassTile);
				break;
			default:
				break;
			}
		} catch (NullPointerException e) {}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		Vector3 pos = new Vector3(camera.unproject(new Vector3(screenX, screenY, 0)));
		pos = new Vector3(Math.round(pos.x) / 32, Math.round(pos.y) / 32, 0);
		if (Gdx.input.isButtonPressed(Buttons.RIGHT)) {
			try {
				mainLayer.getCell((int) pos.x, (int) pos.y).setTile(tiledManager.grassTile);
			} catch (NullPointerException e) {}
		}
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	public SPPlayer getPlayer() {
		return player;
	}

	@Override
	public boolean scrolled(int amount) {
		// Zoom out
		if (amount > 0 && zoom < 1) {
			zoom += 0.1f;
		}

		// Zoom in
		if (amount < 0 && zoom > 0.1) {
			zoom -= 0.1f;
		}
		return true;
	}

	public TiledManager getTiledManager() {
		return tiledManager;
	}

	public ArrayList<NPC> getNPC() {
		return npc;
	}

	@Override
	public void pinchStop() {
		// TODO Auto-generated method stub

	}
}