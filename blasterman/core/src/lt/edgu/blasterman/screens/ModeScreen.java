package lt.edgu.blasterman.screens;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.Blasterman;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class ModeScreen implements Screen {

	private Stage stage;
	private Skin skin;
	private Table table;
	public TextButton singlePlayer, multiPlayer, exitButton;
	private String creator;

	public ModeScreen(String creator) {
		stage = new Stage();
		skin = Blasterman.skin;
		this.creator = creator;
	}

	@Override
	public void show() {
		table = new Table(skin);
		table.setBounds(0.5f * stage.getWidth(), 0.47f * stage.getHeight(), 0, 0);


		singlePlayer = new TextButton("Single Player", skin, "default");
		singlePlayer.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AudioManager.select.play();
				stage.addAction(Actions.sequence(Actions.moveTo(stage.getWidth(), 0, .5f), Actions.run(new Runnable() {
					@Override
					public void run() {
						((Game) Gdx.app.getApplicationListener()).setScreen(new LevelsScreen());
					}
				})));
			}
		});

		multiPlayer = new TextButton("Multi Player\n(Requires Internet)", skin, "default");
		multiPlayer.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				AudioManager.select.play();
				stage.addAction(Actions.sequence(Actions.moveTo(-stage.getWidth(), 0, .5f), Actions.run(new Runnable() {
					@Override
					public void run() {
						((Game) Gdx.app.getApplicationListener()).setScreen(new AuthScreen());
					}
				})));
			}
		});

		exitButton = new TextButton("EXIT", skin, "default");
		exitButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});

		float spaceTop = 0.02f * stage.getHeight();
		float buttonWidth = 0.18f * stage.getWidth();
		float buttonHeight = 0.17f * stage.getHeight();

		table.add(singlePlayer).width(buttonWidth).height(buttonHeight).fill().spaceRight(spaceTop * 5);
		table.add(multiPlayer).width(buttonWidth).height(buttonHeight).fill().spaceLeft(spaceTop * 5);
		table.row().spaceTop(spaceTop * 2.5f);
		table.add(exitButton).width(buttonWidth * 2).height(buttonHeight / 1.5f).fill().colspan(2);
		table.row().spaceTop(spaceTop);
		stage.addActor(table);
		Gdx.input.setInputProcessor(stage);
		
		switch (creator) {
		case "LevelsScreen":
			stage.addAction(Actions.sequence(Actions.moveTo(stage.getWidth(), 0), Actions.moveTo(0, 0, .5f)));
			break;
		case "SplashScreen":
			stage.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(1f)));
			break;
		case "AuthScreen":
			stage.addAction(Actions.sequence(Actions.moveTo(-stage.getWidth(), 0), Actions.moveTo(0, 0, .5f)));
			System.out.println("asdasdads");
			break;
		default:
			break;
		}
	}

	@Override
	public void render(float delta) {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		if (stage.getViewport().getScreenWidth() != width || stage.getViewport().getScreenHeight() != height) {
			stage.getViewport().update(width, height, true);
			show();
		}
	}

	@Override
	public void dispose() {
		stage.dispose();
		table = null;
		exitButton = null;
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {
		dispose();
	}
}