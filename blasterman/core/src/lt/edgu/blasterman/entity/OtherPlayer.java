package lt.edgu.blasterman.entity;

import java.util.ArrayList;

import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.TiledManager;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryonet.Connection;

public class OtherPlayer extends Entity {
	private int id;
	private byte colour;
	private String username;
	private Vector2 position;
	private int rotation;
	private Connection connection;
	private TiledManager tiledManager;
	private TiledMapTileLayer mainLayer;

	// Animation Stuff
	public Animation forwardAnimation, leftAnimation, backwardAnimation, rightAnimation;
	private float animationTime;
	private boolean isSpritesSet;

	private int speed;

	private Vector2 targetPosition;
	private int roundedTargetPosition;

	private ArrayList<Character> moves;
	private char currentMove;
	private char lastMove;

	private boolean alive;

	public OtherPlayer(TiledManager tiledManager, TiledMapTileLayer mainLayer) {
		super((new Animation(1 / 30F, TextureManager.playerAtlasYellow.findRegions("forward_frame")).getKeyFrame(0)));
		this.tiledManager = tiledManager;
		this.mainLayer = mainLayer;
		this.setId(-1);
		this.setUsername("UNNAMED");
		this.position = null;
		this.rotation = -1;
		this.animationTime = 0;
		this.speed = 64;
		currentMove = 'x';
		lastMove = 'x';
		this.moves = new ArrayList<Character>();
		setSpritesSet(false);
		alive = true;
	}

	public void render(Batch batch, float delta) {
		if (isSpritesSet() && position != null) {
			batch.draw(this, position.x, position.y);
			updateMovement(delta);
		}
	}

	public void updateMovement(float delta) {
		animationTime += delta;
		try {
			if (!moves.isEmpty() && moves.get(0) == 'w') {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) position.x, (int) (position.y + this.getHeight() / 2));
				}
				if (rotation != 0) {
					rotation = 0;
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
				if (/*!isTileSolid(new Vector2(position.x, roundedTargetPosition)) && */position.y <= roundedTargetPosition) {
					currentMove = 'w';
					lastMove = currentMove;
					setRegion(forwardAnimation.getKeyFrame(animationTime));
					position.y += speed * delta;
					if (position.y >= roundedTargetPosition) {
						position.y = roundedTargetPosition;
						moves.remove(0);
						currentMove = 'x';
					}
				}
			} else if (!moves.isEmpty() && moves.get(0) == 'a') {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) (position.x - this.getWidth()), (int) position.y);
				}
				if (rotation != 1) {
					rotation = 1;
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
				if (/*!isTileSolid(new Vector2(roundedTargetPosition, position.y)) && */position.x >= roundedTargetPosition) {
					currentMove = 'a';
					lastMove = currentMove;
					setRegion(leftAnimation.getKeyFrame(animationTime));
					position.x -= speed * delta;
					if (position.x <= roundedTargetPosition) {
						position.x = roundedTargetPosition;
						moves.remove(0);
						currentMove = 'x';
					}
				}
			} else if (!moves.isEmpty() && moves.get(0) == 's') {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) position.x, (int) (position.y - this.getHeight()));
				}
				if (rotation != 2) {
					rotation = 2;
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
				if (/*!isTileSolid(new Vector2(position.x, roundedTargetPosition)) && */position.y >= roundedTargetPosition) {
					currentMove = 's';
					lastMove = currentMove;
					setRegion(backwardAnimation.getKeyFrame(animationTime));
					position.y -= speed * delta;
					if (position.y <= roundedTargetPosition) {
						position.y = roundedTargetPosition;
						moves.remove(0);
						currentMove = 'x';
					}
				}
			} else if (!moves.isEmpty() && moves.get(0) == 'd') {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) (position.x + this.getWidth() / 2), (int) position.y);
				}
				if (rotation != 3) {
					rotation = 3;
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
				if (/*!isTileSolid(new Vector2(roundedTargetPosition, position.y)) && */position.x <= roundedTargetPosition) {
					currentMove = 'd';
					lastMove = currentMove;
					setRegion(rightAnimation.getKeyFrame(animationTime));
					position.x += speed * delta;
					if (position.x >= roundedTargetPosition) {
						position.x = roundedTargetPosition;
						moves.remove(0);
						currentMove = 'x';
					}
				}
			} else {
				// Update still animation based on last move
				switch (lastMove) {
				case 'w':
					setRegion(forwardAnimation.getKeyFrame(0));
					break;
				case 'a':
					setRegion(leftAnimation.getKeyFrame(0));
					break;
				case 's':
					setRegion(backwardAnimation.getKeyFrame(0));
					break;
				case 'd':
					setRegion(rightAnimation.getKeyFrame(0));
					break;
				}
			}
		} catch (NullPointerException exception) {
			exception.printStackTrace();
		}
		
		TiledMapTile currentTile = mainLayer.getCell((int) (position.x / mainLayer.getTileWidth()), (int) (position.y / mainLayer.getTileHeight())).getTile();
		// Power-ups
		if (currentTile.getProperties().containsKey("powerUp")) {
			System.out.println("powerup");
			mainLayer.getCell((int) (position.x / mainLayer.getTileWidth()), (int) (position.y / mainLayer.getTileHeight())).setTile(tiledManager.grassTile);
		}
	}

	@SuppressWarnings("unused")
	private boolean isTileSolid(Vector2 position) {
		Cell cell = mainLayer.getCell((int) (position.x / mainLayer.getTileWidth()), (int) (position.y / mainLayer.getTileHeight()));
		return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey("solid");
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setSpeed(int speed) {
		if (this.speed < 224 & speed < 224 && (speed - this.speed) == 32) {
			this.speed = speed;
		}
	}

	public int getSpeed() {
		return speed;
	}
	
	public ArrayList<Character> getMoves() {
		return moves;
	}
	
	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public byte getColour() {
		return colour;
	}

	public void setColour(byte colour) {
		this.colour = colour;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isSpritesSet() {
		return isSpritesSet;
	}

	public void setSpritesSet(boolean isSpritesSet) {
		this.isSpritesSet = isSpritesSet;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Connection getConnection() {
		return connection;
	}
	
	public TiledManager getTiledManager() {
		return tiledManager;
	}

	public void setTiledManager(TiledManager tiledManager) {
		this.tiledManager = tiledManager;
	}

	public TiledMapTileLayer getMainLayer() {
		return mainLayer;
	}

	public void setMainLayer(TiledMapTileLayer mainLayer) {
		this.mainLayer = mainLayer;
	}
}
