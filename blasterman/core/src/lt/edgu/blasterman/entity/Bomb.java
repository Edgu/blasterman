package lt.edgu.blasterman.entity;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.screens.SPGameScreen;
import lt.edgu.blasterman.util.TileE;

import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Vector2;

public class Bomb {
	private int id;
	private int x, y;
	private int radius;
	private SPGameScreen gameScreen;
	private ArrayList<TileE> explosionTiles;

	public Bomb(int id, int x, int y, int radius, SPGameScreen gameScreen) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.gameScreen = gameScreen;
		explosionTiles = new ArrayList<TileE>();
		detonate();
	}

	public void detonate() {
		try {
			// Fills ArrayList with tiles which are going to be destroyed.
			// Tile Under Bomb
			explosionTiles.add(new TileE(id, x, y, gameScreen.getTiledLayer().getCell(x, y).getTile().getId()));

			// Up
			for (int y = this.y + 1; y < ((this.y + 1) + radius); y++) {
				TiledMapTile currentTile = gameScreen.getTiledLayer().getCell(x, y).getTile();
				if (currentTile.getProperties().containsKey("destructible") || currentTile.getId() == 18) {
					explosionTiles.add(new TileE(id, x, y, gameScreen.getTiledLayer().getCell(x, y).getTile().getId()));
					if (currentTile.getId() == 16) {
						break;
					}
				} else {
					break;
				}
			}

			// Down
			for (int y = this.y - 1; y > ((this.y - 1) - radius); y--) {
				TiledMapTile currentTile = gameScreen.getTiledLayer().getCell(x, y).getTile();
				if (currentTile.getProperties().containsKey("destructible") || currentTile.getId() == 18) {
					explosionTiles.add(new TileE(id, x, y, gameScreen.getTiledLayer().getCell(x, y).getTile().getId()));
					if (currentTile.getId() == 16) {
						break;
					}
				} else {
					break;
				}
			}

			// Left
			for (int x = this.x - 1; x > ((this.x - 1) - radius); x--) {
				TiledMapTile currentTile = gameScreen.getTiledLayer().getCell(x, y).getTile();
				if (currentTile.getProperties().containsKey("destructible") || currentTile.getId() == 18) {
					explosionTiles.add(new TileE(id, x, y, gameScreen.getTiledLayer().getCell(x, y).getTile().getId()));
					if (currentTile.getId() == 16) {
						break;
					}
				} else {
					break;
				}
			}

			// Right
			for (int x = this.x + 1; x < ((this.x + 1) + radius); x++) {
				TiledMapTile currentTile = gameScreen.getTiledLayer().getCell(x, y).getTile();
				if (currentTile.getProperties().containsKey("destructible") || currentTile.getId() == 18) {
					explosionTiles.add(new TileE(id, x, y, gameScreen.getTiledLayer().getCell(x, y).getTile().getId()));
					if (currentTile.getId() == 16) {
						break;
					}
				} else {
					break;
				}
			}


			for (TileE tile : explosionTiles) {
				SPGameScreen.willExplode.add(new Vector2(tile.x * 32, tile.y * 32));
			}

			// --- ///
			TimerTask explosion = new TimerTask() {
				@Override
				public void run() {
					final ArrayList<TileE> tiles = explosionTiles;
					AudioManager.explosion.play();
					for (int i = 0; i < tiles.size(); i++) {
						Cell cell = gameScreen.getTiledLayer().getCell((int) tiles.get(i).x, (int) tiles.get(i).y);
						if (cell.getTile().getProperties().containsKey("owner")) {
							if (!cell.getTile().getProperties().get("owner").equals(id)) {
								cell.getTile().getProperties().put("owner", id);
								cell.getTile().getProperties().put("exploded", cell.getTile().getId());
							}
						} else {
							StaticTiledMapTile tile = new StaticTiledMapTile(gameScreen.getTiledManager().explosionTile.getTextureRegion());
							tile.setId(555);
							tile.getProperties().put("name", "explosion");
							tile.getProperties().put("destructible", null);
							tile.getProperties().put("owner", id);
							tile.getProperties().put("ownerName", "singlePlayer");
							tile.getProperties().put("exploded", cell.getTile().getId());
							cell.setTile(tile);
						}
						SPGameScreen.willExplode.remove(new Vector2(tiles.get(i).x * 32, tiles.get(i).y * 32));
					}
					SPPlayer player = gameScreen.getPlayer();
					player.setBombs(player.getBombs() + 1);
					Timer timer2 = new Timer();
					timer2.schedule(new TimerTask() {
						@Override
						public void run() {
							Random random = new Random();
							for (int i = 0; i < tiles.size(); i++) {
								TiledMapTile qw = gameScreen.getTiledLayer().getCell((int) tiles.get(i).x, (int) tiles.get(i).y).getTile();
								if ((random.nextInt((3 - 1) + 1) + 1) == 3 && tiles.get(i).underneathTile == 16 && qw.getProperties().get("exploded").equals(16)) {
									//							if (true) {
									int powerUp = random.nextInt((4 - 1) + 1) + 1;
									TiledMapTile randomPowerUp = null;
									switch (powerUp) {
									case 1:
										randomPowerUp = gameScreen.getTiledManager().additionalBombPowerUp;
										break;
									case 2:
										randomPowerUp = gameScreen.getTiledManager().speedBoostPowerUp;
										break;
									case 3:
										randomPowerUp = gameScreen.getTiledManager().randomPowerUp;
										break;
									case 4:
										randomPowerUp = gameScreen.getTiledManager().bombStrengthPowerUp;
										break;
									}
									// Set the tile
									gameScreen.getTiledLayer().getCell((int) tiles.get(i).x, (int) tiles.get(i).y).setTile(randomPowerUp);
								} else {
									Cell cell = gameScreen.getTiledLayer().getCell((int) tiles.get(i).x, (int) tiles.get(i).y);
									if (cell.getTile().getProperties().containsKey("owner")) {
										if (cell.getTile().getProperties().get("owner").equals(id)) {
											gameScreen.getTiledLayer().getCell((int) tiles.get(i).x, (int) tiles.get(i).y).setTile(gameScreen.getTiledManager().grassTile);
										}
									} else {
										gameScreen.getTiledLayer().getCell((int) tiles.get(i).x, (int) tiles.get(i).y).setTile(gameScreen.getTiledManager().grassTile);
									}
									// If its not our tile, do nothing.
								}
							}
							return;
						}
					}, 1250);
				}
			};

			Timer timer = new Timer();
			timer.schedule(explosion, 2000);
		} catch (NullPointerException e) {
			System.out.println("BOMB CAUGHT ERROR");
			e.printStackTrace();
		}
		System.gc();
	}
}