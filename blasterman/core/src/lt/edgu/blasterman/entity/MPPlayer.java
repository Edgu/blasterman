package lt.edgu.blasterman.entity;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.network.packets.room.PacketMove;
import lt.edgu.blasterman.screens.MPGameScreen;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;

public class MPPlayer extends Entity implements InputProcessor {

	private byte colour;
	private String username;
	private int experience;
	private int level;
	private TiledMapTileLayer mainLayer;
	private int bombs = 1;
	private int speed = 64;

	// Animation Stuff
	public Animation forwardAnimation, leftAnimation, backwardAnimation, rightAnimation;

	private MPGameScreen gameScreen;

	// Movement variables.
	public boolean forwardMove;
	public boolean backwardMove;
	public boolean rightMove;
	public boolean leftMove;

	private Vector2 targetPosition;
	private int roundedTargetPosition;

	// Animation Settings
	private float animationTime = 0;
	private boolean disposed;
	//	public boolean isSpritesSet;
	private char currentMove = 'x';
	private char lastMove = 'x';
	private boolean dead;

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public MPPlayer(TiledMapTileLayer mainLayer, MPGameScreen gameScreen) {
		super((new Animation(1 / 30F, TextureManager.playerAtlasYellow.findRegions("forward_frame")).getKeyFrame(0)));
		dead = false;
		//		isSpritesSet = false;
		this.gameScreen = gameScreen;
		username = gameScreen.getUsername();
		this.mainLayer = mainLayer;
		//		speed = 64;
		//		bombs = 1;
		//		currentMove = 'x';
		//		lastMove = 'x';
		disposed = false;
		position = null;
		animationTime = 0;
	}

	public void render(Batch batch, float delta) {
		if (!isDead()) {
			if (/*isSpritesSet && */position != null) {
				if (!disposed) {
					batch.draw(this, getPosition().x, getPosition().y);
					update(delta);
				}
			}
		}
	}

	public void update(float delta) {
		updateMovement(delta);
	}

	public void updateMovement(float delta) {
		animationTime += delta;
		try {
			if ((forwardMove && currentMove == 'x' || currentMove == 'w')) {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) position.x, (int) (position.y + this.getHeight() / 2));
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
				if (!isTileSolid(new Vector2(position.x, roundedTargetPosition)) && position.y <= roundedTargetPosition) {
					if (currentMove != 'w') {
						currentMove = 'w';
						setLastMove(currentMove);
						PacketMove packet = new PacketMove();
						packet.id = gameScreen.getRoomServerClient().getClient().getID();
						packet.direction = currentMove;
						gameScreen.getRoomServerClient().getClient().sendTCP(packet);
					}
					setRegion(forwardAnimation.getKeyFrame(animationTime));
					position.y += speed * delta;
					if (position.y >= roundedTargetPosition) {
						position.y = roundedTargetPosition;
						currentMove = 'x';
					}
				}
			} else if (leftMove && currentMove == 'x' || currentMove == 'a') {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) (position.x - this.getWidth()), (int) position.y);
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
				if (!isTileSolid(new Vector2(roundedTargetPosition, position.y)) && position.x >= roundedTargetPosition) {
					if (currentMove != 'a') {
						currentMove = 'a';
						setLastMove(currentMove);
						PacketMove packet = new PacketMove();
						packet.id = gameScreen.getRoomServerClient().getClient().getID();
						packet.direction = currentMove;
						gameScreen.getRoomServerClient().getClient().sendTCP(packet);
					}
					setRegion(leftAnimation.getKeyFrame(animationTime));
					position.x -= speed * delta;
					if (position.x <= roundedTargetPosition) {
						position.x = roundedTargetPosition;
						currentMove = 'x';
					}
				}
			} else if (backwardMove && currentMove == 'x' || currentMove == 's') {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) position.x, (int) (position.y - this.getHeight()));
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
				if (!isTileSolid(new Vector2(position.x, roundedTargetPosition)) && position.y >= roundedTargetPosition) {
					if (currentMove != 's') {
						currentMove = 's';
						setLastMove(currentMove);
						PacketMove packet = new PacketMove();
						packet.id = gameScreen.getRoomServerClient().getClient().getID();
						packet.direction = currentMove;
						gameScreen.getRoomServerClient().getClient().sendTCP(packet);
					}
					setRegion(backwardAnimation.getKeyFrame(animationTime));
					position.y -= speed * delta;
					if (position.y <= roundedTargetPosition) {
						position.y = roundedTargetPosition;
						currentMove = 'x';
					}
				}
			} else if (rightMove && currentMove == 'x' || currentMove == 'd') {
				targetPosition = new Vector2((int) (position.x + this.getWidth() / 2), (int) position.y);
				roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
				if (!isTileSolid(new Vector2(roundedTargetPosition, position.y)) && position.x <= roundedTargetPosition) {
					if (currentMove != 'd') {
						currentMove = 'd';
						setLastMove(currentMove);
						PacketMove packet = new PacketMove();
						packet.id = gameScreen.getRoomServerClient().getClient().getID();
						packet.direction = currentMove;
						gameScreen.getRoomServerClient().getClient().sendTCP(packet);
					}
					setRegion(rightAnimation.getKeyFrame(animationTime));
					position.x += speed * delta;
					if (position.x >= roundedTargetPosition) {
						position.x = roundedTargetPosition;
						currentMove = 'x';
					}
				}
			} else {
				// Update still animation based on rotation
				switch (getLastMove()) {
				case 'w':
					setRegion(forwardAnimation.getKeyFrame(0));
					break;
				case 'a':
					setRegion(leftAnimation.getKeyFrame(0));
					break;
				case 's':
					setRegion(backwardAnimation.getKeyFrame(0));
					break;
				case 'd':
					setRegion(rightAnimation.getKeyFrame(0));
					break;
				}
			}
		} catch (NullPointerException exception) {
			exception.printStackTrace();
		}

		TiledMapTile currentTile = mainLayer.getCell((int) (position.x / mainLayer.getTileWidth()), (int) (position.y / mainLayer.getTileHeight())).getTile();
		// Power-ups
		if (currentTile.getProperties().containsKey("powerUp") && !disposed) {
			mainLayer.getCell((int) (position.x / mainLayer.getTileWidth()), (int) (position.y / mainLayer.getTileHeight())).setTile(gameScreen.tiledManager.grassTile);
			AudioManager.powerUp.play();
		}
	}

	private boolean isTileSolid(Vector2 position) {
		Cell cell = mainLayer.getCell((int) (position.x / mainLayer.getTileWidth()), (int) (position.y / mainLayer.getTileHeight()));
		return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey("solid");
	}

	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
		case Keys.W:
			forwardMove = true;
			break;
		case Keys.A:
			leftMove = true;
			break;
		case Keys.S:
			backwardMove = true;
			break;
		case Keys.D:
			rightMove = true;
			break;
		case Keys.F1:
			if (!gameScreen.debug)
				gameScreen.debug = true;
			else 
				gameScreen.debug = false;
			break;
		case Keys.SPACE:
			AudioManager.plantBomb.play();
			switch (getCurrentMove()) {
			case 'w':
				gameScreen.createNewBomb(new Vector2((int) ((getPosition().x + getWidth() / 2) / mainLayer.getTileWidth()) * 32,
						(int) (getPosition().y / mainLayer.getTileHeight()) * 32));
				break;
			case 'a':
				gameScreen.createNewBomb(new Vector2((int) (((getPosition().x + 15) + getWidth() / 2) / mainLayer.getTileWidth()) * 32,
						(int) ((getPosition().y + getHeight() / 2) / mainLayer.getTileHeight()) * 32));
				break;
			case 's':
				gameScreen.createNewBomb(new Vector2((int) ((getPosition().x + getWidth() / 2) / mainLayer.getTileWidth()) * 32,
						(int) ((getPosition().y + 31) / mainLayer.getTileHeight()) * 32));
				break;
			case 'd':
				gameScreen.createNewBomb(new Vector2((int) (((getPosition().x - 16) + getWidth() / 2) / mainLayer.getTileWidth()) * 32,
						(int) ((getPosition().y + getHeight() / 2) / mainLayer.getTileHeight()) * 32));
				break;
			case 'x':
				gameScreen.createNewBomb(new Vector2((int) ((getPosition().x + getWidth() / 2) / mainLayer.getTileWidth()) * 32,
						(int) (getPosition().y / mainLayer.getTileHeight()) * 32));
				break;
			}
			break;
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch (keycode) {
		case Keys.W:
			forwardMove = false;
			break;
		case Keys.A:
			leftMove = false;
			break;
		case Keys.S:
			backwardMove = false;
			break;
		case Keys.D:
			rightMove = false;
			break;
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public TiledMapTileLayer getCollisionLayer() {
		return mainLayer;
	}

	public void setCollisionLayer(TiledMapTileLayer collisionLayer) {
		this.mainLayer = collisionLayer;
	}

	public void die() {
		disposed = true;
		gameScreen.playerDie();
		dispose();
	}

	public void dispose() {
		//		this.getTexture().dispose();
	}

	public MPGameScreen getGameScreen() {
		return gameScreen;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getBombs() {
		return bombs;
	}

	public void setBombs(int bombs) {
		if (bombs < 6) {
			this.bombs = bombs;
		}
	}

	public void setSpeed(int speed) {
		if (this.speed < 224 & speed < 224 && (speed - this.speed) == 32) {
			this.speed = speed;
		}
	}

	public int getSpeed() {
		return speed;
	}

	public char getCurrentMove() {
		return currentMove;
	}

	public void setCurrentMove(char currentMove) {
		this.currentMove = currentMove;
	}

	public byte getColour() {
		return colour;
	}

	public void setColour(byte colour) {
		this.colour = colour;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public char getLastMove() {
		return lastMove;
	}

	public void setLastMove(char lastMove) {
		this.lastMove = lastMove;
	}
	
	public void setTiledMainLayer(TiledMapTileLayer mainLayer) {
		this.mainLayer = mainLayer;
	}
}