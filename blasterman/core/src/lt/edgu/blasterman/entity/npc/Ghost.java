package lt.edgu.blasterman.entity.npc;

import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.screens.SPGameScreen;

import com.badlogic.gdx.graphics.g2d.Batch;

public class Ghost extends NPC {

	public Ghost(SPGameScreen gameScreen, int x, int y) {
		super(gameScreen, TextureManager.enemyAtlas.findRegion("bh"), x, y);
		setSpeed(96);
	}
	
	public void render(Batch batch, float delta) {
		super.render(batch, delta);
	}

	public void die() {
		dispose();
	}

	public void dispose() {}
}