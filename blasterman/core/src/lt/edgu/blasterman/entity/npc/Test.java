package lt.edgu.blasterman.entity.npc;

import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.screens.SPGameScreen;

import com.badlogic.gdx.graphics.g2d.Batch;

public class Test extends NPC {

	public Test(SPGameScreen gameScreen, int x, int y) {
		super(gameScreen, TextureManager.enemyAtlas.findRegion("bh"), x, y);
		setSpeed(128);
	}
	
	public void render(Batch batch, float delta) {
		super.render(batch, delta);
	}

	public void die() {
		dispose();
	}

	public void dispose() {}
}