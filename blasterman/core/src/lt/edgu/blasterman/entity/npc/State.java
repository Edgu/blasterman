package lt.edgu.blasterman.entity.npc;

public enum State {
	IDLE,
	MOVING_TO_PLAYER,
	MOVING_TO_NPC,
	MOVING_TO_SAFETY,
	MOVING_TO_CRATE,
	PLANT_BOMB;
}
