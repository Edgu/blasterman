package lt.edgu.blasterman.entity.npc;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.entity.Entity;
import lt.edgu.blasterman.screens.SPGameScreen;
import lt.edgu.blasterman.util.Node;

public abstract class NPC extends Entity {

	private SPGameScreen gameScreen;
	private boolean dead;

	// Animation Stuff
	public Animation horizontalAnimation, verticalAnimation;
	private float animationTime;
	private Animation currentAnimation;


	// SUPER TEMPVAR
	private List<Node> path;
	private List<Node> lastPath;
	// RoundedPosition used for rounding current player position and
	// calculating path based on that so it doesn't lag because
	// if we send not tile precision (/32) the method returns null,
	// which causes npc to stop when player is moving.
	private State state = State.IDLE;

	public NPC(SPGameScreen gameScreen, TextureRegion textureRegion, int x, int y) {
		super(textureRegion);
		dead = false;
		position.set(x, y);
		this.gameScreen = gameScreen;
		speed = 16;
		animationTime = 0;
		horizontalAnimation = TextureManager.blueEnemyHorizontal;
		verticalAnimation = TextureManager.blueEnemyVertical;
		currentAnimation = horizontalAnimation;
		path = null;
	}

	public void render(Batch batch, float delta) {
		if (!dead) {
			batch.draw(this, getPosition().x, getPosition().y);
			update(delta);
		}
	}

	public void update(float delta) {
		animationTime += delta;

		if ((int) animationTime % 2 == 0) {
			setRegion(currentAnimation.getKeyFrame(animationTime));
		} else {
			setRegion(currentAnimation.getKeyFrame(0));
		}

		nextAction(delta);
		checkStep();
	}
	
	private boolean isMoveFinished() {
		int x = (int) (getPosition().x % 32);
		int y = (int) (getPosition().y % 32);
		return (x == 0 && y == 0) ? true : false;
	}

	public void nextAction(float delta) {
		int x = (int) (getPosition().x % 32);
		int y = (int) (getPosition().y % 32);
		
		System.out.println("x % 32 = " + x);
		System.out.println("y % 32 = " + y);
		System.out.println(isMoveFinished());
		System.out.println();
		
		if (state == State.IDLE) {
			if (path != null) path.clear();
			System.out.println("getPathToCrate() --- " + getPathToCrate());
		}

		if (path != null) {
			if (path.isEmpty()) {
				switch (state) {
				case MOVING_TO_CRATE:
					state = State.PLANT_BOMB;
					plantBomb();
					break;
				case MOVING_TO_SAFETY:
					System.out.println("getPathToSafety() --- " + getPathToSafety());
				default:
					break;
				}
			}
		}

		System.out.println("STATE: " + state);
		System.out.println("PATH: " + path);
		System.out.println();
		move(delta);
	}

	public boolean getPathToCrate() {
		path = gameScreen.testFindPathTile(gameScreen.getTiledManager().boxTile, this, gameScreen.tileList(gameScreen.getTiledManager().boxTile));
		if (path != null) {
			state = State.MOVING_TO_CRATE;
			return true;
		}
		return false;
	}

	public void plantBomb() {
		gameScreen.createNewBomb(getPosition());
	}

	public boolean getPathToSafety() {
		path = gameScreen.findSavePath(this);
		if (path == null || path.size() > 0) { 
			state = State.IDLE;
			return true;
		}
		return false;
	}

	//-----------------------------------------------------------------------------------------

	public boolean getPathToPlayer() {
		if (position.x % 32 != 0 || position.y % 32 != 0) {
			return false;
		}
		if (!gameScreen.getPlayer().isDead()) {
			Vector2 playerPosition = new Vector2(gameScreen.getPlayer().getPosition().x, gameScreen.getPlayer().getPosition().y);
			if (position.x == playerPosition.x && position.y == playerPosition.y) return false;
			playerPosition.x = Math.round(playerPosition.x) / 32;
			playerPosition.y = Math.round(playerPosition.y) / 32;
			if (path != null && path.size() > 0) {
				lastPath = path;
			}
			path = gameScreen.findPath(new Vector2(Math.round(position.x) / 32, Math.round(position.y) / 32), new Vector2(playerPosition.x, playerPosition.y));
			if (path == null || path.size() < 1) {
				path = lastPath;
				return true;
			}
		}
		return false;
	}

	private boolean getPathToNPC() {
		if (position.x % 32 != 0 || position.y % 32 != 0) {
			return false;
		}
		List<List<Node>> paths = new ArrayList<List<Node>>();
		Vector2 npcPosition = new Vector2();
		for (NPC npc : gameScreen.getNPC()) {
			if (npc == this) {
				continue;
			}
			npcPosition.x = Math.round(npc.position.x) / 32;
			npcPosition.y = Math.round(npc.position.y) / 32;
			paths.add(gameScreen.findPath(new Vector2(Math.round(position.x) / 32, Math.round(position.y) / 32), new Vector2(npcPosition.x, npcPosition.y)));
		}
		path = SPGameScreen.sortPaths(paths);
		if (path != null) {
			return true;
		}
		System.out.println(path);
		return false;
	}

	public void move(float delta) {
		if (path != null) {
			if (path.size() > 0) {
				Node currentNode = path.get(path.size() - 1);
				Vector2 nodePosition = new Vector2(currentNode.position.x, currentNode.position.y);
				nodePosition.x *= 32;
				nodePosition.y *= 32;
				//				if (!gameScreen.isRisky(nodePosition)) { ------ SHOULD NOT BE HANDLED HERE
				// Going Up
				if (position.y < nodePosition.y) {
					position.y += speed * delta;
					if (position.y > nodePosition.y) {
						position.y = nodePosition.y;
						path.remove(currentNode);
					}
					// Going Down
				} else if (position.y > nodePosition.y) {
					position.y -= speed * delta;
					if (position.y < nodePosition.y) {
						position.y = nodePosition.y;
						path.remove(currentNode);
					}
					// Going Left
				} else if (position.x < nodePosition.x) {
					position.x += speed * delta;
					if (position.x > nodePosition.x) {
						position.x = nodePosition.x;
						path.remove(currentNode);
					}
					// Going Right
				} else if (position.x > nodePosition.x) {
					position.x -= speed * delta;
					if (position.x < nodePosition.x) {
						position.x = nodePosition.x;
						path.remove(currentNode);
					}
				}
				//				} ------------------------------------------- THIS METHOD SHOULD JUST MOVE THE ENTITY
			}
		}
	}

	private void checkStep() {
		int x = (int) (position.x / gameScreen.getTiledLayer().getTileWidth());
		int y = (int) (position.y / gameScreen.getTiledLayer().getTileHeight());
		Cell cell = gameScreen.getTiledLayer().getCell(x, y);

		// Checks if player steps on these tiles.
		switch (cell.getTile().getId()) {
		case 10:
			// Additional Bomb
//			if (bombs < 6) {
//				bombs++;
//			}
			setGrass(x, y, cell.getTile().getId());
			break;
		case 11:
			// Bomb Strength
//			bombStrenght += 1;
			setGrass(x, y, cell.getTile().getId());
			break;
		case 12:
			// Random
//			Random random = new Random();
//			int powerUp = random.nextInt((3 - 1) + 1) + 1;
//			switch (powerUp) {
//			case 1:
//				// Speed Boost
//				if (speed < 192) {
//					speed += 32;
//				}
//				break;
//			case 2:
//				// Additional Bomb
//				if (bombs < 6) {
//					bombs++;
//				}
//				break;
//			case 3:
//				// Bomb str
//				bombStrenght += 1;
//				break;
//			}
			setGrass(x, y, cell.getTile().getId());
			break;
		case 13:
			// Speed Boost
//			if (speed < 192) {
//				speed += 32;
//			}
			setGrass(x, y, cell.getTile().getId());
			break;
		case 555:
			// Explosion
//			dead = true;
			break;
		}
	}

	private void setGrass(int x, int y, int tileID) {
		if (tileID != 555) {
			AudioManager.powerUp.play();
		}
		gameScreen.getTiledLayer().getCell(x, y).setTile(gameScreen.getTiledManager().grassTile);
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getSpeed() {
		return speed;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public void setPosition(float x, float y) {
		position.set(x, y);
	}
}
