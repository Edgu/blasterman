package lt.edgu.blasterman.entity.npc;

import java.util.Random;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.entity.Entity;
import lt.edgu.blasterman.screens.SPGameScreen;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;

public class Bot extends Entity {

	private SPGameScreen gameScreen;
	private int speed;
	private boolean forwardMove;
	private boolean backwardMove;
	private boolean rightMove;
	private boolean leftMove;
	private Vector2 targetPosition;
	@SuppressWarnings("unused")
	private Vector2 previousTargetPosition;
	private Vector2 previousPosition;
	private int roundedTargetPosition;
	private char currentMove;

	// Animation Stuff
	public Animation horizontalAnimation, verticalAnimation;
	private float animationTime;
	private Animation currentAnimation;

	//TEMPVAR
	private int moves;
	private boolean dead;

	public Bot(SPGameScreen gameScreen, TextureRegion textureRegion, int x, int y) {
		super(textureRegion);
		dead = false;
		position.set(x, y);
		this.gameScreen = gameScreen;
		speed = 16;
		currentMove = 'x';
		targetPosition = new Vector2();
		previousTargetPosition = new Vector2();
		previousPosition = new Vector2();
		moves = 4;
		animationTime = 0;
		horizontalAnimation = TextureManager.blueEnemyHorizontal;
		verticalAnimation = TextureManager.blueEnemyVertical;
		currentAnimation = horizontalAnimation;

		if (!isTileSolid(new Vector2(position.x, position.y + 32))) {
			forwardMove = true;
		} else if (!isTileSolid(new Vector2(position.x - 32, position.y))) {
			leftMove = true;
		} else if (!isTileSolid(new Vector2(position.x, position.y - 32))) {
			backwardMove = true;
		} else if (!isTileSolid(new Vector2(position.x + 32, position.y))) {
			rightMove = true;
		}
	}

	public void render(Batch batch, float delta) {
		if (!dead) {
			batch.draw(this, getPosition().x, getPosition().y);
			update(delta);
		}
	}

	public void update(float delta) {
		plantBomb();
		steeringBehavior(delta);
	}
	
	private void plantBomb() {
		if (currentMove == 'x') {
			if (getTileID(new Vector2(position.x, position.y + 32)) == 16) {
				gameScreen.createNewBomb(position);
			}
		}
	}

	private void steeringBehavior(float delta) {
		animationTime += delta;

		if ((int) animationTime % 2 == 0) {
			setRegion(currentAnimation.getKeyFrame(animationTime));
		} else {
			setRegion(currentAnimation.getKeyFrame(0));
		}

		/*
		 * if walking down (from bomb)and bomb get exploded
		 * and player gets stuck at the end, it means
		 * the targetpos prob coordinates of the next tile like grass
		 * 
		 * potential fix : store last targetPos coordinates
		 * check if the tile contains 555 and 
		 * 
		 */
		if (currentMove == 'x' && moves > 1) {
			changeDirection();
		}

		try {
			if ((forwardMove && currentMove == 'x' || currentMove == 'w')) {
				if (currentMove == 'x')
					setTargetPosition(new Vector2((int) position.x, (int) (position.y + this.getHeight() / 2)));
				roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
				if (!isTileSolid(new Vector2(position.x, roundedTargetPosition))) {
					currentAnimation = verticalAnimation;
					currentMove = 'w';
					setPosition(position.x, position.y + (speed * delta));
					if (position.y >= roundedTargetPosition) {
						setPosition(position.x, roundedTargetPosition);
						currentMove = 'x';
						moves++;
					}
				} else {
					forwardMove = false;
					backwardMove = true;
				}
			} else if (leftMove && currentMove == 'x' || currentMove == 'a') {
				if (currentMove == 'x')
					setTargetPosition(new Vector2((int) (position.x - this.getWidth()), (int) position.y));
				roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
				if (!isTileSolid(new Vector2(roundedTargetPosition, position.y))) {
					currentAnimation = horizontalAnimation;
					currentMove = 'a';
					setPosition(position.x - (speed * delta), position.y);
					if (position.x <= roundedTargetPosition) {
						setPosition(roundedTargetPosition, position.y);
						currentMove = 'x';
						moves++;
					}
				} else {
					leftMove = false;
					rightMove = true;
				}
			} else if (backwardMove && currentMove == 'x' || currentMove == 's') {
				if (currentMove == 'x')
					setTargetPosition(new Vector2((int) position.x, (int) (position.y - this.getHeight())));
				roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
				if (!isTileSolid(new Vector2(position.x, roundedTargetPosition))) {
					currentAnimation = verticalAnimation;
					currentMove = 's';
					setPosition(position.x, position.y - (speed * delta));
					if (position.y <= roundedTargetPosition) {
						setPosition(position.x, roundedTargetPosition);
						currentMove = 'x';
						moves++;
					}
				} else {
					backwardMove = false;
					forwardMove = true;
				}
			} else if (rightMove && currentMove == 'x' || currentMove == 'd') {
				if (currentMove == 'x')
					setTargetPosition(new Vector2((int) (position.x + this.getWidth() / 2), (int) position.y));
				roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
				if (!isTileSolid(new Vector2(roundedTargetPosition, position.y))) {
					currentAnimation = horizontalAnimation;
					currentMove = 'd';
					setPosition(position.x + (speed * delta), position.y);
					if (position.x >= roundedTargetPosition) {
						setPosition(roundedTargetPosition, position.y);
						currentMove = 'x';
						moves++;
					}
				} else {
					rightMove = false;
					leftMove = true;
				}
			}
		} catch (NullPointerException exception) {
			exception.printStackTrace();
		}
		
//		if (currentMove != 'x' && ((getTileID(position) == 555 || getTileID(previousTargetPosition) == 555 || getTileID(targetPosition) == 555)) && (previousPosition.equals(position) && getTileID(position) != 18)) {
//			System.out.println(previousPosition + " --- " + position);
//			dead = true;
//		}

		// check powerup
		TiledMapTile currentTile = gameScreen.getTiledLayer().getCell((int) (position.x / gameScreen.getTiledLayer().getTileWidth()), (int) (position.y / gameScreen.getTiledLayer().getTileHeight())).getTile();
		if (currentTile.getProperties().containsKey("powerUp")) {
			AudioManager.powerUp.play();
			gameScreen.getTiledLayer().getCell((int) (position.x / gameScreen.getTiledLayer().getTileWidth()), (int) (position.y / gameScreen.getTiledLayer().getTileHeight())).setTile(gameScreen.getTiledManager().grassTile);
		}
	}

	private void changeDirection() {
		byte lastMove = 0;

		if (forwardMove) {
			lastMove = 1;
		} else if (leftMove) {
			lastMove = 2;
		} else if (backwardMove) {
			lastMove = 3;
		} else if (rightMove) {
			lastMove = 4;
		}

		Random random = new Random();
		int direction = random.nextInt((4 - 1) + 1) + 1;

		if (lastMove == direction) {
			return;
		}

		// block forward-back
		if (isTileSolid(new Vector2(position.x, (position.y + 32))) && isTileSolid(new Vector2(position.x, (position.y - 32)))) {
			return;
			//block right-left
		} else if (isTileSolid(new Vector2((position.x - 32), position.y)) && isTileSolid(new Vector2((position.x + 32), position.y))) {
			return;
		}

		switch (direction) {
		case 1:
			if (!isTileSolid(new Vector2(position.x, (position.y + 32)))) {
				setMove(true, false, false, false);
			} else {
				setMove(false, false, true, false);
			}
			break;
		case 2:
			if (!isTileSolid(new Vector2((position.x - 32), position.y))) {
				setMove(false, true, false, false);
			} else {
				setMove(false, false, false, true);

			}
			break;
		case 3:
			if (!isTileSolid(new Vector2(position.x, (position.y - 32)))) {
				setMove(false, false, true, false);
			} else {
				setMove(true, false, false, false);
			}
			break;
		case 4:
			if (!isTileSolid(new Vector2((position.x + 32), position.y))) {
				setMove(false, false, false, true);
			} else {
				setMove(false, true, false, false);
			}
			break;
		default:
			break;
		}
	}

	private void setMove(boolean forwardMove, boolean leftMove, boolean backwardMove, boolean rightMove) {
		moves = 0;
		this.forwardMove = forwardMove;
		this.leftMove = leftMove;
		this.backwardMove = backwardMove;
		this.rightMove = rightMove;
	}

	private int getTileID(Vector2 position) {
		return gameScreen.getTiledLayer().getCell((int) (position.x / gameScreen.getTiledLayer().getTileWidth()), (int) (position.y / gameScreen.getTiledLayer().getTileHeight())).getTile().getId();
	}

	private boolean isTileSolid(Vector2 position) {
		Cell cell = gameScreen.getTiledLayer().getCell((int) (position.x / gameScreen.getTiledLayer().getTileWidth()), (int) (position.y / gameScreen.getTiledLayer().getTileHeight()));
		return cell != null && cell.getTile() != null && (cell.getTile().getProperties().containsKey("solid") || cell.getTile().getProperties().get("name").equals("explosion"));
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getSpeed() {
		return speed;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public void setTargetPosition(Vector2 targetPosition) {
		previousTargetPosition = this.targetPosition;
		this.targetPosition = targetPosition;
	}

	public void setPosition(float x, float y) {
		previousPosition.set(position.x, position.y);
		position.set(x, y);
	}
	//	
	//	public SPGameScreen getGameScreen() {
	//		return gameScreen;
	//	}
}
