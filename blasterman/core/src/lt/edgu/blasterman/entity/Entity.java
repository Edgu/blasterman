package lt.edgu.blasterman.entity;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public abstract class Entity extends Sprite {
	
	protected Vector2 position;
	protected int bombs;
	protected int bombStrenght;
	protected int speed;
	
	public Entity(TextureRegion textureRegion) {
		super(textureRegion);
		position = new Vector2();
	}
	
	public Vector2 getPosition() {
		return position;
	}
	
	public void setPosition(Vector2 position) {
		this.position = position;
	}
	
	public Vector2 getRoundedPosition() {
		return new Vector2(Math.round(position.x) / 32, Math.round(position.y) / 32);
	}
	
	public void setSpeed(int speed) {
		if (this.speed < 224 & speed < 224 && (speed - this.speed) == 32) {
			this.speed = speed;
		}
	}

	public int getSpeed() {
		return speed;
	}
}
