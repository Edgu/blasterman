package lt.edgu.blasterman.entity;

import java.util.Random;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.screens.SPGameScreen;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;

public class SPPlayer extends Entity implements InputProcessor {
	private int bombs;
	private int speed;
	private int bombStrenght;

	// Animation Stuff
	public Animation forwardAnimation, leftAnimation, backwardAnimation, rightAnimation;

	// Movement variables.
	public boolean forwardMove;
	public boolean backwardMove;
	public boolean rightMove;
	public boolean leftMove;

	private Vector2 targetPosition;
	private int roundedTargetPosition;

	// Animation Settings
	private float animationTime;
	private boolean disposed;
	private char currentMove;
	private boolean dead;
	private SPGameScreen gameScreen;

	public SPPlayer(SPGameScreen gameScreen) {
		super(TextureManager.playerAtlasYellow.findRegion("forward_frame"));
		this.gameScreen = gameScreen;
		dead = false;
		speed = 64;
		bombs = 1;
		bombStrenght = 1;
		currentMove = 'x';
		disposed = false;
		position = new Vector2(96, 96);
		animationTime = 0;

		forwardAnimation = TextureManager.yellowForwardAnimation;
		leftAnimation = TextureManager.yellowLeftAnimation;
		backwardAnimation = TextureManager.yellowBackwardAnimation;
		rightAnimation = TextureManager.yellowRightAnimation;

		setRegion(forwardAnimation.getKeyFrame(0));
	}

	public void render(Batch batch, float delta) {
		if (!isDead()) {
			if (!disposed) {
				batch.draw(this, getPosition().x, getPosition().y);
				update(delta);
			}
		}
	}

	public void update(float delta) {
		animationTime += delta;
		try {
			if ((forwardMove && currentMove == 'x' || currentMove == 'w')) {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) position.x, (int) (position.y + this.getHeight() / 2));
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
				if (!isTileSolid(new Vector2(position.x, roundedTargetPosition)) && position.y <= roundedTargetPosition) {
					if (currentMove != 'w') {
						currentMove = 'w';
					}
					setRegion(forwardAnimation.getKeyFrame(animationTime));
					position.y += speed * delta;
					if (position.y >= roundedTargetPosition) {
						position.y = roundedTargetPosition;
						currentMove = 'x';
					}
				}
			} else if (leftMove && currentMove == 'x' || currentMove == 'a') {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) (position.x - this.getWidth()), (int) position.y);
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
				if (!isTileSolid(new Vector2(roundedTargetPosition, position.y)) && position.x >= roundedTargetPosition) {
					if (currentMove != 'a') {
						currentMove = 'a';
					}
					setRegion(leftAnimation.getKeyFrame(animationTime));
					position.x -= speed * delta;
					if (position.x <= roundedTargetPosition) {
						position.x = roundedTargetPosition;
						currentMove = 'x';
					}
				}
			} else if (backwardMove && currentMove == 'x' || currentMove == 's') {
				if (currentMove == 'x') {
					targetPosition = new Vector2((int) position.x, (int) (position.y - this.getHeight()));
				}
				roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
				if (!isTileSolid(new Vector2(position.x, roundedTargetPosition)) && position.y >= roundedTargetPosition) {
					if (currentMove != 's') {
						currentMove = 's';
					}
					setRegion(backwardAnimation.getKeyFrame(animationTime));
					position.y -= speed * delta;
					if (position.y <= roundedTargetPosition) {
						position.y = roundedTargetPosition;
						currentMove = 'x';
					}
				}
			} else if (rightMove && currentMove == 'x' || currentMove == 'd') {
				targetPosition = new Vector2((int) (position.x + this.getWidth() / 2), (int) position.y);
				roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
				if (!isTileSolid(new Vector2(roundedTargetPosition, position.y)) && position.x <= roundedTargetPosition) {
					if (currentMove != 'd') {
						currentMove = 'd';
					}
					setRegion(rightAnimation.getKeyFrame(animationTime));
					position.x += speed * delta;
					if (position.x >= roundedTargetPosition) {
						position.x = roundedTargetPosition;
						currentMove = 'x';
					}
				}
			} else {
				// Update still animation based on rotation
				switch (currentMove) {
				case 'w':
					setRegion(forwardAnimation.getKeyFrame(0));
					break;
				case 'a':
					setRegion(leftAnimation.getKeyFrame(0));
					break;
				case 's':
					setRegion(backwardAnimation.getKeyFrame(0));
					break;
				case 'd':
					setRegion(rightAnimation.getKeyFrame(0));
					break;
				}
			}
		} catch (NullPointerException exception) {
			exception.printStackTrace();
		}
		checkStep();
	}

	private void checkStep() {
		int x = (int) (position.x / gameScreen.getTiledLayer().getTileWidth());
		int y = (int) (position.y / gameScreen.getTiledLayer().getTileHeight());
		Cell cell = gameScreen.getTiledLayer().getCell(x, y);

		// Checks if player steps on these tiles.
		switch (cell.getTile().getId()) {
		case 10:
			// Additional Bomb
			if (bombs < 6) {
				bombs++;
			}
			setGrass(x, y, cell.getTile().getId());
			break;
		case 11:
			// Bomb Strength
			bombStrenght += 1;
			setGrass(x, y, cell.getTile().getId());
			break;
		case 12:
			// Random
			Random random = new Random();
			int powerUp = random.nextInt((3 - 1) + 1) + 1;
			switch (powerUp) {
			case 1:
				// Speed Boost
				if (speed < 192) {
					speed += 32;
				}
				break;
			case 2:
				// Additional Bomb
				if (bombs < 6) {
					bombs++;
				}
				break;
			case 3:
				// Bomb str
				bombStrenght += 1;
				break;
			}
			setGrass(x, y, cell.getTile().getId());
			break;
		case 13:
			// Speed Boost
			if (speed < 192) {
				speed += 32;
			}
			setGrass(x, y, cell.getTile().getId());
			break;
		case 555:
			// Explosion
			setDead(true);
			break;
		}
	}

	private void setGrass(int x, int y, int tileID) {
		if (tileID != 555) {
			AudioManager.powerUp.play();
		}
		gameScreen.getTiledLayer().getCell(x, y).setTile(gameScreen.getTiledManager().grassTile);
	}

	private boolean isTileSolid(Vector2 position) {
		Cell cell = gameScreen.getTiledLayer().getCell((int) (position.x / gameScreen.getTiledLayer().getTileWidth()), (int) (position.y / gameScreen.getTiledLayer().getTileHeight()));
		return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey("solid");
	}

	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
		case Keys.W:
			forwardMove = true;
			break;
		case Keys.A:
			leftMove = true;
			break;
		case Keys.S:
			backwardMove = true;
			break;
		case Keys.D:
			rightMove = true;
			break;
		case Keys.SPACE:
			if (!dead && bombs > 0) {
				AudioManager.plantBomb.play();
				switch (currentMove) {
				case 'w':
					gameScreen.createNewBomb(new Vector2((int) ((getPosition().x + getWidth() / 2) / gameScreen.getTiledLayer().getTileWidth()) * 32,
							(int) (getPosition().y / gameScreen.getTiledLayer().getTileHeight()) * 32));
					break;
				case 'a':
					gameScreen.createNewBomb(new Vector2((int) (((getPosition().x + 15) + getWidth() / 2) / gameScreen.getTiledLayer().getTileWidth()) * 32,
							(int) ((getPosition().y + getHeight() / 2) / gameScreen.getTiledLayer().getTileHeight()) * 32));
					break;
				case 's':
					gameScreen.createNewBomb(new Vector2((int) ((getPosition().x + getWidth() / 2) / gameScreen.getTiledLayer().getTileWidth()) * 32,
							(int) ((getPosition().y + 31) / gameScreen.getTiledLayer().getTileHeight()) * 32));
					break;
				case 'd':
					gameScreen.createNewBomb(new Vector2((int) (((getPosition().x - 16) + getWidth() / 2) / gameScreen.getTiledLayer().getTileWidth()) * 32,
							(int) ((getPosition().y + getHeight() / 2) / gameScreen.getTiledLayer().getTileHeight()) * 32));
					break;
				case 'x':
					gameScreen.createNewBomb(new Vector2((int) ((getPosition().x + getWidth() / 2) / gameScreen.getTiledLayer().getTileWidth()) * 32,
							(int) (getPosition().y / gameScreen.getTiledLayer().getTileHeight()) * 32));
					break;
				}
			}
			break;
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch (keycode) {
		case Keys.W:
			forwardMove = false;
			break;
		case Keys.A:
			leftMove = false;
			break;
		case Keys.S:
			backwardMove = false;
			break;
		case Keys.D:
			rightMove = false;
			break;
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public void die() {
		disposed = true;
		//		gameScreen.playerDie();
		dispose();

	}

	public void dispose() {
		//		this.getTexture().dispose();
	}

	public int getBombs() {
		return bombs;
	}

	public void setBombs(int bombs) {
		if (bombs < 6) {
			this.bombs = bombs;
		}
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public char getCurrentMove() {
		return currentMove;
	}

	public int getBombStrenght() {
		return bombStrenght;
	}
	
	public Vector2 getRoundedPosition() {
		return new Vector2(Math.round(position.x) / 32, Math.round(position.y) / 32);
	}
}