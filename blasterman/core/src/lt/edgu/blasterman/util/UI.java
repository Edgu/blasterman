package lt.edgu.blasterman.util;

import lt.edgu.blasterman.TextureManager;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class UI {
	public static Table gameFinish(String winner, Skin skin, Stage stage) {
		Table table = new Table(skin);
		table.setPosition(0.291F * stage.getWidth(), 0.1F * stage.getHeight());
		table.setSize(0.653F * stage.getWidth(), (0.400F * 2) * stage.getHeight());
		table.setBackground(TextureManager.gameOver.getDrawable());
		Label label;
		if (winner.equals("NOWIN")) {
			label = new Label("DRAW", skin, "default32");
		} else {
			label = new Label(winner + " has won the game!", skin, "default32");
		}
		table.add(label).center().height(150).fillX();
		stage.addActor(table);
		return table;
	}
}