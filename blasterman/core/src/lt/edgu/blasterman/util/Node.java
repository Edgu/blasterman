package lt.edgu.blasterman.util;

import com.badlogic.gdx.math.Vector2;

public class Node {
	
	// Node position.
	public Vector2 position;
	// Node which created this node.
	public Node parent;
	/*
	 * hCost - (Heuristic) - Cost from a node to the target node.
	 * gCost - Cost from parent plus this node.
	 * fCost -  
	 */
	public double fCost, gCost, hCost;
	
	public Node(Vector2 position, Node parent, double gCost, double hCost) {
		this.position = position;
		this.parent = parent;
		this.gCost = gCost;
		this.hCost = hCost;
		this.fCost = this.gCost + this.hCost;
	}
	
	@Override
	public String toString() {
		return "[" + position.x + ", " + position.y + "]";
	}

}
