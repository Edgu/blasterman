package lt.edgu.blasterman.network;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import lt.edgu.blasterman.AudioManager;
import lt.edgu.blasterman.Blasterman;
import lt.edgu.blasterman.TextureManager;
import lt.edgu.blasterman.entity.OtherPlayer;
import lt.edgu.blasterman.network.packets.PacketJoinRoom;
import lt.edgu.blasterman.network.packets.PacketNewMessage;
import lt.edgu.blasterman.network.packets.room.PacketAdditionalBomb;
import lt.edgu.blasterman.network.packets.room.PacketEndGame;
import lt.edgu.blasterman.network.packets.room.PacketExplosionTiles;
import lt.edgu.blasterman.network.packets.room.PacketMove;
import lt.edgu.blasterman.network.packets.room.PacketNewBomb;
import lt.edgu.blasterman.network.packets.room.PacketNewPlayer;
import lt.edgu.blasterman.network.packets.room.PacketPlayerDeath;
import lt.edgu.blasterman.network.packets.room.PacketPlayerSpeed;
import lt.edgu.blasterman.network.packets.room.PacketRandomPowerUp;
import lt.edgu.blasterman.network.packets.room.PacketRemovePlayer;
import lt.edgu.blasterman.network.packets.room.PacketSetHost;
import lt.edgu.blasterman.network.packets.room.PacketSetTile;
import lt.edgu.blasterman.network.packets.room.PacketSetUsername;
import lt.edgu.blasterman.network.packets.room.PacketSpeedBoost;
import lt.edgu.blasterman.network.packets.room.PacketStartGame;
import lt.edgu.blasterman.screens.MPGameScreen;
import lt.edgu.blasterman.screens.RoomsScreen;
import lt.edgu.blasterman.util.TileE;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage.Ping;
import com.esotericsoftware.kryonet.Listener;

public class RoomServerClient extends Listener {

	private Client client;
	private MPGameScreen gameScreen;
	private RoomsScreen roomsScreen;
	private int port;
	public boolean roomStarted;

	public RoomServerClient(RoomsScreen roomsScreen, MPGameScreen gameScreen, int port) {
		this.roomsScreen = roomsScreen;
		this.gameScreen = gameScreen;
		this.port = port;
		roomStarted = false;
		client = new Client();
		registerPackets();
		getClient().addListener(this);
	}

	public void connect() {
		new Thread(client).start();
		try {
			client.connect(5000, Blasterman.SERVER_ADDRESS, port, port);
			PacketSetUsername packet = new PacketSetUsername();
			packet.id = client.getID();
			packet.username = gameScreen.getUsername();
			client.sendTCP(packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void registerPackets() {
		Kryo kryo = client.getKryo();
		kryo.register(PacketNewPlayer.class);
		kryo.register(PacketPlayerDeath.class);
		kryo.register(PacketRemovePlayer.class);
		kryo.register(PacketSetUsername.class);
		kryo.register(PacketAdditionalBomb.class);
		kryo.register(PacketSpeedBoost.class);
		kryo.register(PacketNewBomb.class);
		kryo.register(PacketExplosionTiles.class);
		kryo.register(PacketRandomPowerUp.class);
		kryo.register(PacketMove.class);
		kryo.register(PacketPlayerSpeed.class);
		kryo.register(ArrayList.class);
		kryo.register(TileE.class);
		kryo.register(PacketJoinRoom.class);
		kryo.register(PacketNewMessage.class);
		kryo.register(PacketStartGame.class);
		kryo.register(PacketSetTile.class);
		kryo.register(PacketSetHost.class);
		kryo.register(PacketEndGame.class);
	}

	@Override
	public void connected(Connection connection) {
		client.updateReturnTripTime();
	}

	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof Ping) {
			Ping ping = (Ping) object;
			if (ping.isReply)
				getClient().updateReturnTripTime();
		}
		if (object instanceof PacketNewPlayer) {
			PacketNewPlayer packet = (PacketNewPlayer) object;
			if (packet.id == client.getID()) {
				gameScreen.getPlayer().setPosition(new Vector2(packet.x, packet.y));
				switch (packet.colour) {
				case 0:
					gameScreen.getPlayer().forwardAnimation = TextureManager.yellowForwardAnimation;
					gameScreen.getPlayer().leftAnimation = TextureManager.yellowLeftAnimation;
					gameScreen.getPlayer().backwardAnimation = TextureManager.yellowBackwardAnimation;
					gameScreen.getPlayer().rightAnimation = TextureManager.yellowRightAnimation;

					gameScreen.getPlayer().setRegion(gameScreen.getPlayer().forwardAnimation.getKeyFrame(0));
					gameScreen.playerYellow.add(TextureManager.yellowPlayer);
					gameScreen.yellowPlayerName.setText(gameScreen.getUsername());
					break;
				case 1:
					gameScreen.getPlayer().forwardAnimation = TextureManager.blueForwardAnimation;
					gameScreen.getPlayer().leftAnimation = TextureManager.blueLeftAnimation;
					gameScreen.getPlayer().backwardAnimation = TextureManager.blueBackwardAnimation;
					gameScreen.getPlayer().rightAnimation = TextureManager.blueRightAnimation;

					gameScreen.getPlayer().setRegion(gameScreen.getPlayer().forwardAnimation.getKeyFrame(0));
					gameScreen.playerBlue.add(TextureManager.bluePlayer);
					gameScreen.bluePlayerName.setText(gameScreen.getUsername());
					break;
				case 2:
					gameScreen.getPlayer().forwardAnimation = TextureManager.redForwardAnimation;
					gameScreen.getPlayer().leftAnimation = TextureManager.redLeftAnimation;
					gameScreen.getPlayer().backwardAnimation = TextureManager.redBackwardAnimation;
					gameScreen.getPlayer().rightAnimation = TextureManager.redRightAnimation;

					gameScreen.getPlayer().setRegion(gameScreen.getPlayer().forwardAnimation.getKeyFrame(0));
					gameScreen.playerRed.add(TextureManager.redPlayer);
					gameScreen.redPlayerName.setText(gameScreen.getUsername());
					break;
				case 3:
					gameScreen.getPlayer().forwardAnimation = TextureManager.grayForwardAnimation;
					gameScreen.getPlayer().leftAnimation = TextureManager.grayLeftAnimation;
					gameScreen.getPlayer().backwardAnimation = TextureManager.grayBackwardAnimation;
					gameScreen.getPlayer().rightAnimation = TextureManager.grayRightAnimation;

					gameScreen.getPlayer().setRegion(gameScreen.getPlayer().forwardAnimation.getKeyFrame(0));
					gameScreen.playerGray.add(TextureManager.grayPlayer);
					gameScreen.grayPlayerName.setText(gameScreen.getUsername());
					break;
				}
			} else {
				OtherPlayer newPlayer = new OtherPlayer(gameScreen.tiledManager, gameScreen.getTiledLayer());
				newPlayer.setId(packet.id);
				newPlayer.setColour(packet.colour);
				newPlayer.setPosition(new Vector2(packet.x, packet.y));
				newPlayer.setUsername(packet.username);
				if (packet.username == "NOT SET") {
					packet.username = "";
				}
				switch (packet.colour) {
				case 0:
					newPlayer.forwardAnimation = TextureManager.yellowForwardAnimation;
					newPlayer.leftAnimation = TextureManager.yellowLeftAnimation;
					newPlayer.backwardAnimation = TextureManager.yellowBackwardAnimation;
					newPlayer.rightAnimation = TextureManager.yellowRightAnimation;

					newPlayer.setRegion(newPlayer.forwardAnimation.getKeyFrame(0));
					gameScreen.playerYellow.add(TextureManager.yellowPlayer).fill();
					gameScreen.yellowPlayerName.setText(packet.username);
					newPlayer.setSpritesSet(true);
					break;
				case 1:
					newPlayer.forwardAnimation = TextureManager.blueForwardAnimation;
					newPlayer.leftAnimation = TextureManager.blueLeftAnimation;
					newPlayer.backwardAnimation = TextureManager.blueBackwardAnimation;
					newPlayer.rightAnimation = TextureManager.blueRightAnimation;

					newPlayer.setRegion(newPlayer.forwardAnimation.getKeyFrame(0));
					gameScreen.playerBlue.add(TextureManager.bluePlayer).fill();
					gameScreen.bluePlayerName.setText(packet.username);
					newPlayer.setSpritesSet(true);
					break;
				case 2:
					newPlayer.forwardAnimation = TextureManager.redForwardAnimation;
					newPlayer.leftAnimation = TextureManager.redLeftAnimation;
					newPlayer.backwardAnimation = TextureManager.redBackwardAnimation;
					newPlayer.rightAnimation = TextureManager.redRightAnimation;

					newPlayer.setRegion(newPlayer.forwardAnimation.getKeyFrame(0));
					gameScreen.playerRed.add(TextureManager.redPlayer).fill();
					gameScreen.redPlayerName.setText(packet.username);
					newPlayer.setSpritesSet(true);
					break;
				case 3:
					newPlayer.forwardAnimation = TextureManager.grayForwardAnimation;
					newPlayer.leftAnimation = TextureManager.grayLeftAnimation;
					newPlayer.backwardAnimation = TextureManager.grayBackwardAnimation;
					newPlayer.rightAnimation = TextureManager.grayRightAnimation;

					newPlayer.setRegion(newPlayer.forwardAnimation.getKeyFrame(0));
					gameScreen.playerGray.add(TextureManager.grayPlayer).fill();
					gameScreen.grayPlayerName.setText(packet.username);
					newPlayer.setSpritesSet(true);
					break;
				default:
					break;
				}
				gameScreen.players.put(packet.id, newPlayer);
			}
		} else if (object instanceof PacketSetUsername) {
			PacketSetUsername packet = (PacketSetUsername) object;
			gameScreen.players.get(packet.id).setUsername(packet.username);
			switch (gameScreen.players.get(packet.id).getColour()) {
			case 0:
				gameScreen.yellowPlayerName.setText(packet.username);
				break;
			case 1:
				gameScreen.bluePlayerName.setText(packet.username);
				break;
			case 2:
				gameScreen.redPlayerName.setText(packet.username);
				break;
			case 3:
				gameScreen.grayPlayerName.setText(packet.username);
				break;
			default:
				break;
			}
		} else if (object instanceof PacketRemovePlayer) {
			PacketRemovePlayer packet = (PacketRemovePlayer) object;
			if (gameScreen.players.get(packet.id) != null) {
				switch (gameScreen.players.get(packet.id).getColour()) {
				case 0:
					gameScreen.playerYellow.removeActor(TextureManager.yellowPlayer);
					gameScreen.yellowPlayerName.setText("");
					break;
				case 1:
					gameScreen.playerBlue.removeActor(TextureManager.bluePlayer);
					gameScreen.bluePlayerName.setText("");
					break;
				case 2:
					gameScreen.playerRed.removeActor(TextureManager.redPlayer);
					gameScreen.redPlayerName.setText("");
					break;
				case 3:
					gameScreen.playerGray.removeActor(TextureManager.grayPlayer);
					gameScreen.grayPlayerName.setText("");
					break;
				default:
					break;
				}
				gameScreen.players.remove(packet.id);
			}
		} else if (object instanceof PacketPlayerDeath) {
			PacketPlayerDeath packet = (PacketPlayerDeath) object;
			if (packet.id == gameScreen.getRoomServerClient().getClient().getID()) {
				gameScreen.backButton.setVisible(true);
				gameScreen.getPlayer().die();
			} else {
				gameScreen.players.get(packet.id).setAlive(false);
			}
		} else if (object instanceof PacketNewBomb) {
			PacketNewBomb packet = (PacketNewBomb) object;
			if (packet.id == gameScreen.getRoomServerClient().getClient().getID()) {
				if (gameScreen.getPlayer() != null && gameScreen.getPlayer().getBombs() > 0) {
					gameScreen.getPlayer().setBombs(gameScreen.getPlayer().getBombs() - 1);
				}
			}
			gameScreen.getTiledLayer().getCell(packet.x, packet.y).setTile(gameScreen.tiledManager.bombTile);
		} else if (object instanceof PacketSetTile) {
			PacketSetTile packet = (PacketSetTile) object;
			TiledMapTile receivedTile = null;
			switch (packet.tile) {
			case 10:
				receivedTile = gameScreen.tiledManager.additionalBombPowerUp;
				break;
			case 11:
				receivedTile = gameScreen.tiledManager.bombStrengthPowerUp;
				break;
			case 12:
				receivedTile = gameScreen.tiledManager.randomPowerUp;
				break;
			case 13:
				receivedTile = gameScreen.tiledManager.speedBoostPowerUp;
				break;
			case 14:
				receivedTile = gameScreen.tiledManager.grassTile;
				break;
			}
			gameScreen.getTiledLayer().getCell(packet.x, packet.y).setTile(receivedTile);
		} else if (object instanceof PacketExplosionTiles) {
			AudioManager.explosion.play();
			PacketExplosionTiles packet = (PacketExplosionTiles) object;
			if (packet.id == gameScreen.getRoomServerClient().getClient().getID()) {
				if (gameScreen.getPlayer() != null && gameScreen.getPlayer().getBombs() < 6) {
					gameScreen.getPlayer().setBombs(gameScreen.getPlayer().getBombs() + 1);
				}
			}
			ArrayList<TileE> tiles = packet.tiles;
			for (int i = 0; i < tiles.size(); i++) {
				gameScreen.getTiledLayer().getCell((int) tiles.get(i).x, (int) tiles.get(i).y).setTile(gameScreen.tiledManager.explosionTile);
			}
		} else if (object instanceof PacketAdditionalBomb) {
			gameScreen.getPlayer().setBombs(gameScreen.getPlayer().getBombs() + 1);
		} else if (object instanceof PacketSpeedBoost) {
			PacketSpeedBoost packet = (PacketSpeedBoost) object;
			if (packet.id == client.getID()) {
				gameScreen.getPlayer().setSpeed(gameScreen.getPlayer().getSpeed() + 32);
			} else {
				OtherPlayer player = gameScreen.players.get(packet.id);
				player.setSpeed(player.getSpeed() + 32);
			}
		} else if (object instanceof PacketMove) {
			PacketMove packet = (PacketMove) object;
			if (gameScreen.players.get(packet.id) != null) {
				gameScreen.players.get(packet.id).getMoves().add(packet.direction);
			}
		} else if (object instanceof PacketJoinRoom) {
			PacketJoinRoom packet = (PacketJoinRoom) object;
			roomsScreen.joined = packet.connect;
			roomsScreen.receivedPacket = true;
		} else if (object instanceof PacketNewMessage) {
			if (gameScreen != null) {
				PacketNewMessage packet = (PacketNewMessage) object;
				gameScreen.newMessage(packet.message, packet.sender, true);
			}
		} else if (object instanceof PacketStartGame)  {
			if (((Game) Gdx.app.getApplicationListener()).getScreen() != gameScreen) {
				Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                    	((Game) Gdx.app.getApplicationListener()).setScreen(gameScreen);
                    }
                });
			}
			gameScreen.newMessage("Game starts in 5 seconds...", "---ROOM NOTIFICATION---", true);
			gameScreen.backButton.setVisible(false);
			gameScreen.hostButton.setVisible(false);
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					start();
				}
			}, 5000);
		} else if (object instanceof PacketSetHost) {
			gameScreen.hostButton.setVisible(true);
			gameScreen.leftBottomButtons.setTouchable(Touchable.enabled);
			roomsScreen.currentRoom.host = roomsScreen.username;
		} else if (object instanceof PacketEndGame) {
			gameScreen.hostButton.setVisible(false);
			roomsScreen.currentRoom = null;
			PacketEndGame packet = (PacketEndGame) object;
			if (gameScreen.touchpad != null) {
				gameScreen.touchpad.setVisible(false);
			}
			gameScreen.backButton.setVisible(true);
			gameScreen.gameOver(packet.winner);
		}
	}

	public void start() {
		roomStarted = true;
		gameScreen.backButton.setVisible(false);
		gameScreen.chat.setVisible(false);
		gameScreen.chat.getCell(gameScreen.messageField).getActor().clearListeners();
		gameScreen.updateCam();
		if (gameScreen.isUsingPhone()) {
			gameScreen.touchpad.setVisible(true);
			gameScreen.touchpad.setTouchable(Touchable.enabled);
			gameScreen.bombButton.setVisible(true);
			gameScreen.bombButton.setTouchable(Touchable.enabled);
		}
		if (gameScreen.getPlayer() != null) {
			gameScreen.getInputMultiplexer().addProcessor(gameScreen.getPlayer());
		}
	}

	@Override
	public void disconnected(Connection connection) {
		roomsScreen.currentRoom = null;
		//		System.out.println("Client is no longer connected to the room.");
	}

	public MPGameScreen getGameScreen() {
		return gameScreen;
	}

	public void setGameScreen(MPGameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public Client getClient() {
		return client;
	}
}