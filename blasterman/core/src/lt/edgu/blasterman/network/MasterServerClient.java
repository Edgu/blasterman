package lt.edgu.blasterman.network;

import java.util.ArrayList;

import lt.edgu.blasterman.Blasterman;
import lt.edgu.blasterman.network.packets.PacketCreateRoom;
import lt.edgu.blasterman.network.packets.PacketDeleteRoom;
import lt.edgu.blasterman.network.packets.PacketJoinRoom;
import lt.edgu.blasterman.network.packets.PacketNewMessage;
import lt.edgu.blasterman.network.packets.PacketRequestRoomList;
import lt.edgu.blasterman.network.packets.PacketRoomsList;
import lt.edgu.blasterman.network.packets.PacketUpdateRoom;
import lt.edgu.blasterman.network.packets.room.PacketNewPlayer;
import lt.edgu.blasterman.network.packets.room.PacketSetUsername;
import lt.edgu.blasterman.screens.MPGameScreen;
import lt.edgu.blasterman.screens.LobbyScreen;
import lt.edgu.blasterman.screens.RoomsScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class MasterServerClient extends Listener {

	private Client client;
	private final String ip = Blasterman.SERVER_ADDRESS;
	private int tcpPort = 54555, udpPort = 54555;

	private MPGameScreen gameScreen;
	private LobbyScreen lobbyScreen;
	private RoomsScreen roomsScreen;

	public void connect(String username) {
		//System.out.println("Connecting to master server.");
		client = new Client();
		registerPackets();
		client.addListener(this);
		new Thread(client).start();
		try {
			client.connect(5000, ip, tcpPort, udpPort);
			//System.out.println("Client has succesfully connected to the master server!");
			PacketSetUsername packet = new PacketSetUsername();
			packet.id = client.getID();
			packet.username = username;
			client.sendTCP(packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerPackets() {
		Kryo kryo = client.getKryo();
		kryo.register(PacketNewPlayer.class);
		kryo.register(PacketNewMessage.class);
		kryo.register(PacketSetUsername.class);
		kryo.register(PacketRequestRoomList.class);
		kryo.register(PacketRoomsList.class);
		kryo.register(PacketCreateRoom.class);
		kryo.register(PacketJoinRoom.class);
		kryo.register(PacketUpdateRoom.class);
		kryo.register(ArrayList.class);
		kryo.register(MPRoom.class);
		kryo.register(PacketDeleteRoom.class);
	}

	@Override
	public void connected(Connection connection) {
		client.updateReturnTripTime();
	}

	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof PacketRoomsList) {
			if (((Game) Gdx.app.getApplicationListener()).getScreen() == roomsScreen) {
				PacketRoomsList packet = (PacketRoomsList) object;
				if (!packet.rooms.isEmpty()) {
					roomsScreen.tempRoomList = packet.rooms;
					int lastSelected = roomsScreen.lastSelected;
					roomsScreen.rooms.setItems(convertToArray(roomsScreen.tempRoomList));
					if (lastSelected > -1) {
						try {
							roomsScreen.rooms.setSelectedIndex(lastSelected);
						} catch (IllegalArgumentException e) {}
					}
				} else {
					roomsScreen.rooms.clearItems();
				}
			}
		} else if (object instanceof PacketNewMessage) {
			if (((Game) Gdx.app.getApplicationListener()).getScreen() == lobbyScreen) {
				PacketNewMessage packet = (PacketNewMessage) object;
				lobbyScreen.newMessage(packet.message, packet.sender, true);
			}
		} else if (object instanceof PacketCreateRoom) {
			PacketCreateRoom packet = (PacketCreateRoom) object;
			MPRoom room = new MPRoom();
			room.host = packet.host;
			room.name = packet.name;
			room.mapName = packet.mapName;
			room.port = packet.port;
			room.players = packet.players;
			roomsScreen.rooms.getItems().add(room);
			if (room.host.equals(roomsScreen.username)) {
				roomsScreen.rooms.setSelected(room);
				roomsScreen.currentRoom = room;
				roomsScreen.joinCreatedRoom = 1;
			}
		} else if (object instanceof PacketDeleteRoom) {
			PacketDeleteRoom packet = (PacketDeleteRoom) object;
			for (MPRoom mpRoom : roomsScreen.rooms.getItems()) {
				if (mpRoom.port == packet.port) {
					if (roomsScreen.rooms.getSelected().port == mpRoom.port) {
						roomsScreen.cleanLabels();
					}
					roomsScreen.rooms.getItems().removeValue(mpRoom, false);
				}
			}
		} else if (object instanceof PacketJoinRoom) {

		}
	}

	public MPRoom[] convertToArray(ArrayList<MPRoom> rooms) {
		MPRoom[] roomsArray = new MPRoom[rooms.size()];
		for (int i = 0; i < roomsArray.length; i++) {
			roomsArray[i] = rooms.get(i);
		}
		return roomsArray;
	}

	@Override
	public void disconnected(Connection connection) {
		//System.out.println("Client is no longer connected to the game server.");
	}

	public MPGameScreen getGameScreen() {
		return gameScreen;
	}

	public void setGameScreen(MPGameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}

	public LobbyScreen getLobbyScreen() {
		return lobbyScreen;
	}

	public void setLobbyScreen(LobbyScreen lobbyScreen) {
		this.lobbyScreen = lobbyScreen;
	}

	public RoomsScreen getRoomsScreen() {
		return roomsScreen;
	}

	public void setRoomsScreen(RoomsScreen roomsScreen) {
		this.roomsScreen = roomsScreen;
	}

	public Client getClient() {
		return client;
	}
}