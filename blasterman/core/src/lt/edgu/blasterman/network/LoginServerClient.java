package lt.edgu.blasterman.network;

import lt.edgu.blasterman.Blasterman;
import lt.edgu.blasterman.network.packets.auth.PacketLogin;
import lt.edgu.blasterman.network.packets.auth.PacketLoginFailure;
import lt.edgu.blasterman.network.packets.auth.PacketLoginSuccess;
import lt.edgu.blasterman.network.packets.auth.PacketRegister;
import lt.edgu.blasterman.network.packets.auth.PacketRegisterFailure;
import lt.edgu.blasterman.network.packets.auth.PacketRegisterSuccess;
import lt.edgu.blasterman.screens.AuthScreen;
import lt.edgu.blasterman.screens.RoomsScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage.Ping;
import com.esotericsoftware.kryonet.Listener;

public class LoginServerClient extends Listener {

	public Client client;
	private final String ip = Blasterman.SERVER_ADDRESS;
	private int tcpPort = 54554, udpPort = 54554;
	private AuthScreen authScreen;
	
	public LoginServerClient(AuthScreen authScreen) {
		this.authScreen = authScreen;
	}

	public void connect() {
		System.out.println("Connecting to login server...");
		client = new Client();
		registerPackets();
		client.addListener(this);
		/* Starts a new Client. */
		new Thread(client).start();
		try {
			/* Connects to the server. */
			client.connect(5000, ip, tcpPort, udpPort);
			System.out.println("Succesfully connected to the login server!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void registerPackets() {
		Kryo kryo = client.getKryo();
		kryo.register(PacketLogin.class);
		kryo.register(PacketLoginSuccess.class);
		kryo.register(PacketLoginFailure.class);
		kryo.register(PacketRegister.class);
		kryo.register(PacketRegisterSuccess.class);
		kryo.register(PacketRegisterFailure.class);
	}
	/** Called when the remote end has been connected. */
	@Override
	public void connected(Connection connection) {
		client.updateReturnTripTime();
	}

	/** Called when an object has been received from the remote end of the connection. */
	@Override
	public void received(Connection connection, Object object) {
		if (object instanceof Ping) {
			Ping ping = (Ping) object;
			if (ping.isReply)
				client.updateReturnTripTime();
		}
		if (object instanceof PacketLoginSuccess) {
			final PacketLoginSuccess packet = (PacketLoginSuccess) object;
			Gdx.app.postRunnable(new Runnable() {
				@Override
				public void run() {
					((Game) Gdx.app.getApplicationListener()).setScreen(new RoomsScreen(packet.username));
				}
			});
		} else if (object instanceof PacketLoginFailure) {
			authScreen.loginButton.setTouchable(Touchable.enabled);
			authScreen.loginButton.setDisabled(false);
			switch (((PacketLoginFailure) object).errorCode) {
			case -2:
				authScreen.statusField.setText("STATUS: User is already logged in!");
				break;
			case -1:
				authScreen.statusField.setText("STATUS: Error connecting to database, try again.");
				break;
			case 0:
				authScreen.statusField.setText("STATUS: Incorrect username or password!");
				break;
			}
		} else if (object instanceof PacketRegisterSuccess) {
			final PacketRegisterSuccess packet = (PacketRegisterSuccess) object;
			Gdx.app.postRunnable(new Runnable() {
				@Override
				public void run() {
					((Game) Gdx.app.getApplicationListener()).setScreen(new RoomsScreen(packet.username));
				}
			});
		} else if (object instanceof PacketRegisterFailure) {
			authScreen.registerButton.setTouchable(Touchable.enabled);
			authScreen.registerButton.setDisabled(false);
			switch (((PacketRegisterFailure) object).errorCode) {
			case -6:
				authScreen.statusField.setText("STATUS: Username is too long! (has to be < 9 chars)");
				break;
			case -5:
				authScreen.statusField.setText("STATUS: Username is too short! (has to be > 0 chars)");
				break;
			case -4:
				authScreen.statusField.setText("STATUS: Email is already registered!");
				break;
			case -3:
				authScreen.statusField.setText("STATUS: Invalid Email!");
				break;
			case -2:
				authScreen.statusField.setText("STATUS: Username is already taken!");
				break;
			case -1:
				authScreen.statusField.setText("STATUS: Error connecting to database, try again.");
				break;
			case 0:
				authScreen.statusField.setText("STATUS: Passwords do not match!");
				break;
			}
		}
	}

	/** Called when the remote end is no longer connected. There is no guarantee as to what thread will invoke this method. */
	@Override
	public void disconnected(Connection connection) {
		System.out.println("Client is no longer connected to the login server.");
	}
}