package lt.edgu.blasterman.network.packets;

import java.util.ArrayList;

public class PacketCreateRoom {
	public int id;
	public String host;
	public String name;
	public String mapName;
	public int port;
	public ArrayList<String> players;
}