package lt.edgu.blasterman.network.packets.room;

import java.util.ArrayList;

import lt.edgu.blasterman.util.TileE;

public class PacketExplosionTiles {
	public int id;
	public ArrayList<TileE> tiles;
}
