package lt.edgu.blasterman.network;

import java.util.ArrayList;

public class MPRoom {
	public String host;
	public String name;
	public String mapName;
	public int port;
	public ArrayList<String> players = new ArrayList<String>();
	
	public String toString() {
		return " " + name;
	}
}
