package lt.edgu.blasterman;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import lt.edgu.blasterman.screens.SPGameScreen;

public class Blasterman extends Game {
	public static final String TITLE = "Blasterman";
	public static final String SERVER_ADDRESS = "london.edgu.lt";
	//public static final String SERVER_ADDRESS = "localhost";
	public static Preferences preferences;
	public static TextureAtlas textureAtlas;
	public static Skin skin;
	public Stage stage;

	public Blasterman() {}

	@Override
	public void create() {
		TextureManager.init();
		AudioManager.init();
		textureAtlas = new TextureAtlas("ui/uiskin.atlas");
		skin = new Skin(Gdx.files.internal("ui/loginSkin.json"), textureAtlas);
		preferences = Gdx.app.getPreferences("lt.edgu.blasterman");
		stage = new Stage();
		Table bg = new Table(skin);
		bg.setFillParent(true);
		bg.setBackground(TextureManager.authBG.getDrawable());
		stage.addActor(bg);
		//		setScreen(new SplashScreen());
		
		//DEBUG <->
		((Game) Gdx.app.getApplicationListener()).setScreen(new SPGameScreen("Level1"));
		
		//setScreen(new RoomsScreen("ti"));
		//setScreen(new ModeScreen());
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

		switch (getScreen().getClass().getSimpleName()) {
		case "SplashScreen":
		case "SPGameScreen":
		case "MPGameScreen":
			break;
		default:
			stage.act();
			stage.draw();
			break;
		}
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();		
	}

	public static Preferences getPrefereces() {
		return preferences;
	}
}