package lt.edgu.blasterman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class TextureManager {
	
	// UI, MISC
	public static Sprite splashScreen;
	public static Sprite pegi;
	public static Image ingameHUD;
	public static Image roomScreenBG;
	public static Image chatBG;
	public static Image authBG;
	public static Image gameOver;
	public static Texture grass;

	// Player Boxes
	public static Image yellowPlayer;
	public static Image bluePlayer;
	public static Image redPlayer;
	public static Image grayPlayer;
	
	// Texture Atlasses
	public static TextureAtlas enemyAtlas;
	public static TextureAtlas playerAtlasYellow;
	public static TextureAtlas playerAtlasBlue;
	public static TextureAtlas playerAtlasRed;
	public static TextureAtlas playerAtlasGray;
	

	// Yellow Player Atlas
	public static Animation yellowForwardAnimation;
	public static Animation yellowLeftAnimation;
	public static Animation yellowBackwardAnimation;
	public static Animation yellowRightAnimation;
	
	// Blue Player
	public static Animation blueForwardAnimation;
	public static Animation blueLeftAnimation;
	public static Animation blueBackwardAnimation;
	public static Animation blueRightAnimation;
	
	// Red Player
	public static Animation redForwardAnimation;
	public static Animation redLeftAnimation;
	public static Animation redBackwardAnimation;
	public static Animation redRightAnimation;
	
	// Red Player
	public static Animation grayForwardAnimation;
	public static Animation grayLeftAnimation;
	public static Animation grayBackwardAnimation;
	public static Animation grayRightAnimation;
	
	// Map thumbnails
	public static Image bombermanThumbnail;
	public static Image spaceInvadersThumbnail;
	
	// Tempvar
	public static Animation enemyHorizontal;
	public static Animation enemyVertical;
	
	public static Animation blueEnemyHorizontal;
	public static Animation blueEnemyVertical;
	public static Animation redEnemyHorizontal;
	public static Animation redEnemyVertical;
	public static Animation pinkEnemyHorizontal;
	public static Animation pinkEnemyVertical;
	public static Animation yellowEnemyHorizontal;
	public static Animation yellowEnemyVertical;
	
	public static Texture x;
	
	public static void init() {
		x = new Texture(Gdx.files.internal("ui/x.png"));
		splashScreen = new Sprite(new Texture(Gdx.files.internal("ui/SplashScreen.png")));
		pegi = new Sprite(new Texture(Gdx.files.internal("ui/PEGI.png")));
		ingameHUD = new Image(new Texture(Gdx.files.internal("ui/ingameHUD/hud.png")));
		roomScreenBG = new Image(new Texture(Gdx.files.internal("ui/roomScreenbg.png")));
		chatBG = new Image(new Texture(Gdx.files.internal("ui/chatbg.png")));
		authBG = new Image(new Texture(Gdx.files.internal("ui/auth.png")));
		gameOver = new Image(new Texture(Gdx.files.internal("ui/gameOver.png")));
		grass = new Texture(Gdx.files.internal("maps/multiPlayer/Bomberman/tiles/misc/grass.png"));
		grass.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		
		bombermanThumbnail = new Image(new Texture(Gdx.files.internal("maps/multiPlayer/Bomberman/thumb.png")));
		spaceInvadersThumbnail = new Image(new Texture(Gdx.files.internal("maps/multiPlayer/Space_Invaders/thumb.png")));
		
		yellowPlayer = new Image(new Texture(Gdx.files.internal("ui/ingameHUD/players/yellowPlayer.png")));
		bluePlayer = new Image(new Texture(Gdx.files.internal("ui/ingameHUD/players/bluePlayer.png")));
		redPlayer = new Image(new Texture(Gdx.files.internal("ui/ingameHUD/players/redPlayer.png")));
		grayPlayer = new Image(new Texture(Gdx.files.internal("ui/ingameHUD/players/grayPlayer.png")));
		
		// Texture Atlasses
		enemyAtlas = new TextureAtlas("npc/ghosts.txt");
		playerAtlasYellow = new TextureAtlas("playerAnimations/yellow/yellow.txt");
		playerAtlasBlue = new TextureAtlas("playerAnimations/blue/blue.txt");
		playerAtlasRed = new TextureAtlas("playerAnimations/red/red.txt");
		playerAtlasGray = new TextureAtlas("playerAnimations/gray/gray.txt");
		
		// Animations
		//		Enemy Animations
		blueEnemyHorizontal = new Animation(1 / 10f, enemyAtlas.findRegions("bv"));
		blueEnemyVertical = new Animation(1 / 10f, enemyAtlas.findRegions("bh"));
		
		redEnemyHorizontal = new Animation(1 / 10f, enemyAtlas.findRegions("rv"));
		redEnemyVertical = new Animation(1 / 10f, enemyAtlas.findRegions("rh"));
		
		pinkEnemyHorizontal = new Animation(1 / 10f, enemyAtlas.findRegions("pv"));
		pinkEnemyVertical = new Animation(1 / 10f, enemyAtlas.findRegions("ph"));
		
		yellowEnemyHorizontal = new Animation(1 / 10f, enemyAtlas.findRegions("yv"));
		yellowEnemyVertical = new Animation(1 / 10f, enemyAtlas.findRegions("yh"));
		
		yellowForwardAnimation = new Animation(1 / 10F, playerAtlasYellow.findRegions("forward_frame"));
		yellowLeftAnimation = new Animation(1 / 10F, playerAtlasYellow.findRegions("left_frame"));
		yellowBackwardAnimation = new Animation(1 / 10F, playerAtlasYellow.findRegions("backward_frame"));
		yellowRightAnimation = new Animation(1 / 10F, playerAtlasYellow.findRegions("right_frame"));
		yellowForwardAnimation.setPlayMode(Animation.PlayMode.LOOP);
		yellowLeftAnimation.setPlayMode(Animation.PlayMode.LOOP);
		yellowBackwardAnimation.setPlayMode(Animation.PlayMode.LOOP);
		yellowRightAnimation.setPlayMode(Animation.PlayMode.LOOP);
		
		
		blueForwardAnimation = new Animation(1 / 10F, playerAtlasBlue.findRegions("forward_frame"));
		blueLeftAnimation = new Animation(1 / 10F, playerAtlasBlue.findRegions("left_frame"));
		blueBackwardAnimation = new Animation(1 / 10F, playerAtlasBlue.findRegions("backward_frame"));
		blueRightAnimation = new Animation(1 / 10F, playerAtlasBlue.findRegions("right_frame"));
		blueForwardAnimation.setPlayMode(Animation.PlayMode.LOOP);
		blueLeftAnimation.setPlayMode(Animation.PlayMode.LOOP);
		blueBackwardAnimation.setPlayMode(Animation.PlayMode.LOOP);
		blueRightAnimation.setPlayMode(Animation.PlayMode.LOOP);
		
		
		redForwardAnimation = new Animation(1 / 10F, playerAtlasRed.findRegions("forward_frame"));
		redLeftAnimation = new Animation(1 / 10F, playerAtlasRed.findRegions("left_frame"));
		redBackwardAnimation = new Animation(1 / 10F, playerAtlasRed.findRegions("backward_frame"));
		redRightAnimation = new Animation(1 / 10F, playerAtlasRed.findRegions("right_frame"));
		redForwardAnimation.setPlayMode(Animation.PlayMode.LOOP);
		redLeftAnimation.setPlayMode(Animation.PlayMode.LOOP);
		redBackwardAnimation.setPlayMode(Animation.PlayMode.LOOP);
		redRightAnimation.setPlayMode(Animation.PlayMode.LOOP);
		
		
		grayForwardAnimation = new Animation(1 / 10F, playerAtlasGray.findRegions("forward_frame"));
		grayLeftAnimation = new Animation(1 / 10F, playerAtlasGray.findRegions("left_frame"));
		grayBackwardAnimation = new Animation(1 / 10F, playerAtlasGray.findRegions("backward_frame"));
		grayRightAnimation = new Animation(1 / 10F, playerAtlasGray.findRegions("right_frame"));
		grayForwardAnimation.setPlayMode(Animation.PlayMode.LOOP);
		grayLeftAnimation.setPlayMode(Animation.PlayMode.LOOP);
		grayBackwardAnimation.setPlayMode(Animation.PlayMode.LOOP);
		grayRightAnimation.setPlayMode(Animation.PlayMode.LOOP);
	}
}
