package lt.edgu.blasterman;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;

public class TiledManager {
	
	public TiledMap tiledMap;
	
	// Tiles
	public TiledMapTile grassTile;
	public TiledMapTile grassCollideTile;
	public TiledMapTile wallTile;
	public TiledMapTile explosionTile;
	public TiledMapTile bombTile;
	public TiledMapTile boxTile;
	
	// Power Up Tiles
	public TiledMapTile additionalBombPowerUp;
	public TiledMapTile speedBoostPowerUp;
	public TiledMapTile bombStrengthPowerUp;
	public TiledMapTile randomPowerUp;
	
	public void init(TiledMap tiledMap) {
		this.tiledMap = tiledMap;
//		System.out.println("\n--- START OF TILE LIST ---\n----- ID | TILE NAME | TILE SET -----");
//		for (int i = 1; i <= 50; i++) {
//			try {
//			System.out.println("TILE ID: " + i + " - " + this.tiledMap.getTileSets().getTile(i).getProperties().get("name") + " - " + this.tiledMap.getTileSets().getTile(i).getProperties().get("tileSet"));
//			} catch (NullPointerException e) {
//				System.out.println("--- END OF TILE LIST ---\n");
//				break;
//			}
//		}
		wallTile = this.tiledMap.getTileSets().getTileSet("walls").getTile(9);
		additionalBombPowerUp = this.tiledMap.getTileSets().getTileSet("powerUp").getTile(10);
		bombStrengthPowerUp = this.tiledMap.getTileSets().getTileSet("powerUp").getTile(11);
		randomPowerUp = this.tiledMap.getTileSets().getTileSet("powerUp").getTile(12);
		speedBoostPowerUp = this.tiledMap.getTileSets().getTileSet("powerUp").getTile(13);
		grassTile = this.tiledMap.getTileSets().getTileSet("misc").getTile(14);
		grassCollideTile = this.tiledMap.getTileSets().getTileSet("misc").getTile(15);
		explosionTile = this.tiledMap.getTileSets().getTileSet("misc").getTile(17);
		bombTile = this.tiledMap.getTileSets().getTileSet("misc").getTile(18);
		boxTile = this.tiledMap.getTileSets().getTileSet("misc").getTile(16);
	}
	
}
