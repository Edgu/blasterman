package lt.edgu.blasterman.desktop;

import lt.edgu.blasterman.Blasterman;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		new DesktopLauncher();
	}
	
	public DesktopLauncher() {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = Blasterman.TITLE;
		config.vSyncEnabled = true;
		config.height = 480;
		config.width = 800;
		config.samples = 3;
		new LwjglApplication(new Blasterman(), config);
	}
}