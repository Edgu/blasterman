package lt.edgu.blasterman.gameserver;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import lt.edgu.blasterman.gameserver.customClasses.MPRoom;
import lt.edgu.blasterman.gameserver.packets.PacketCreateRoom;
import lt.edgu.blasterman.gameserver.packets.PacketDeleteRoom;
import lt.edgu.blasterman.gameserver.packets.PacketJoinRoom;
import lt.edgu.blasterman.gameserver.packets.PacketRequestRoomList;
import lt.edgu.blasterman.gameserver.packets.PacketRoomsList;
import lt.edgu.blasterman.gameserver.packets.PacketUpdateRoom;
import lt.edgu.blasterman.gameserver.packets.room.PacketNewMessage;
import lt.edgu.blasterman.gameserver.packets.room.PacketNewPlayer;
import lt.edgu.blasterman.gameserver.packets.room.PacketSetUsername;
import lt.edgu.blasterman.gameserver.sql.DBConnect;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class GameServer extends Listener {

	private static Server server;
	private static CommandListener commandListener;
	private static final int TCP_PORT = 54555, UDP_PORT = 54555;

	private static Map<Integer, Boolean> ports;
	private static volatile ArrayList<Room> rooms;

	private static Log log;
	protected static boolean logEnabled = false;
	private static SimpleDateFormat timeStamp;
	private static Calendar date;
	private final static int MAX_ROOMS = 300;

	public static void main(String[] args) throws IOException {
		server = new Server();
		registerPackets();
		server.bind(TCP_PORT, UDP_PORT);
		server.start();
		server.addListener(new GameServer());
		ports = new HashMap<Integer, Boolean>();
		rooms = new ArrayList<Room>();
		date = Calendar.getInstance();
		timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		date.getTime();
		//log = new Log("logs\\Logs-" + timeStamp.format(date.getTime()) + ".txt");
		log = new Log("GameServer-" + timeStamp.format(date.getTime()) + ".txt");
		log.output("[Launch] <Game Server is ready on " + TCP_PORT + " Port. [TCP]>");
		DBConnect.init(log);
		commandListener = new CommandListener(log);
		new Thread(commandListener).start();

		for (int i = TCP_PORT + 1; i < TCP_PORT + MAX_ROOMS; i++) {
			ports.put(i, false);
		}

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				try {
					DBConnect.terminate();
				} catch (SQLException e) {
					try {
						DBConnect.terminate();
					} catch (SQLException e1) {
					}
				}
			}
		}));
	}

	private static void registerPackets() {
		Kryo kryo = server.getKryo();
		kryo.register(PacketNewPlayer.class);
		kryo.register(PacketNewMessage.class);
		kryo.register(PacketSetUsername.class);
		kryo.register(PacketRequestRoomList.class);
		kryo.register(PacketRoomsList.class);
		kryo.register(PacketCreateRoom.class);
		kryo.register(PacketJoinRoom.class);
		kryo.register(PacketUpdateRoom.class);
		kryo.register(ArrayList.class);
		kryo.register(MPRoom.class);
		kryo.register(PacketDeleteRoom.class);
	}

	public void connected(Connection connection) {
		listRooms(connection.getID(), false);
//		log.output("[Connected] <Player: " + connection.getID() + ", IP: " + connection.getRemoteAddressTCP().getHostString() + ">");
	}

	public void received(Connection connection, Object object) {
		if (object instanceof PacketNewMessage) {
			PacketNewMessage packet = (PacketNewMessage) object;
			server.sendToAllExceptTCP(connection.getID(), packet);
		} else if (object instanceof PacketRequestRoomList) {
			listRooms(connection.getID(), false);
		} else if (object instanceof PacketCreateRoom) {
			PacketCreateRoom packet = (PacketCreateRoom) object;
			packet.id = connection.getID();
			createRoom(packet, connection.getID());
		} else if (object instanceof PacketUpdateRoom) {

		} else if (object instanceof PacketJoinRoom) {

		}
	}

	public static void listRooms(int id, boolean sendToAll) {
		ArrayList<MPRoom> roomList = new ArrayList<MPRoom>();
		for (Room room : rooms) {
			if (room.getRoomState() == RoomState.WAITING) {
				MPRoom newRoom = new MPRoom();
				newRoom.host = room.getHost();
				newRoom.players = room.getPlayerNames();
				newRoom.port = room.getPort();
				newRoom.name = room.getName();
				newRoom.mapName = room.getMapName();
				roomList.add(newRoom);
				
			}
		}
		PacketRoomsList packet = new PacketRoomsList();
		packet.rooms = roomList;
		if (!sendToAll) {
			server.sendToTCP(id, packet);
		} else {
			server.sendToAllTCP(packet);
		}
	}

	public static void createRoom(PacketCreateRoom packet, int host) {
		boolean assigned = false;
		for (int i = TCP_PORT + 1; (i < TCP_PORT + MAX_ROOMS) && !assigned; i++) {
			if (!ports.get(i)) {
				ports.put(i, true);
				assigned = true;
				Room room = new Room(packet, i);
				rooms.add(room);
				PacketCreateRoom packetCreateRoom = new PacketCreateRoom();
				packetCreateRoom.host = room.getHost();
				packetCreateRoom.port = i;
				packetCreateRoom.name = room.getName();
				packetCreateRoom.mapName = packet.mapName;
				packetCreateRoom.players = room.getPlayerNames();
				server.sendToAllTCP(packetCreateRoom);
			}
		}
		if (!assigned)
			System.out.println("Karocia uzimti portai visi.");
	}

	public static int threadCount() {
		System.out.println(Thread.getAllStackTraces());
		return Thread.activeCount();
	}

	public static void deleteRoom(Room room) {
		ports.put(room.getPort(), false);
		rooms.remove(room);
		PacketDeleteRoom packet = new PacketDeleteRoom();
		packet.port = room.getPort();
		server.sendToAllTCP(packet);
		//		listRooms(0, true);
		System.gc();
	}

	public static void startingRoom(Room room) {
		PacketDeleteRoom packet = new PacketDeleteRoom();
		packet.port = room.getPort();
		server.sendToAllTCP(packet);
	}

	public void disconnected(Connection connection) {
//		log.output("[Disconnected] <Player: " + connection.getID() + ">" + connection);
	}

	public static ArrayList<Room> getRooms() {
		return rooms;
	}
}