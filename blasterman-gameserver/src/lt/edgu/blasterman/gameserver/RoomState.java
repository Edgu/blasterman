package lt.edgu.blasterman.gameserver;

public enum RoomState {
	IDLE,
	WAITING,
	FULL,
	STARTING,
	STARTED,
	FINISHED,
	DELETE;
	
	public RoomState getRoomState() {
		return this;
	}
}
