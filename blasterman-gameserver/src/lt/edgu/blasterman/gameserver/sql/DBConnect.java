package lt.edgu.blasterman.gameserver.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Timer;
import java.util.TimerTask;

import lt.edgu.blasterman.gameserver.Log;

public abstract class DBConnect {
	// Database credentials
	private static String database = "blasterman";
	private static String userName = "blasterman";
	private static String password = "edgaras2232";

	// JDBC driver name and database URL
	private static String driver = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://server.edgu.lt:3306/" + database + "?autoReconnect=true";

	private static Connection connection;
	private static volatile Statement statement;

	public static void init(Log log) {
		try {
			//STEP 2: Register JDBC driver
			Class.forName(driver);

			//STEP 3: Open a connection
			log.output("[MySQL] <Connecting to database...>");
			connection = DriverManager.getConnection(url, userName, password);

			//STEP 4: Execute a query
			statement = connection.createStatement();
			statement.executeQuery("/* ping */ select 1");
			log.output("[MySQL] <Succesfully connected!>");
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
				public void run() {
					try {
						statement.executeQuery("/* ping */ select 1");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
		}, 10 * 60 * 1000, 10 * 60 * 1000);
	}

	public static void incrementKillCount(String username) {
		int playerID = getPlayerID(username);
		if (playerID > 0 && checkPlayerStatus(username) == 1) {
			try {
				NamedParameterStatement update = new NamedParameterStatement(connection, "UPDATE players SET kills = kills + 1 WHERE id = :id");
				update.setInt("id", playerID);
				update.executeUpdate();
				update.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void incrementDeathCount(String username) {
		int playerID = getPlayerID(username);
		if (playerID > 0 && checkPlayerStatus(username) == 1) {
			try {
				NamedParameterStatement update = new NamedParameterStatement(connection, "UPDATE players SET deaths = deaths + 1 WHERE id = :id");
				update.setInt("id", playerID);
				update.executeUpdate();
				update.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void addExperience(String username, int experience) {
		int playerID = getPlayerID(username);
		if (playerID > 0 && checkPlayerStatus(username) == 1) {
			try {
				NamedParameterStatement update = new NamedParameterStatement(connection, "UPDATE players SET experience = experience + xp  WHERE id = :id");
				update.setInt("id", playerID);
				update.setInt("xp", experience);
				update.executeUpdate();
				update.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Returns true if player was logged out, false otherwise.
	public static boolean logOutPlayer(String username) {
		switch (checkPlayerStatus(username)) {
		case 0:
			return true;
		case 1:
			try {
				NamedParameterStatement nps = new NamedParameterStatement(connection, "UPDATE players SET tempID = 0 ,online = 0 WHERE BINARY username = :username");
				nps.setString("username", username);
				nps.executeUpdate();
			} catch (Exception exception) {
				exception.printStackTrace();
			}
			return true;
		default:
			return false;
		}
	}

	// Returns 1 if player is online, 0 if ofline, -1 if player is not found.
	public static int checkPlayerStatus(String username) {
		if (getPlayerID(username) > 0) {
			try {
				NamedParameterStatement nps = new NamedParameterStatement(connection, "SELECT online FROM players WHERE BINARY username = :username LIMIT 1");
				nps.setString("username", username);

				ResultSet rs = nps.executeQuery();

				while (rs.next()) {
					int status = rs.getInt("online");
					nps.close();
					rs.close();
					return status;
				}
				nps.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	public static int getPlayerID(String username) {
		try {
			NamedParameterStatement nps = new NamedParameterStatement(connection, "SELECT id FROM players WHERE BINARY username = :username LIMIT 1");
			nps.setString("username", username);

			ResultSet rs = nps.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				if (id > 0) {
					nps.close();
					rs.close();
					return id;
				}
			}
			nps.close();
			rs.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return -1;
	}

	public static void terminate() throws SQLException {
		NamedParameterStatement nps = new NamedParameterStatement(connection, "UPDATE players SET tempID = 0, online = 0");
		nps.executeUpdate();
		nps.close();
	}
}