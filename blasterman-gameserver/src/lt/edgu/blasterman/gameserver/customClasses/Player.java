package lt.edgu.blasterman.gameserver.customClasses;

import java.util.ArrayList;
import java.util.Random;

import lt.edgu.blasterman.gameserver.TiledManager;
import lt.edgu.blasterman.gameserver.packets.room.PacketAdditionalBomb;
import lt.edgu.blasterman.gameserver.packets.room.PacketPlayerDeath;
import lt.edgu.blasterman.gameserver.packets.room.PacketSetTile;
import lt.edgu.blasterman.gameserver.packets.room.PacketSpeedBoost;
import lt.edgu.blasterman.gameserver.sql.DBConnect;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;

public class Player {
	private Server server;
	private Connection connection;
	private String username;
	private float x, y;
	private byte colour;
	private int bombs;
	// new vars
	private int speed;
	private boolean alive;
	private TiledManager tiledManager;

	private Vector2 targetPosition;
	private int roundedTargetPosition;

	public ArrayList<Character> moves;
	public char currentMove;
	public char lastMove;
	public int spriteSizePOW = 32;
	
	private int bombStrenght;

	public Player(Server server, Connection connection, String username, float x, float y, TiledManager tiledManager) {
		this.server = server;
		this.connection = connection;
		this.username = username;
		this.x = x;
		this.y = y;
		this.tiledManager = tiledManager;
		bombs = 1;
		speed = 64;
		currentMove = 'x';
		lastMove = 'x';
		moves = new ArrayList<Character>();
		bombStrenght = 1;
		alive = true;
	}

	public void update(float delta) {
		if (alive) {
			try {
				if (!moves.isEmpty() && moves.get(0) == 'w') {
					if (currentMove == 'x') {
						targetPosition = new Vector2((int) x, (int) (y + spriteSizePOW / 2));
					}
					roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
					if (!isTileSolid(new Vector2(x, roundedTargetPosition)) && y <= roundedTargetPosition) {
						currentMove = 'w';
						lastMove = currentMove;
						y += speed * delta;
						if (y >= roundedTargetPosition) {
							y = roundedTargetPosition;
							moves.remove(0);
							currentMove = 'x';
						}
					}
				} else if (!moves.isEmpty() && moves.get(0) == 'a') {
					if (currentMove == 'x') {
						targetPosition = new Vector2((int) (x - spriteSizePOW), (int) y);
					}
					roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
					if (!isTileSolid(new Vector2(roundedTargetPosition, y)) && x >= roundedTargetPosition) {
						currentMove = 'a';
						lastMove = currentMove;
						x -= speed * delta;
						if (x <= roundedTargetPosition) {
							x = roundedTargetPosition;
							moves.remove(0);
							currentMove = 'x';
						}
					}
				} else if (!moves.isEmpty() && moves.get(0) == 's') {
					if (currentMove == 'x') {
						targetPosition = new Vector2((int) x, (int) (y - spriteSizePOW));
					}
					roundedTargetPosition = 32 * (Math.round(targetPosition.y / 32));
					if (!isTileSolid(new Vector2(x, roundedTargetPosition)) && y >= roundedTargetPosition) {
						currentMove = 's';
						lastMove = currentMove;
						y -= speed * delta;
						if (y <= roundedTargetPosition) {
							y = roundedTargetPosition;
							moves.remove(0);
							currentMove = 'x';
						}
					}
				} else if (!moves.isEmpty() && moves.get(0) == 'd') {
					if (currentMove == 'x') {
						targetPosition = new Vector2((int) (x + spriteSizePOW / 2), (int) y);
					}
					roundedTargetPosition = 32 * (Math.round(targetPosition.x / 32));
					if (!isTileSolid(new Vector2(roundedTargetPosition, y)) && x <= roundedTargetPosition) {
						currentMove = 'd';
						lastMove = currentMove;
						x += speed * delta;
						if (x >= roundedTargetPosition) {
							x = roundedTargetPosition;
							moves.remove(0);
							currentMove = 'x';
						}
					}
				}
			} catch (NullPointerException exception) {}
			checkStep();
		}

	}

	private boolean isTileSolid(Vector2 position) {
		Cell cell = tiledManager.mainLayer.getCell((int) (position.x / tiledManager.mainLayer.getTileWidth()), (int) (position.y / tiledManager.mainLayer.getTileHeight()));
		return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey("solid");
	}

	private void checkStep() {
		int x = (int) (this.x / tiledManager.mainLayer.getTileWidth());
		int y = (int) (this.y / tiledManager.mainLayer.getTileHeight());
		Cell cell = tiledManager.mainLayer.getCell(x, y);

		// Checks if player steps on these tiles.
		switch (cell.getTile().getId()) {
		case 10:
			// Additional Bomb
			if (bombs < 6) {
				bombs++;
				PacketAdditionalBomb addBombPacket = new PacketAdditionalBomb();
				server.sendToTCP(connection.getID(), addBombPacket);
			}
			setTile(x, y, 14);
			break;
		case 11:
			// Bomb Strength
			bombStrenght += 1;
			setTile(x, y, 14);
			break;
		case 12:
			// Random
			Random random = new Random();
			int powerUp = random.nextInt((3 - 1) + 1) + 1;
			switch (powerUp) {
			case 1:
				// Speed Boost
				if (speed < 192) {
					speed += 32;
					PacketSpeedBoost speedBoost = new PacketSpeedBoost();
					speedBoost.id = connection.getID();
					server.sendToAllTCP(speedBoost);
				}
				break;
			case 2:
				// Additional Bomb
				if (bombs < 6) {
					bombs++;
					PacketAdditionalBomb addBombPacket = new PacketAdditionalBomb();
					server.sendToTCP(connection.getID(), addBombPacket);
				}
				break;
			case 3:
				// Bomb str
				bombStrenght += 1;
				break;
			}
			setTile(x, y, 14);
			break;
		case 13:
			// Speed Boost
			if (speed < 192) {
				speed += 32;
				PacketSpeedBoost speedBoost = new PacketSpeedBoost();
				speedBoost.id = connection.getID();
				server.sendToAllTCP(speedBoost);
			}
			setTile(x, y, 14);
			break;
		case 555:
			// Explosion
			alive = false;
			PacketPlayerDeath deathPacket = new PacketPlayerDeath();
			deathPacket.id = connection.getID();
			deathPacket.killer = cell.getTile().getProperties().get("ownerName").toString();
			if (!getUsername().equals(deathPacket.killer)) {
				DBConnect.incrementKillCount(deathPacket.killer);
				DBConnect.incrementDeathCount(getUsername());
			} else {
				DBConnect.incrementDeathCount(getUsername());
			}
			server.sendToAllTCP(deathPacket);
			break;
		}
	}
	
	private void setTile(int x, int y, int tile) {
		boolean dontSend = false;
		switch (tile) {
		case 10:
			tiledManager.mainLayer.getCell(x, y).setTile(tiledManager.additionalBombPowerUp);
			break;
		case 11:
			tiledManager.mainLayer.getCell(x, y).setTile(tiledManager.bombStrengthPowerUp);
			break;
		case 12:
			tiledManager.mainLayer.getCell(x, y).setTile(tiledManager.randomPowerUp);
			break;
		case 13:
			tiledManager.mainLayer.getCell(x, y).setTile(tiledManager.speedBoostPowerUp);
			break;
		case 14:
			tiledManager.mainLayer.getCell(x, y).setTile(tiledManager.grassTile);
			dontSend = true;
			break;
		default:
			dontSend = true;
		}
		if (!dontSend) {
			PacketSetTile packet = new PacketSetTile();
			packet.tile = tile;
			packet.x = x;
			packet.y = y;
			server.sendToAllTCP(packet);
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public byte getColour() {
		return colour;
	}
	
	public String getColourString() {
		switch (colour) {
		case 0:
			return "Yellow";
		case 1:
			return "Blue";
		case 2:
			return "Red";
		case 3:
			return "Gray";
		default:
			return null;
		}
	}

	public void setColour(byte colour) {
		this.colour = colour;
	}

	public int getBombs() {
		return bombs;
	}

	public void setBombs(int bombs) {
		if (bombs < 6) {
			this.bombs = bombs;
		}
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		if (this.speed < 224 & speed < 224 && (speed - this.speed) == 32) {
			this.speed = speed;
		}
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public TiledManager getTiledManager() {
		return tiledManager;
	}

	public void setTiledManager(TiledManager tiledManager) {
		this.tiledManager = tiledManager;
	}

	public int getBombStrenght() {
		return bombStrenght;
	}

	public void setBombStrenght(int bombStrenght) {
		this.bombStrenght = bombStrenght;
	}
}
