package lt.edgu.blasterman.gameserver.customClasses;

import java.util.ArrayList;

public class MPRoom {
	public String host;
	public String name;
	public String mapName;
	public int port;
	public ArrayList<String> players;
	
	public String toString() {
		return name;
	}
}
