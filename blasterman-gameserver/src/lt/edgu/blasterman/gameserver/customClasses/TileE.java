package lt.edgu.blasterman.gameserver.customClasses;

public class TileE {
	public int id;
	public int x;
	public int y;
	public int underneathTile;
	
	public TileE() {}
	
	public TileE(int id, int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public TileE(int id, int x, int y, int underneathTile) {
		this.x = x;
		this.y = y;
		this.underneathTile = underneathTile;
	}
}
