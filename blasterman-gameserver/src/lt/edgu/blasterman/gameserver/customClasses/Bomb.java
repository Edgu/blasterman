package lt.edgu.blasterman.gameserver.customClasses;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import lt.edgu.blasterman.gameserver.Room;
import lt.edgu.blasterman.gameserver.packets.room.PacketExplosionTiles;
import lt.edgu.blasterman.gameserver.packets.room.PacketSetTile;

import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;

public class Bomb {
	private int id;
	private int owner;
	private int x, y;
	private int radius;
	private volatile Room room;
	private ArrayList<TileE> explosionTiles;

	public Bomb(int owner, int id, int x, int y, int radius, Room room) {
		this.id = id;
		this.owner = owner;
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.room = room;
		explosionTiles = new ArrayList<TileE>();
		detonate();
	}

	public void detonate() {
		// Fills ArrayList with tiles which are going to be destroyed.

		// Tile Under Bomb
		explosionTiles.add(new TileE(id, x, y, room.getTiledManager().mainLayer.getCell(x, y).getTile().getId()));

		// Up
		for (int y = this.y + 1; y < ((this.y + 1) + radius); y++) {
			TiledMapTile currentTile = room.getTiledManager().mainLayer.getCell(x, y).getTile();
			if (currentTile.getProperties().containsKey("destructible") || currentTile.getId() == 18) {
				explosionTiles.add(new TileE(id, x, y, currentTile.getId()));
				if (currentTile.getId() == 16) {
					break;
				}
			} else {
				break;
			}
		}

		// Down
		for (int y = this.y - 1; y > ((this.y - 1) - radius); y--) {
			TiledMapTile currentTile = room.getTiledManager().mainLayer.getCell(x, y).getTile();
			if (currentTile.getProperties().containsKey("destructible") || currentTile.getId() == 18) {
				explosionTiles.add(new TileE(id, x, y, currentTile.getId()));
				if (currentTile.getId() == 16) {
					break;
				}
			} else {
				break;
			}
		}

		// Left
		for (int x = this.x - 1; x > ((this.x - 1) - radius); x--) {
			TiledMapTile currentTile = room.getTiledManager().mainLayer.getCell(x, y).getTile();
			if (currentTile.getProperties().containsKey("destructible") || currentTile.getId() == 18) {
				explosionTiles.add(new TileE(id, x, y, currentTile.getId()));
				if (currentTile.getId() == 16) {
					break;
				}
			} else {
				break;
			}
		}

		// Right
		for (int x = this.x + 1; x < ((this.x + 1) + radius); x++) {
			TiledMapTile currentTile = room.getTiledManager().mainLayer.getCell(x, y).getTile();
			if (currentTile.getProperties().containsKey("destructible") || currentTile.getId() == 18) {
				explosionTiles.add(new TileE(id, x, y, currentTile.getId()));
				if (currentTile.getId() == 16) {
					break;
				}
			} else {
				break;
			}
		}


		// --- ///
		TimerTask explosion = new TimerTask() {

			@Override
			public void run() {
				final ArrayList<TileE> tiles = explosionTiles;
				for (int i = 0; i < tiles.size(); i++) {
					Cell cell = room.getTiledManager().mainLayer.getCell((int) tiles.get(i).x, (int) tiles.get(i).y);
					if (cell.getTile().getProperties().containsKey("owner")) {
						if (!cell.getTile().getProperties().get("owner").equals(id)) {
							cell.getTile().getProperties().put("owner", id);
							cell.getTile().getProperties().put("exploded", cell.getTile().getId());
						}
					} else {
						StaticTiledMapTile tile = new StaticTiledMapTile(room.getTiledManager().explosionTile.getTextureRegion());
						tile.setId(555);
						tile.getProperties().put("destructible", null);
						tile.getProperties().put("owner", id);
						if (room.getPlayer(owner) != null) {
							tile.getProperties().put("ownerName", room.getPlayer(owner).getUsername());
						}
						tile.getProperties().put("exploded", cell.getTile().getId());
						cell.setTile(tile);
					}
				}
				PacketExplosionTiles packetExplosion = new PacketExplosionTiles();
				packetExplosion.id = owner;
				packetExplosion.tiles = tiles;
				room.getServer().sendToAllTCP(packetExplosion);
				Player player = room.getPlayer(owner);
				player.setBombs(player.getBombs() + 1);
				Timer timer2 = new Timer();
				timer2.schedule(new TimerTask() {
					@Override
					public void run() {
						Random random = new Random();
						for (int i = 0; i < tiles.size(); i++) {
							//							int ddd = room.getTiledManager().mainLayer.getCell((int) tiles.get(i).x, (int) tiles.get(i).y).getTile().getId();
							//								if (ddd == 10 || ddd == 13 || ddd == 12 || ddd == 11) {
							//									System.out.println("BOOST 13");
							//									tiles.get(i).underneathTile = ddd;
							//								}
							///
							TiledMapTile qw = room.getTiledManager().mainLayer.getCell((int) tiles.get(i).x, (int) tiles.get(i).y).getTile();
							if ((random.nextInt((3 - 1) + 1) + 1) == 3 && tiles.get(i).underneathTile == 16 && qw.getProperties().get("exploded").equals(16)) {
								//							if (true) {
								int powerUp = random.nextInt((4 - 1) + 1) + 1;
								switch (powerUp) {
								case 1:
									PacketSetTile additionalBombPacket = new PacketSetTile();
									additionalBombPacket.tile = 10;
									additionalBombPacket.x = (int) tiles.get(i).x;
									additionalBombPacket.y = (int) tiles.get(i).y;
									room.getTiledManager().mainLayer.getCell(additionalBombPacket.x, additionalBombPacket.y).setTile(room.getTiledManager().additionalBombPowerUp);
									room.getServer().sendToAllTCP(additionalBombPacket);
									break;
								case 2:
									PacketSetTile speedBoostPacket = new PacketSetTile();
									speedBoostPacket.tile = 13;
									speedBoostPacket.x = (int) tiles.get(i).x;
									speedBoostPacket.y = (int) tiles.get(i).y;
									room.getTiledManager().mainLayer.getCell(speedBoostPacket.x, speedBoostPacket.y).setTile(room.getTiledManager().speedBoostPowerUp);
									room.getServer().sendToAllTCP(speedBoostPacket);
									break;
								case 3:
									PacketSetTile randomPowerUpPacket = new PacketSetTile();
									randomPowerUpPacket.tile = 12;
									randomPowerUpPacket.x = (int) tiles.get(i).x;
									randomPowerUpPacket.y = (int) tiles.get(i).y;
									room.getTiledManager().mainLayer.getCell(randomPowerUpPacket.x, randomPowerUpPacket.y).setTile(room.getTiledManager().randomPowerUp);
									room.getServer().sendToAllTCP(randomPowerUpPacket);
									break;
								case 4:
									PacketSetTile bombStrengthBoost = new PacketSetTile();
									bombStrengthBoost.tile = 11;
									bombStrengthBoost.x = (int) tiles.get(i).x;
									bombStrengthBoost.y = (int) tiles.get(i).y;
									room.getTiledManager().mainLayer.getCell(bombStrengthBoost.x, bombStrengthBoost.y).setTile(room.getTiledManager().bombStrengthPowerUp);
									room.getServer().sendToAllTCP(bombStrengthBoost);
									break;
								}
							} else {
								Cell cell = room.getTiledManager().mainLayer.getCell((int) tiles.get(i).x, (int) tiles.get(i).y);
								if (cell.getTile().getProperties().containsKey("owner")) {
									if (cell.getTile().getProperties().get("owner").equals(id)) {
										PacketSetTile grassPacket = new PacketSetTile();
										grassPacket.tile = 14;
										grassPacket.x = (int) tiles.get(i).x;
										grassPacket.y = (int) tiles.get(i).y;
										room.getTiledManager().mainLayer.getCell((int) tiles.get(i).x, (int) tiles.get(i).y).setTile(room.getTiledManager().grassTile);
										room.getServer().sendToAllTCP(grassPacket);
									}
								} else {
									PacketSetTile grassPacket = new PacketSetTile();
									grassPacket.tile = 14;
									grassPacket.x = (int) tiles.get(i).x;
									grassPacket.y = (int) tiles.get(i).y;
									room.getTiledManager().mainLayer.getCell((int) tiles.get(i).x, (int) tiles.get(i).y).setTile(room.getTiledManager().grassTile);
									room.getServer().sendToAllTCP(grassPacket);
								}
								// If its not our tile, do nothing.
							}
						}
						return;
					}
				}, 1250);
			}
		};

		Timer timer = new Timer();
		timer.schedule(explosion, 2000);
		System.gc();
	}

	public int getId() {
		return id;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getRadius() {
		return radius;
	}
}