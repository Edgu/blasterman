package lt.edgu.blasterman.gameserver;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

public class TiledManager {

	// Could be different map for each room so we make this non static.
	public volatile TiledMap tiledMap;
	public volatile TiledMapTileLayer mainLayer;
	public TiledMapTileLayer spawnPoints;

	// Tiles
	public TiledMapTile grassTile;
	public TiledMapTile grassCollideTile;
	public TiledMapTile wallTile;
	public TiledMapTile explosionTile;
	public TiledMapTile bombTile;

	// Power Up Tiles
	public TiledMapTile additionalBombPowerUp;
	public TiledMapTile speedBoostPowerUp;
	public TiledMapTile bombStrengthPowerUp;
	public TiledMapTile randomPowerUp;

	public boolean tilesSet = false;

	public TiledManager(String mapLocation) {
		TmxMapLoader tmxMapLoader = new TmxMapLoader();
		tiledMap = tmxMapLoader.load(mapLocation);
		mainLayer = (TiledMapTileLayer) tiledMap.getLayers().get(0);
		spawnPoints = (TiledMapTileLayer) tiledMap.getLayers().get(1);
		//		System.out.println("\n--- START OF TILE LIST ---\n----- ID | TILE NAME | TILE SET -----");
		//		for (int i = 1; i <= 50; i++) {
		//			try {
		//				System.out.println("TILE ID: " + i + " - " + tiledMap.getTileSets().getTile(i).getProperties().get("name") + " - " + tiledMap.getTileSets().getTile(i).getProperties().get("tileSet"));
		//			} catch (NullPointerException e) {
		//				System.out.println("--- END OF TILE LIST ---\n");
		//				break;
		//			}
		//		}
		if (!tilesSet) {
			wallTile = tiledMap.getTileSets().getTileSet("walls").getTile(9);
			additionalBombPowerUp = tiledMap.getTileSets().getTileSet("powerUp").getTile(10);
			bombStrengthPowerUp = tiledMap.getTileSets().getTileSet("powerUp").getTile(11);
			randomPowerUp = tiledMap.getTileSets().getTileSet("powerUp").getTile(12);
			speedBoostPowerUp = tiledMap.getTileSets().getTileSet("powerUp").getTile(13);
			grassTile = tiledMap.getTileSets().getTileSet("misc").getTile(14);
			grassCollideTile = tiledMap.getTileSets().getTileSet("misc").getTile(15);
			explosionTile = tiledMap.getTileSets().getTileSet("misc").getTile(17);
			bombTile = tiledMap.getTileSets().getTileSet("misc").getTile(18);
			tilesSet = true;
		}
	}
}