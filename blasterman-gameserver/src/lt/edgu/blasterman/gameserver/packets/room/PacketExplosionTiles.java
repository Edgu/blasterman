package lt.edgu.blasterman.gameserver.packets.room;

import java.util.ArrayList;

import lt.edgu.blasterman.gameserver.customClasses.TileE;

public class PacketExplosionTiles {
	public int id;
	public ArrayList<TileE> tiles;
}