package lt.edgu.blasterman.gameserver.packets.room;

public class PacketNewPlayer {
	public int id;
	public String username;
	public float x, y;
	public byte colour;
}