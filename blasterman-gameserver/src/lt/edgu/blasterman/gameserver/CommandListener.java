package lt.edgu.blasterman.gameserver;

import java.util.Scanner;

public class CommandListener implements Runnable {

	private String command;
	private boolean newCommand = false;
	private Log log;
	
	public CommandListener(Log log) {
		this.log = log;
	}

	@Override
	public void run() {
		Scanner in = new Scanner(System.in);

		log.output("[Launch] <CommandListener succesfully started>");

		while (true) {
			command = in.nextLine();
			newCommand= true;

			if (newCommand){
				newCommand = false;
				switch (command) {
				case "help":
					log.output("[CommandListener] <Help command executed>");
					System.out.println("Available commands are: \n"
							+ "help, restart, shutdown");
					break;
				case "restart":
					log.output("[CommandListener] <Restart command executed>");
					break;
				case "shutdown":
					log.output("[CommandListener] <Shutdown command executed>");
					in.close();
					System.exit(0);
					break;
				case "listRooms":
					for (Room room : GameServer.getRooms()) {
						System.out.println("Room Port: " + room.getPort());
						System.out.println("Room Host: " + room.getHost());
						System.out.println("Room Name: " + room.getName());
						System.out.println("Room State: " + room.getRoomState());
						System.out.println("Room Players: " + room.getPlayerNames() + "\n");
					}
					break;
				case "threads":
					System.out.println(GameServer.threadCount());
					break;
				case "sendRoomList":
					GameServer.listRooms(0, true);
					break;
				case "":
					break;
				case " ":
					break;
				default:
					log.output("[CommandListener] <Invalid Command: " + command + ">");
					break;
				}
			}
		}
	}
}