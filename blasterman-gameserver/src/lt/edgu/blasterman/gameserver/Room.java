package lt.edgu.blasterman.gameserver;

import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import lt.edgu.blasterman.gameserver.customClasses.Bomb;
import lt.edgu.blasterman.gameserver.customClasses.Player;
import lt.edgu.blasterman.gameserver.customClasses.TileE;
import lt.edgu.blasterman.gameserver.packets.PacketCreateRoom;
import lt.edgu.blasterman.gameserver.packets.PacketJoinRoom;
import lt.edgu.blasterman.gameserver.packets.room.PacketAdditionalBomb;
import lt.edgu.blasterman.gameserver.packets.room.PacketEndGame;
import lt.edgu.blasterman.gameserver.packets.room.PacketExplosionTiles;
import lt.edgu.blasterman.gameserver.packets.room.PacketMove;
import lt.edgu.blasterman.gameserver.packets.room.PacketNewBomb;
import lt.edgu.blasterman.gameserver.packets.room.PacketNewMessage;
import lt.edgu.blasterman.gameserver.packets.room.PacketNewPlayer;
import lt.edgu.blasterman.gameserver.packets.room.PacketPlayerDeath;
import lt.edgu.blasterman.gameserver.packets.room.PacketPlayerSpeed;
import lt.edgu.blasterman.gameserver.packets.room.PacketRandomPowerUp;
import lt.edgu.blasterman.gameserver.packets.room.PacketRemovePlayer;
import lt.edgu.blasterman.gameserver.packets.room.PacketSetHost;
import lt.edgu.blasterman.gameserver.packets.room.PacketSetTile;
import lt.edgu.blasterman.gameserver.packets.room.PacketSetUsername;
import lt.edgu.blasterman.gameserver.packets.room.PacketSpeedBoost;
import lt.edgu.blasterman.gameserver.packets.room.PacketStartGame;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class Room extends Listener implements ApplicationListener {
	private Server server;
	private ArrayList<Player> players;
	private int port;
	private String name;
	private String mapName;
	private String host;
	//	private int hostID;

	private volatile TiledManager tiledManager;

	private ArrayList<Vector2> spawns;
	private HashMap<Integer, Boolean> colours;

	private HeadlessApplication headlessApplication;

	private volatile RoomState roomState;

	public Room(PacketCreateRoom packet, int port) {
		this.port = port;
		setName(packet.name);
		setHost(packet.host);
		setMapName(packet.mapName);
		server = new Server();
		registerPackets();
		try {
			server.bind(port, port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		server.start();
		server.addListener(this);
		players = new ArrayList<Player>();
		colours = new HashMap<Integer, Boolean>();
		populateColoursMap();
		//		hostID = packet.id;
		setRoomState(RoomState.IDLE);
		spawns = new ArrayList<Vector2>();
		HeadlessApplicationConfiguration conf = new HeadlessApplicationConfiguration();
		headlessApplication = new HeadlessApplication(this, conf);
		Gdx.gl = mock(GL20.class);
		tiledManager = new TiledManager((packet.mapName + "/" + packet.mapName + ".tmx"));

		// Iterate through tiles to find spawn points.
		for (int rows = 0; rows < tiledManager.spawnPoints.getWidth(); rows++) {
			for (int columns = 0; columns < tiledManager.spawnPoints.getHeight(); columns++) {
				// Try&Catch for null tiles
				try {
					if (tiledManager.spawnPoints.getCell(rows, columns).getTile().getProperties().get("name").equals("spawnPoint")) {
						spawns.add(new Vector2(rows * tiledManager.spawnPoints.getTileWidth(), columns * tiledManager.spawnPoints.getTileHeight()));
					}
				} catch (NullPointerException e) {}
			}
		}
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (roomState == RoomState.IDLE) {
					setRoomState(RoomState.DELETE);
				}
			}
		}, 5000);
	}

	public void populateColoursMap() {
		colours.put(0, false);
		colours.put(1, false);
		colours.put(2, false);
		colours.put(3, false);
	}

	public void registerPackets() {
		Kryo kryo = server.getKryo();
		kryo.register(PacketNewPlayer.class);
		kryo.register(PacketPlayerDeath.class);
		kryo.register(PacketRemovePlayer.class);
		kryo.register(PacketSetUsername.class);
		kryo.register(PacketAdditionalBomb.class);
		kryo.register(PacketSpeedBoost.class);
		kryo.register(PacketNewBomb.class);
		kryo.register(PacketExplosionTiles.class);
		kryo.register(PacketRandomPowerUp.class);
		kryo.register(PacketMove.class);
		kryo.register(PacketPlayerSpeed.class);
		kryo.register(ArrayList.class);
		kryo.register(TileE.class);
		kryo.register(PacketJoinRoom.class);
		kryo.register(PacketNewMessage.class);
		kryo.register(PacketStartGame.class);
		kryo.register(PacketSetTile.class);
		kryo.register(PacketSetHost.class);
		kryo.register(PacketEndGame.class);
	}

	@Override
	public void connected(Connection connection) {
		if (roomState == RoomState.IDLE || roomState == RoomState.WAITING) {
			if (players.size() < 4) {
				for (Player player : players) {
					PacketNewPlayer packet = new PacketNewPlayer();
					packet.id = player.getConnection().getID();
					packet.colour = player.getColour();
					packet.username = player.getUsername();
					packet.x = player.getX();
					packet.y = player.getY();
					server.sendToTCP(connection.getID(), packet);
				}
				Random random = new Random();
				int randomSpawn = random.nextInt(((spawns.size() - 1) - 0) + 1);

				Player player = new Player(server, connection, "", spawns.get(randomSpawn).x, spawns.get(randomSpawn).y, tiledManager);
				spawns.remove(randomSpawn);

				newPlayer(player);
			} else {
				PacketJoinRoom joinRoom = new PacketJoinRoom();
				joinRoom.connect = false;
				server.sendToTCP(connection.getID(), joinRoom);
				connection.close();
			}
		} else {
			PacketJoinRoom joinRoom = new PacketJoinRoom();
			joinRoom.connect = false;
			server.sendToTCP(connection.getID(), joinRoom);
			connection.close();
		}
	}

	public void newPlayer(Player player) {
		setRoomState(RoomState.WAITING);
		for (int i = 0; i < colours.size(); i++) {
			if (!colours.get(i)) {
				colours.put(i, true);
				player.setColour((byte) i);
				break;
			}
		}
		PacketNewPlayer packet = new PacketNewPlayer();
		packet.id = player.getConnection().getID();
		packet.colour = player.getColour();
		packet.username = player.getUsername();
		packet.x = player.getX();
		packet.y = player.getY();
		players.add(player);
		server.sendToAllTCP(packet);
		PacketJoinRoom joinRoom = new PacketJoinRoom();
		joinRoom.connect = true;
		server.sendToTCP(player.getConnection().getID(), joinRoom);
		if (players.size() == 4) {
			setRoomState(RoomState.FULL);
			GameServer.listRooms(0, true);
		}
	}

	public void removePlayer(int id) {
		boolean removed = false;
		for (byte i = 0; i < players.size() && !removed; i++) {
			if (players.get(i).getConnection().getID() == id) {
				spawns.add(new Vector2(players.get(i).getX(), players.get(i).getY()));
				players.remove(i);
				removed = true;
			}
		}
	}

	public Player getPlayer(int id) {
		for (byte i = 0; i < players.size(); i++) {
			if (players.get(i).getConnection().getID() == id) {
				return players.get(i);
			}
		}
		return null;
	}

	@Override
	public void received(Connection connection, Object object) {
		switch (roomState) {
		case WAITING:
		case FULL:
			if (object instanceof PacketStartGame) {
				boolean usernamesSet = true;
				for (Player player : players) {
					if (player.getUsername() == null || player.getUsername().isEmpty()) {
						usernamesSet = false;
						break;
					}
				}
				if (getPlayer(connection.getID()).getUsername().equals(host) && players.size() > 1 && usernamesSet) {
					setRoomState(RoomState.STARTING);
					server.sendToAllTCP(new PacketStartGame());
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						public void run() {
							if (getRoomState() == RoomState.STARTING) {
								setRoomState(RoomState.STARTED);
							}
						};
					}, 5000);
				} else {
					if (!getPlayer(connection.getID()).getUsername().equals(host)) {
						PacketNewMessage notHost = new PacketNewMessage();
						notHost.sender = "---ROOM NOTIFICATION---";
						notHost.message = "You cant start the room because you are not the host!";
						server.sendToTCP(connection.getID(), notHost);
					}
					if (players.size() < 2) {
						PacketNewMessage notEnoughPlayers = new PacketNewMessage();
						notEnoughPlayers.sender = "---ROOM NOTIFICATION---";
						notEnoughPlayers.message = "Not enough players connected! <Minimum 2 Needed>";
						server.sendToTCP(connection.getID(), notEnoughPlayers);
					}
				}
			} else if (object instanceof PacketNewMessage) {
				PacketNewMessage packet = (PacketNewMessage) object;
				server.sendToAllExceptTCP(connection.getID(), packet);
			} else if (object instanceof PacketSetUsername) {
				PacketSetUsername packet = (PacketSetUsername) object;
				Player player = getPlayer(connection.getID());
				player.setUsername(packet.username);
				//				if (DBConnect.checkPlayerStatus(player.getUsername()) <= 0) {
				//					player.getConnection().close();
				//				}
				server.sendToAllExceptTCP(connection.getID(), packet);
				String colour = "Undefined";
				switch (getPlayer(connection.getID()).getColour()) {
				case 0:
					colour = "Yellow";
					break;
				case 1:
					colour = "Blue";
					break;
				case 2:
					colour = "Red";
					break;
				case 3:
					colour = "Gray";
					break;
				default:
					break;
				}
				PacketNewMessage message = new PacketNewMessage();
				message.sender = "---ROOM NOTIFICATION---";
				message.message = "Player " + getPlayer(connection.getID()).getUsername() + " (" + colour + ") has joined the room!";
				server.sendToAllExceptTCP(connection.getID(), message);
			}

			if (object instanceof PacketPlayerDeath || object instanceof PacketNewBomb || object instanceof PacketMove) {
				connection.close();
			}
			break;
		case STARTED:
			if (object instanceof PacketNewMessage) {
				PacketNewMessage packet = (PacketNewMessage) object;
				server.sendToAllExceptTCP(connection.getID(), packet);
			} else if (object instanceof PacketNewBomb) {
				PacketNewBomb packet = (PacketNewBomb) object;
				Player player = getPlayer(connection.getID());
				if (player.isAlive()) {
					if (placeBomb(tiledManager.mainLayer.getCell(packet.x, packet.y).getTile().getId(), 14, 10, 11, 12, 13, 17)) {
						if (player.getBombs() > 0) {
							player.setBombs(player.getBombs() - 1);
							tiledManager.mainLayer.getCell(packet.x, packet.y).setTile(tiledManager.bombTile);
							server.sendToAllTCP(packet);
							Random random = new Random();
							int num = random.nextInt((69569596 - 24) + 786) + 25;
							new Bomb(connection.getID(), num, packet.x, packet.y, player.getBombStrenght(), Room.this);
						}
					}
				}
			} else if (object instanceof PacketMove) {
				PacketMove packet = (PacketMove) object;
				Player player = getPlayer(connection.getID());
				if (player.isAlive()) {
					player.moves.add(packet.direction);
					server.sendToAllExceptTCP(connection.getID(), packet);
				}
			}
			break;
		case FINISHED:
			break;
		default:
			break;
		}
	}

	public boolean placeBomb(int targetTileID, int... tileID) {
		for (int tile : tileID) {
			if (targetTileID == tile) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void disconnected(Connection connection) {
		//		DBConnect.logOutPlayer(getPlayer(connection.getID()).getUsername());
		if (getPlayer(connection.getID()) != null) {
			String colour = "Undefined";
			switch (getPlayer(connection.getID()).getColour()) {
			case 0:
				colour = "Yellow";
				colours.put(0, false);
				break;
			case 1:
				colour = "Blue";
				colours.put(1, false);
				break;
			case 2:
				colour = "Red";
				colours.put(2, false);
				break;
			case 3:
				colour = "Gray";
				colours.put(3, false);
				break;
			default:
				break;
			}

			if (getPlayer(connection.getID()).getUsername().equals(host) && (getRoomState() == RoomState.WAITING || getRoomState() == RoomState.FULL)) {
				if (players.size() > 1) {
					PacketSetHost packet = new PacketSetHost();
					host = players.get(1).getUsername();
					server.sendToTCP(players.get(1).getConnection().getID(), packet);

					PacketNewMessage message = new PacketNewMessage();
					message.sender = "---ROOM NOTIFICATION---";
					message.message = "Player " + players.get(1).getUsername() + " (" + players.get(1).getColourString() + ") has become a room host!";
					server.sendToAllExceptTCP(connection.getID(), message);
				}
			}

			PacketNewMessage message = new PacketNewMessage();
			message.sender = "---ROOM NOTIFICATION---";
			message.message = "Player " + getPlayer(connection.getID()).getUsername() + " (" + colour + ") has left the room!";
			server.sendToAllExceptTCP(connection.getID(), message);
			removePlayer(connection.getID());
			PacketRemovePlayer packet = new PacketRemovePlayer();
			packet.id = connection.getID();
			server.sendToAllExceptTCP(connection.getID(), packet);
			switch (players.size()) {
			case 0:
				if (getRoomState() == RoomState.WAITING || roomState == RoomState.FINISHED || roomState == RoomState.IDLE) {
					setRoomState(RoomState.DELETE);
				}
				break;
			case 3:
				if (getRoomState() == RoomState.FULL) {
					setRoomState(RoomState.WAITING);
					GameServer.listRooms(0, true);
				}
				break;
			}
		}
	}

	public ArrayList<String> getPlayerNames() {
		ArrayList<String> playerNames = new ArrayList<String>();
		for (Player player : players) {
			playerNames.add(player.getUsername() + player.getColour());
		}
		return playerNames;
	}

	public RoomState getRoomState() {
		return roomState;
	}

	public void setRoomState(RoomState rs) {
		this.roomState = rs;
		switch (roomState) {
		case WAITING:
		case FULL:
			break;
		case DELETE:
			GameServer.deleteRoom(this);
			server.close();
			server.stop();
			Thread.currentThread().interrupt();
			headlessApplication.getApplicationListener().dispose();
			headlessApplication.exit();
			return;
		case FINISHED:
			break;
		case STARTED:
			break;
		case STARTING:
			GameServer.startingRoom(this);
			break;
		case IDLE:
			break;
		default:
			break;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPort() {
		return port;
	}

	@Override
	public void create() {}

	@Override
	public void dispose() {}

	@Override
	public void pause() {}

	@Override
	public void render() {
		// \r to print on same line	
		if (getRoomState() == RoomState.STARTED) {
			try {
				int playerCOUNT = players.size();
				int dead = 0;
				for (Player player : players) {
					if (player.isAlive()) {
						player.update(Gdx.graphics.getDeltaTime());
					} else {
						dead++;
					}
				}
				if ((playerCOUNT - dead) <= 1) {
					PacketEndGame packet = new PacketEndGame();
					packet.winner = "NOWIN";
					if ((playerCOUNT - dead) == 1) {
						for (Player player : players) {
							if (player.isAlive()) {
								packet.winner = player.getUsername();
								break;
							}
						}
					}
					server.sendToAllTCP(packet);
					setRoomState(RoomState.FINISHED);
				}
			} catch (Exception error) {
				error.printStackTrace();
			}
		}
	}

	@Override
	public void resize(int width, int height) {}

	@Override
	public void resume() {}

	public TiledManager getTiledManager() {
		return tiledManager;
	}

	public void setTiledManager(TiledManager tiledManager) {
		this.tiledManager = tiledManager;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Server getServer() {
		return server;
	}
	
	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}
}