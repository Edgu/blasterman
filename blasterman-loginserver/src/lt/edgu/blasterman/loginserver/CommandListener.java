package lt.edgu.blasterman.loginserver;


import java.util.Scanner;

import lt.edgu.blasterman.loginserver.Log;

public class CommandListener implements Runnable {

	private String command;
	private boolean newCommand = false;
	private Log log;
	
	public CommandListener(Log log) {
		this.log = log;
	}

	@Override
	public void run() {
		Scanner in = new Scanner(System.in);

		log.output("[Launch] <CommandListener succesfully started>");

		while (true) {
			command = in.nextLine();
			newCommand= true;

			if (newCommand){
				newCommand = false;
				switch (command) {
				case "help":
					log.output("[CommandListener] <Help command executed>");
					System.out.println("Available commands are: \n"
							+ "help, restart, shutdown");
					break;
				case "restart":
					log.output("[CommandListener] <Restart command executed>");
					break;
				case "shutdown":
					log.output("[CommandListener] <Shutdown command executed>");
					in.close();
					System.exit(0);
					break;
				default:
					log.output("[CommandListener] <Invalid Command: " + command + ">");
					break;
				}
			}
		}
	}
}