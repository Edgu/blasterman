package lt.edgu.blasterman.loginserver;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import lt.edgu.blasterman.loginserver.packets.PacketLogin;
import lt.edgu.blasterman.loginserver.packets.PacketLoginFailure;
import lt.edgu.blasterman.loginserver.packets.PacketLoginSuccess;
import lt.edgu.blasterman.loginserver.packets.PacketRegister;
import lt.edgu.blasterman.loginserver.packets.PacketRegisterFailure;
import lt.edgu.blasterman.loginserver.packets.PacketRegisterSuccess;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class LoginServer extends Listener {

	private static Server server;
	private static final int tcpPort = 54554, TCPPort = 54554;
	private static Log log;
	private static SimpleDateFormat timeStamp;
	private static Calendar date;

	public static void main(String[] args) throws IOException {
		server = new Server();
		registerPackets();
		server.bind(tcpPort, TCPPort);
		server.start();
		server.addListener(new LoginServer());
		date = Calendar.getInstance();
		timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		date.getTime();
		//log = new Log("logs\\Logs-" + timeStamp.format(date.getTime()) + ".txt");
		log = new Log("LoginServer-" + timeStamp.format(date.getTime()) + ".txt");
		log.output("[Launch] <Login Server is ready on " + tcpPort + " Port. [TCP]>");
		Thread thread = new Thread(new CommandListener(log));
		thread.start();
		DBConnect.init(log);
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				// Executes before shutting down.
				try {
					DBConnect.terminate();
				} catch (SQLException e) {
					try {
						DBConnect.terminate();
					} catch (SQLException e1) {
					}
				}
			}
		}));
	}

	private static void registerPackets() {
		Kryo kryo = server.getKryo();
		kryo.register(PacketLogin.class);
		kryo.register(PacketLoginSuccess.class);
		kryo.register(PacketLoginFailure.class);
		kryo.register(PacketRegister.class);
		kryo.register(PacketRegisterSuccess.class);
		kryo.register(PacketRegisterFailure.class);
	}

	public void connected(Connection connection) {
		log.output("[Connected] <Player: " + connection.getID() + ", IP: " + connection.getRemoteAddressTCP().getHostString() + ">");
	}

	public void received(Connection connection, Object object) {
		if (!object.getClass().getSimpleName().equals("Ping"))
			//		System.out.println("Packet Received: " + object.getClass().getSimpleName());
			if (object instanceof PacketLogin) {
				PacketLogin packet = (PacketLogin) object;
				int code = DBConnect.login(packet.username, packet.password, connection.getID());
				if (code > 0) {
					PacketLoginSuccess packetLoginSuccess = new PacketLoginSuccess();
					packetLoginSuccess.playerId = code;
					packetLoginSuccess.username = packet.username;
					server.sendToTCP(connection.getID(), packetLoginSuccess);
				} else {
					PacketLoginFailure packetLoginFailure = new PacketLoginFailure();
					packetLoginFailure.errorCode = code;
					server.sendToTCP(connection.getID(), packetLoginFailure);
				}
			} else if (object instanceof PacketRegister) {
				PacketRegister packet = (PacketRegister) object;
				try {
					int code = DBConnect.register(packet.username, connection.getID(), packet.password, packet.confirmPassword, packet.email);
					if (code > 0) {
						PacketRegisterSuccess packetRegisterSuccess = new PacketRegisterSuccess();
						packetRegisterSuccess.username = packet.username;
						server.sendToTCP(connection.getID(), packetRegisterSuccess);
					} else {
						PacketRegisterFailure fail = new PacketRegisterFailure();
						fail.errorCode = (byte) code;
						server.sendToTCP(connection.getID(), fail);
					}
				} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
					e.printStackTrace();
				}
			}
	}

	public void disconnected(Connection connection) {
		String logOutText = "[Disconnected] <Player: " + connection.getID() + ">";
		if (DBConnect.logOutPlayer(DBConnect.getPlayerNameByID(connection.getID()))) {
			logOutText = "[Disconnected] <Player: " + connection.getID() + ", DDC: true>";
		} else {
			//			logOutText = "[Disconnected] <Player: " + connection.getID() + ", DDC: false>";
			logOutText = null;
		}
		if (logOutText != null) {
			log.output(logOutText);
		}
	}
}