package lt.edgu.blasterman.loginserver;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import lt.edgu.blasterman.loginserver.customClasses.NamedParameterStatement;

public abstract class DBConnect {
	// Database credentials
	private static String database = "blasterman";
	private static String userName = "blasterman";
	private static String password = "tavomama";

	// JDBC driver name and database URL
	private static String driver = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://server.edgu.lt:3306/" + database + "?autoReconnect=true";

	private static Connection connection;
	private static volatile Statement statement;

	public static void init(Log log) {
		try {
			//STEP 2: Register JDBC driver
			Class.forName(driver);

			//STEP 3: Open a connection
			log.output("[MySQL] <Connecting to database...>");
			connection = DriverManager.getConnection(url, userName, password);

			//STEP 4: Execute a query
			statement = connection.createStatement();
			statement.executeQuery("/* ping */ select 1");
			log.output("[MySQL] <Succesfully connected>");
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				try {
					statement.executeQuery("/* ping */ select 1");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}, 10 * 60 * 1000, 10 * 60 * 1000);
	}

	public static int login(String username, String password, int tempID) {
		try {
			NamedParameterStatement nps = new NamedParameterStatement(connection, "SELECT id, online, password FROM players WHERE BINARY username = :username LIMIT 1");
			nps.setString("username", username);
			ResultSet rs = nps.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				int online = rs.getInt("online");
				if (online == 0) {
					if (id > 0) {
						String passwordFromDb = rs.getString("password");
						String[] parts = passwordFromDb.split(":");
						int iterations = Integer.parseInt(parts[0]);
						byte[] salt = fromHex(parts[1]);
						if (validatePassword(password, passwordFromDb, iterations, salt)) {
							NamedParameterStatement update = new NamedParameterStatement(connection, "UPDATE players SET online = 1, tempID = " + tempID + " WHERE id = :id");
							update.setInt("id", id);
							update.executeUpdate();
							update.close();
							nps.close();
							rs.close();
							return id;
						}
					}
				} else {
					nps.close();
					rs.close();
					return -2;
				}
			}
			nps.close();
			rs.close();
			return 0;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return -1;
	}

	// Returns 1 if player is online, 0 if ofline, -1 if player is not found.
	public static int checkPlayerStatus(String username) {
		if (getPlayerID(username) > 0) {
			try {
				NamedParameterStatement nps = new NamedParameterStatement(connection, "SELECT online FROM players WHERE BINARY username = :username LIMIT 1");
				nps.setString("username", username);
				ResultSet rs = nps.executeQuery();
				while (rs.next()) {
					int status = rs.getInt("online");
					nps.close();
					rs.close();
					return status;
				}
				nps.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	// Returns true if player was logged out, false otherwise.
	public static boolean logOutPlayer(String username) {
		switch (checkPlayerStatus(username)) {
		case 0:
			return true;
		case 1:
			try {
				NamedParameterStatement nps = new NamedParameterStatement(connection, "UPDATE players SET tempID = 0 ,online = 0 WHERE BINARY username = :username");
				nps.setString("username", username);
				nps.executeUpdate();
			} catch (Exception exception) {
				exception.printStackTrace();
			}
			return true;
		default:
			return false;
		}
	}

	public static int register(String username, int tempID, String password, String confirmPassword, String email) throws NoSuchAlgorithmException, InvalidKeySpecException {
		// Need to check if input is correct, if passwords match, if email is valid, if no user is register with same email or username.
		if (password.equals(confirmPassword)) {
			Pattern pattern = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
			Matcher matcher = pattern.matcher(email.toUpperCase());
			if (!matcher.matches()) {
				return -3;
			} else {
				if (username.length() > 0 && username.length() < 9) {
					NamedParameterStatement nps;
					try {
						nps = new NamedParameterStatement(connection, "SELECT username FROM players WHERE email = :email LIMIT 1");
						nps.setString("email", email);
						ResultSet rs = nps.executeQuery();
						//nps.close();
						if (rs.next()) {
							//rs.close();
							return -4;
						}
						//rs.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
					if (username.length() <= 0) return -5;
					else if (username.length() >= 9) return -6;
				}
			}
		} else {
			return 0;
		}
		String generatedSecuredPasswordHash = generateStrongPasswordHash(password);
		if (!(getPlayerID(username) > 0)) {
			try {
				NamedParameterStatement nps = new NamedParameterStatement(connection, "INSERT INTO players (username, tempID, password, email, online) VALUES (:username, :tempID, :password, :email, 1)");
				nps.setString("username", username);
				nps.setInt("tempID", tempID);
				nps.setString("password", generatedSecuredPasswordHash);
				nps.setString("email", email);
				nps.executeUpdate();
				return getPlayerID(username);
			} catch(Exception exception) {
				exception.printStackTrace();
				return -1;
			}
		} else {
			//Username taken
			return -2;
		}
	}

	public static int getPlayerID(String username) {
		try {
			NamedParameterStatement nps = new NamedParameterStatement(connection, "SELECT id FROM players WHERE BINARY username = :username LIMIT 1");
			nps.setString("username", username);

			ResultSet rs = nps.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				if (id > 0) {
					nps.close();
					rs.close();
					return id;
				}
			}
			nps.close();
			rs.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return -1;
	}

	public static String getPlayerNameByID(int tempID) {
		try {
			NamedParameterStatement nps = new NamedParameterStatement(connection, "SELECT username FROM players WHERE tempID = :tempID LIMIT 1");
			nps.setInt("tempID", tempID);

			ResultSet rs = nps.executeQuery();

			while (rs.next()) {
				String username = rs.getString("username");
				nps.close();
				rs.close();
				return username;
			}
			nps.close();
			rs.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	// Hashing Methods
	private static String generateStrongPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
		Random random = new Random();
		int iterations = random.nextInt(23456 - 12345 + 1) + 12345;
		char[] chars = password.toCharArray();
		byte[] salt = getSalt().getBytes();
		PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] hash = skf.generateSecret(spec).getEncoded();
		return iterations + ":" + toHex(salt) + ":" + toHex(hash);
	}

	private static boolean validatePassword(String password, String storedPassword, int iterations, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
		char[] chars = password.toCharArray();
		PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
		SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] hash = skf.generateSecret(spec).getEncoded();
		if ((iterations + ":" + toHex(salt) + ":" + toHex(hash)).equals(storedPassword)) {
			return true;
		} else {
			return false;
		}
	}

	private static String getSalt() throws NoSuchAlgorithmException {
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt.toString();
	}

	private static String toHex(byte[] array) throws NoSuchAlgorithmException {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}

	private static byte[] fromHex(String hex) throws NoSuchAlgorithmException {
		byte[] bytes = new byte[hex.length() / 2];
		for(int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return bytes;
	}

	public static void terminate() throws SQLException {
		NamedParameterStatement nps = new NamedParameterStatement(connection, "UPDATE players SET tempID = 0, online = 0");
		nps.executeUpdate();
		nps.close();
	}
}